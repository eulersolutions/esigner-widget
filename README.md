Для включения дев-режима на странице браузера необходимо добавить в параметры запроса ключ `_esdev`, значение не обязательно.

# Локальный сервер

```
    npm run serve
```

# Запуск сборки

```
    npm run build
```

# Подключение скрипта атрибутом в <head>

```
    <script type="module" src="https://bitbucket.org/eulersolutions/esigner-widget/raw/master/loader/vue-bnovo-loader.js">
```

# Подключение скрипта скрипта-хелпера для разработки

*Пока не будет полноценной интеграции

```
    // ==UserScript==
    // @name         Bnovo esigner widget
    // @namespace    https://online.bnovo.ru
    // @version      1.0
    // @description  Загрузчик виджета без тега
    // @author       You
    // @match        https://online.bnovo.ru/*
    // @icon         https://www.google.com/s2/favicons?domain=bnovo.ru
    // @grant        none
    // ==/UserScript==

    if(typeof window._eswc === 'undefined') window._eswc = {};
    window._eswc.token = 'sometoken';
    window._eswc.domain = 'localhost:4000';
    // window._eswc.dev = true; // Включение локальной разработки через сервер данного проекта
    // window._eswc.dev_domain = 'localhost:3011'; // Если локальный сервер на другом порту можно изменить

    let stands = {
        '':'',
        'Локаль': 'localhost:4000',
        'Первый': 'widget-api.dev1.euler.solutions',
        'Второй': 'widget-api.dev.euler.solutions',
        'Препрод': 'widget-api.euler.solutions',
    };
    let loaderUtil = $('<div style="position:absolute;left:0;bottom:16px;z-index:9999;padding:4px;">Стенд: </div>');
    let select = $('<select id="_eswcUtil"/>');
    const _c = localStorage.getItem('_eswcUtilStand');
    for(const [k,v] of Object.entries(stands)) {
        let _v = k;
        if(_c && _c == k) {
            window._eswc.domain = v;
            select.append($(`<option selected value="${_v}">${k}</option>`)) 
        } else {
            select.append($(`<option value="${_v}">${k}</option>`))
        }
    };
    select.on('change', function(e) {
        const $el = $(this);
        localStorage.setItem('_eswcUtilStand', $el.val());
        window.location.reload();
    });

    loaderUtil.append(select);
    $('body').append(loaderUtil);

    // Убрать код ниже когда vue-bnovo-loader.js будет подключен на странице биново разработчиками
    dynamicallyLoadScript(`https://${window._eswc.domain}/widget_loader/bnovo/v2`);

    function dynamicallyLoadScript(url) {
        var script = document.createElement("script");
        script.type = 'module';
        script.crossOrigin = 'anonymous';
        script.src = url;
        document.head.appendChild(script);
    }
```