import serve from 'rollup-plugin-serve';
import common from './rollup.config.js';

common.output = [
  {
    file: 'build/umd/widget.js',
    format: "umd",
    sourcemap: false,
    name: "ESWidget",
    exports: 'named'
  },
];
common.plugins.push(
  serve({
    contentBase: ['build', 'loader'],
    host: 'localhost',
    port: 3011,
    headers: {
      'Access-Control-Allow-Origin': '*'
    }
  })
);

export default common;