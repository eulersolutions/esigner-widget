(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('vuex')) :
    typeof define === 'function' && define.amd ? define(['exports', 'vuex'], factory) :
    (global = typeof globalThis !== 'undefined' ? globalThis : global || self, factory(global.ESWidget = {}, global.Vuex));
})(this, (function (exports, Vuex) { 'use strict';

    function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

    var Vuex__default = /*#__PURE__*/_interopDefaultLegacy(Vuex);

    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //

    var script$b = {
        props: [
            'apartments',
            'locks'
        ],
        name: 'LocksInfo',
    };

    function normalizeComponent(template, style, script, scopeId, isFunctionalTemplate, moduleIdentifier /* server only */, shadowMode, createInjector, createInjectorSSR, createInjectorShadow) {
        if (typeof shadowMode !== 'boolean') {
            createInjectorSSR = createInjector;
            createInjector = shadowMode;
            shadowMode = false;
        }
        // Vue.extend constructor export interop.
        var options = typeof script === 'function' ? script.options : script;
        // render functions
        if (template && template.render) {
            options.render = template.render;
            options.staticRenderFns = template.staticRenderFns;
            options._compiled = true;
            // functional template
            if (isFunctionalTemplate) {
                options.functional = true;
            }
        }
        // scopedId
        if (scopeId) {
            options._scopeId = scopeId;
        }
        var hook;
        if (moduleIdentifier) {
            // server build
            hook = function (context) {
                // 2.3 injection
                context =
                    context || // cached call
                        (this.$vnode && this.$vnode.ssrContext) || // stateful
                        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext); // functional
                // 2.2 with runInNewContext: true
                if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
                    context = __VUE_SSR_CONTEXT__;
                }
                // inject component styles
                if (style) {
                    style.call(this, createInjectorSSR(context));
                }
                // register component module identifier for async chunk inference
                if (context && context._registeredComponents) {
                    context._registeredComponents.add(moduleIdentifier);
                }
            };
            // used by ssr in case component is cached and beforeCreate
            // never gets called
            options._ssrRegister = hook;
        }
        else if (style) {
            hook = shadowMode
                ? function (context) {
                    style.call(this, createInjectorShadow(context, this.$root.$options.shadowRoot));
                }
                : function (context) {
                    style.call(this, createInjector(context));
                };
        }
        if (hook) {
            if (options.functional) {
                // register for functional component in vue file
                var originalRender = options.render;
                options.render = function renderWithStyleInjection(h, context) {
                    hook.call(context);
                    return originalRender(h, context);
                };
            }
            else {
                // inject component registration as beforeCreate hook
                var existing = options.beforeCreate;
                options.beforeCreate = existing ? [].concat(existing, hook) : [hook];
            }
        }
        return script;
    }

    var isOldIE = typeof navigator !== 'undefined' &&
        /msie [6-9]\\b/.test(navigator.userAgent.toLowerCase());
    function createInjector(context) {
        return function (id, style) { return addStyle(id, style); };
    }
    var HEAD;
    var styles = {};
    function addStyle(id, css) {
        var group = isOldIE ? css.media || 'default' : id;
        var style = styles[group] || (styles[group] = { ids: new Set(), styles: [] });
        if (!style.ids.has(id)) {
            style.ids.add(id);
            var code = css.source;
            if (css.map) {
                // https://developer.chrome.com/devtools/docs/javascript-debugging
                // this makes source maps inside style tags work properly in Chrome
                code += '\n/*# sourceURL=' + css.map.sources[0] + ' */';
                // http://stackoverflow.com/a/26603875
                code +=
                    '\n/*# sourceMappingURL=data:application/json;base64,' +
                        btoa(unescape(encodeURIComponent(JSON.stringify(css.map)))) +
                        ' */';
            }
            if (!style.element) {
                style.element = document.createElement('style');
                style.element.type = 'text/css';
                if (css.media)
                    { style.element.setAttribute('media', css.media); }
                if (HEAD === undefined) {
                    HEAD = document.head || document.getElementsByTagName('head')[0];
                }
                HEAD.appendChild(style.element);
            }
            if ('styleSheet' in style.element) {
                style.styles.push(code);
                style.element.styleSheet.cssText = style.styles
                    .filter(Boolean)
                    .join('\n');
            }
            else {
                var index = style.ids.size - 1;
                var textNode = document.createTextNode(code);
                var nodes = style.element.childNodes;
                if (nodes[index])
                    { style.element.removeChild(nodes[index]); }
                if (nodes.length)
                    { style.element.insertBefore(textNode, nodes[index]); }
                else
                    { style.element.appendChild(textNode); }
            }
        }
    }

    /* script */
    var __vue_script__$b = script$b;

    /* template */
    var __vue_render__$b = function () {
      var _vm = this;
      var _h = _vm.$createElement;
      var _c = _vm._self._c || _h;
      return _c(
        "div",
        { staticClass: "apartments-list" },
        [
          _vm._m(0),
          _vm._v(" "),
          _vm._l(_vm.apartments, function (apart) {
            return _c(
              "div",
              { key: apart.id, staticClass: "apartments-list_element" },
              [
                _c("span", { staticClass: "apartments__name" }, [
                  _c("b", [
                    _vm._v(
                      "- " +
                        _vm._s(apart.room_name) +
                        " / " +
                        _vm._s(apart.room_type_name)
                    ) ]) ]),
                _vm._v(" "),
                _c("br"),
                _vm._v(
                  " C: " +
                    _vm._s(apart.dfrom) +
                    " По: " +
                    _vm._s(apart.dto) +
                    "\n\n        "
                ),
                apart.room_id == "0"
                  ? _c("div", [
                      _c("br"),
                      _c("span", { staticClass: "status m-text-red" }, [
                        _vm._v("Данный период не распределен по номерам!") ]) ])
                  : _vm._e(),
                _vm._v(" "),
                apart.room_id != "0" && !apart.isSignerExists
                  ? _c("div", [
                      _c("br"),
                      _c("span", { staticClass: "status m-text-red" }, [
                        _vm._v(
                          "Апартаменты не синхронизированы с системой Signer!"
                        ) ]) ])
                  : _vm._e(),
                _vm._v(" "),
                apart.isSiggnerExists && apart.locks.length == 0
                  ? _c("div", [
                      _c("br"),
                      _c("span", { staticClass: "status m-text-yellow" }, [
                        _vm._v("Нет привязанных замков") ]) ])
                  : _vm._e(),
                _vm._v(" "),
                apart.isSignerExists
                  ? _c(
                      "div",
                      { staticClass: "apartments-locks" },
                      _vm._l(apart.locks, function (lockEl) {
                        return _c(
                          "div",
                          {
                            key: lockEl["lockAlias"],
                            staticClass: "apartments-locks_element",
                          },
                          [
                            _vm._v(
                              "\n                " +
                                _vm._s(lockEl["lockAlias"]) +
                                " \n            "
                            ) ]
                        )
                      }),
                      0
                    )
                  : _vm._e() ]
            )
          }),
          _vm._v(" "),
          _c("br") ],
        2
      )
    };
    var __vue_staticRenderFns__$b = [
      function () {
        var _vm = this;
        var _h = _vm.$createElement;
        var _c = _vm._self._c || _h;
        return _c("p", [
          _c("b", [
            _vm._v("Номера, для которых будут сгенерированы коды доступов:") ]) ])
      } ];
    __vue_render__$b._withStripped = true;

      /* style */
      var __vue_inject_styles__$b = function (inject) {
        if (!inject) { return }
        inject("data-v-acea98c0_0", { source: "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", map: {"version":3,"sources":[],"names":[],"mappings":"","file":"LocksInfo.vue"}, media: undefined });

      };
      /* scoped */
      var __vue_scope_id__$b = undefined;
      /* module identifier */
      var __vue_module_identifier__$b = undefined;
      /* functional template */
      var __vue_is_functional_template__$b = false;
      /* style inject SSR */
      
      /* style inject shadow dom */
      

      
      var __vue_component__$b = /*#__PURE__*/normalizeComponent(
        { render: __vue_render__$b, staticRenderFns: __vue_staticRenderFns__$b },
        __vue_inject_styles__$b,
        __vue_script__$b,
        __vue_scope_id__$b,
        __vue_is_functional_template__$b,
        __vue_module_identifier__$b,
        false,
        createInjector,
        undefined,
        undefined
      );

    //
    //
    //
    //
    //
    //
    //
    //

    var script$a = {
        props: [
            'items',
            'selected'
        ],
        data: function data() {
            return {
                selectedLocal: null
            };
        },
        beforeMount: function beforeMount() {
            this.selectedLocal = this.selected;
        },
        methods: {
            onChanged: function onChanged() {
                this.$emit('select', this.selectedLocal);
            }
        },
        mounted: function() {
            this.$emit('select', this.selectedLocal);
        }
    };

    /* script */
    var __vue_script__$a = script$a;

    /* template */
    var __vue_render__$a = function () {
      var _vm = this;
      var _h = _vm.$createElement;
      var _c = _vm._self._c || _h;
      return _c(
        "select",
        {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.selectedLocal,
              expression: "selectedLocal",
            } ],
          staticClass: "form__select select m-transparent",
          on: {
            change: [
              function ($event) {
                var $$selectedVal = Array.prototype.filter
                  .call($event.target.options, function (o) {
                    return o.selected
                  })
                  .map(function (o) {
                    var val = "_value" in o ? o._value : o.value;
                    return val
                  });
                _vm.selectedLocal = $event.target.multiple
                  ? $$selectedVal
                  : $$selectedVal[0];
              },
              _vm.onChanged ],
          },
        },
        _vm._l(_vm.items, function (item) {
          return _c("option", { key: item.id, domProps: { value: item.id } }, [
            _vm._v("\n        " + _vm._s(item.name) + "\n    ") ])
        }),
        0
      )
    };
    var __vue_staticRenderFns__$a = [];
    __vue_render__$a._withStripped = true;

      /* style */
      var __vue_inject_styles__$a = function (inject) {
        if (!inject) { return }
        inject("data-v-5e54cabc_0", { source: "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", map: {"version":3,"sources":[],"names":[],"mappings":"","file":"Select.vue"}, media: undefined });

      };
      /* scoped */
      var __vue_scope_id__$a = undefined;
      /* module identifier */
      var __vue_module_identifier__$a = undefined;
      /* functional template */
      var __vue_is_functional_template__$a = false;
      /* style inject SSR */
      
      /* style inject shadow dom */
      

      
      var __vue_component__$a = /*#__PURE__*/normalizeComponent(
        { render: __vue_render__$a, staticRenderFns: __vue_staticRenderFns__$a },
        __vue_inject_styles__$a,
        __vue_script__$a,
        __vue_scope_id__$a,
        __vue_is_functional_template__$a,
        __vue_module_identifier__$a,
        false,
        createInjector,
        undefined,
        undefined
      );

    /**
     * 
     * @param {*} isMultiple 
     * @param {*} isUsingLockService 
     * @param {*} contractTemplate 
     * @param {*} checkinInstructionTemplate 
     * @param {*} widgetApi 
     * @param {*} store 
     * @returns {Promise}
     */
    async function registerContract(
        isMultiple,
        adultCustomers,
        isUsingLockService,
        contractTemplate,
        checkinInstructionTemplate,
        sendMessageToClients,
        partnerData,
        partnerUser,
        widgetApi
    ) {

        // Обязательные поля для регистрации удаленного заселения
        /**
         bnovo_user_email
         bnovo_booking_id
         bnovo_customer_id
         bnovo_hotel_id
         bnovo_custom_tempplate
         bnovo_checkin_instruction_template
         schedule
         notify_list
         */
        
        var partnerDataCopy = Object.assign({}, partnerData);

        // В обоих случаях бек ждет ассоциативный массив, где ключ элемента - порядковый индекс элемента
        var apartments = {};
        partnerData.apartments.forEach(function (el, index) {
            apartments[index] = el;
        });
        var schedule = {};
        if(partnerData.schedule) { partnerData.schedule.forEach(function (el, index) {
            schedule[index] = el;
        }); }

        // Это очень странно, да, но бек смотрит на данные ключи именно так
        partnerDataCopy.schedule = apartments;
        partnerDataCopy.apartments = schedule;

        var data = Object.assign(
            {
                // выбранный шаблон договора
                bnovo_custom_template: contractTemplate,
                // выбранный шаблон инструкции
                bnovo_checkin_instruction_template: checkinInstructionTemplate,
            },
            partnerDataCopy,
            partnerUser
        );

        // чекбокс отправки смс. Бек принимает любое значение ключа send_link_to_guest как true, потому ключ добавляется только
        // если надо отправить смс
        if(sendMessageToClients) {
            data.send_link_to_guest = true;
        }

        if(isMultiple) {
            data.signer_contract_strategy = 'separate_contract_for_each_guest';
            data.signer_extra_customers = {};
            for (var i = 0; i < adultCustomers.length; i++) {
                var el = adultCustomers[i];
                data.signer_extra_customers[el.phone] = {
                    bnovo_customer_id: el.id,
                    phone: el.phone,
                    email: el.email,
                    name: el.name,
                    surname: el.surname
                };
            }
        } else {
            data.signer_contract_strategy = 'booking_customer_only';
        }

        // Отличаются эндпоинты регистрации контракта с замками или без
        var promise = null;
        if(isUsingLockService) {
            promise = widgetApi.registerContractWithLocks(data);
        } else {
            promise = widgetApi.registerContract(data);
        }

        return promise.then(function (r) { return r.json(); });
    }

    var PartnerStoreHelper = {
        signedStatuses: ['signed', 'waiting_signing_of_other_contracts'],
        SERVICES: {
            REMOTE_CONTRACT_SIGNING: 'remote_contract_signing',
            REMOTE_LOCK_MANAGEMENT: 'remote_lock_management',
            SEND_SMS_LINK_TO_CLIENT: 'send_sms_link_to_client',
            MULTI_GUEST_SIGNING: 'multi_guest_signing',
            MOBILE_ID: 'mobile_id',
            PASSPORT_OCR: 'passport_ocr'
        },

        /**
         * Префиксы сокет каналов на бекенде
         */
        CHANNELS: {
            /**
             * Канал виджета
             */
            WIDGET_CHANNEL: 'bnovo:widget',
            /**
             * Канал юридического лица (пользователя виджета)
             */
            ACCOUNT: 'bnovo:legal_entity',
        },

        /**
         * Regex для проверки номера телефона
         */
        CONTRACT_PHONE_REGEX: /^\+?7\d{10}$|^8\d{10}$/,

        /**
         * Regex для проверки ФИО гостя
         */
        GUEST_FULLNAME_REGEX: /^[а-яА-Яр-юA-Za-z]+([\-\s–]?[а-яА-Яр-юA-Za-z])+$/,

        /**
         * Определяет и возвращает уникальный идентификатор контракта из системы ESigner
         * основываясь на ключевых полях сущностей.
         * Если одно из ключевых полей будет изменено - изменится ключ.
         * 
         * Может использоваться для определения необходимости ре-рендера виджета
         */
        getContractIdentifier: function getContractIdentifier(esContractState) {
            var result = '';

            if(esContractState && esContractState.code) {
                result += esContractState.code;

                if(esContractState.bookings) {
                    for (var i = 0; i < esContractState.bookings.length; i++) {
                        var el = esContractState.bookings[i];

                        // Добавляется информация по статусам бронированиям внутри контракта
                        result += el.code;

                        // Добавляются ссылки на подписание
                        result += el.form_url;

                        // Добавляет номер телефона
                        result += el.phone;

                        // Количество документов (может быть изменено)
                        result += el.signed_files.length;
                    }
                }
            }

            return result;
        },
    };

    //

    var TEMPLATE_CACHE_KEY = "signer_widget_selected_template";
    var CHECKIN_TEMPLATE_CACHE_KEY = "signer_widget_selected_checkin_template";

    var script$9 = {
      props: ["usingLockService"],
      data: function data() {
        return {
          selectedTemplate: null,
          selectedCheckinTemplate: null,
          isMultipleContractType: false,
          sentMessageToCustomerOnRegister: false,

          isBillingConfirmAccepted: false,

          apartments: [],
        };
      },
      computed: {
        partnerTemplates: function partnerTemplates() {
          return this.$eswStore.getters.getPartnerTemplates;
        },
        partnerCheckinTemplates: function partnerCheckinTemplates() {
          return this.$eswStore.getters.getPartnerCheckinTemplates;
        },
        isTemplatesExists: function isTemplatesExists() {
          return this.$eswStore.getters.getPartnerTemplates.length > 0;
        },
        canSentBookingRequest: function canSentBookingRequest() {
          var apartments = this.$eswStore.getters.getApartments;
          var isApartmentsValid = true;

          for (var i = 0; i < apartments.length; i++) {
            var el = apartments[i];
            isApartmentsValid =
              isApartmentsValid && el.room_id != "0" && el.isSignerExists;
          }

          return this.usingLockService ? isApartmentsValid : true;
        },
        previousSelectedTemplate: {
          get: function () {
            var templates = this.$eswStore.getters.getPartnerTemplates;
            var cache = window.localStorage.getItem(TEMPLATE_CACHE_KEY);
            var result = templates.find(function (el) { return el.id == cache; });

            return result ? result.id : null;
          },
          set: function (template) {
            this.selectedTemplate = template;
          },
        },
        previousSelectedCheckinTemplate: {
          get: function () {
            var roomId = this.$eswStore.getters.getFirstRoomId;
            var templates = this.$eswStore.getters.getPartnerCheckinTemplates;
            var cache = window.localStorage.getItem(
              CHECKIN_TEMPLATE_CACHE_KEY + "_" + roomId
            );
            var result = templates.find(function (el) { return el.id == cache; });

            return result ? result.id : null;
          },
          set: function (template) {
            this.selectedCheckinTemplate = template;
          },
        },
        isMultipleContract: function isMultipleContract() {
          var adultsCount = this.$eswStore.getters.getAdultsCount;
          return adultsCount > 1;
        },
        getApartments: function getApartments() {
          return this.$eswStore.getters.getApartments;
        },
        /**
         * Флаг доступности сервиса отправки СМС с приглашением
         */
        isSmsServiceAvailable: function isSmsServiceAvailable() {
          return this.$eswStore.getters.isServiceAvailable(
            PartnerStoreHelper.SERVICES.SEND_SMS_LINK_TO_CLIENT
          );
        },
        billingState: function billingState() {
          return this.$eswStore.getters.getWidgetBillingState;
        },
      },
      components: {
        LocksInfo: __vue_component__$b,
        Select: __vue_component__$a,
      },
      methods: {
        isTemplateSelected: function isTemplateSelected(template) {
          return typeof template == 'string' && template !== '';
        },
        setTemplate: function setTemplate(template) {
          this.selectedTemplate = template;
          window.localStorage.setItem(TEMPLATE_CACHE_KEY, template);
        },
        setCheckinTemplate: function setCheckinTemplate(template) {
          this.selectedCheckinTemplate = template;
          var roomId = this.$eswStore.getters.getFirstRoomId;
          window.localStorage.setItem(
            CHECKIN_TEMPLATE_CACHE_KEY + "_" + roomId,
            template
          );
        },
        registerBaseContract: function registerBaseContract() {
          this.isMultipleContractType = false;

          this.processRegisterContract();
        },
        registerMultipleContract: function registerMultipleContract() {
          this.isMultipleContractType = true;

          this.processRegisterContract();
        },

        /**
         * Проверяет доступность сервиса множественного заселения
         */
        isMultipleContractServiceAvailable: function isMultipleContractServiceAvailable() {
          return this.$eswStore.getters.isServiceAvailable(
            PartnerStoreHelper.SERVICES.MULTI_GUEST_SIGNING
          );
        },
        /**
         * Проверяет условие на то, что множественное заселение может быть оформлено
         * По параметрам:
         * 1. Заполнены все гости и их количество равно количеству взрослых в бронировании
         * 2. У всех гостей уникальный номер телефона
         */
        isMultipleContractConditionsPass: function isMultipleContractConditionsPass() {
          var adultsList = this.$eswStore.getters.getAdultCustomersWithPhones;
          var primaryCustomerPhone = this.$eswStore.getters.getClientData.phone;
          var phoneList = adultsList.reduce(function (acc, customer) {
            if (customer.phone !== primaryCustomerPhone) { acc[customer.phone] = true; }
            return acc;
          }, []);
          var uniquePhoneList = Object.keys(phoneList);

          var allPhonesAreUnique = uniquePhoneList.length === adultsList.length;

          var adultsCount = this.$eswStore.getters.getAdultsCount;
          // +1 сделано потому что в getAdultCustomersWithPhones нет
          // гостя, для которого оформляется основное удаленное заселение
          var adultListLength = adultsList.length + 1;
          var allAdultsHavePhones = adultListLength === adultsCount;

          return allPhonesAreUnique && allAdultsHavePhones;
        },

        /**
         * Проверяет условие на то, что у аккаунта пользователя виджета
         * 1. Достаточно единиц бронирований для оформления
         * 2. Или тариф аккаунта "триал"
         */
        isBalanceEnoughtForMultipleContract: function isBalanceEnoughtForMultipleContract() {
          var billing = this.$eswStore.getters.getWidgetBillingState;
          var adultsList = this.$eswStore.getters.getAdultCustomersWithPhones;

          return (
            billing.tarif_type_id == "trial" ||
            adultsList.length + 1 <= billing.balance
          );
        },
        processRegisterContract: async function processRegisterContract() {
          var this$1$1 = this;

          await this.$eswStore.dispatch("cleanWarning");

          var contractsCount =
            this.$eswStore.getters.getAdultCustomersWithPhones.length + 1;
          var billingState = this.$eswStore.getters.getWidgetBillingState;

          var cb = function () {
            // Отключение прелоадера не сделано специально
            // виджет самостоятельно уберет прелоадер как только получит обновленный контракт(бронирование)
            this$1$1.$eswStore.dispatch("togglePreloader", true);
            // Отображение компонента "Создание заселения..."
            this$1$1.$eswStore.dispatch("contractCreating", true);

            registerContract(
              this$1$1.isMultipleContractType,
              this$1$1.$eswStore.getters.getAdultCustomersWithPhones,
              this$1$1.usingLockService,
              this$1$1.selectedTemplate,
              this$1$1.selectedCheckinTemplate,
              this$1$1.sentMessageToCustomerOnRegister,
              this$1$1.$eswStore.getters.getPartnerData,
              this$1$1.$eswStore.getters.getPartnerUser,
              this$1$1.$eswStore.getters.getWidgetApi
            ).then(function (result) {
              switch (result.code) {
                case 500:
                  this$1$1.$eswStore.dispatch("handleFatalError", {
                    message: result.error,
                  });
                  break;
                case 501:
                  this$1$1.$eswStore.dispatch("handleFatalError", {
                    message: result.error,
                  });
                  break;
                case 4020:
                  this$1$1.$eswStore.dispatch(
                    "handleWidgetWarning",
                    "Действие вашего пакета истекло"
                  );
                  break;
                case 4021:
                  this$1$1.$eswStore.dispatch(
                    "handleWidgetWarning",
                    "На балансе аккаунта не осталось возможных бронирований"
                  );
                  break;
              }
            });
          };

          // Показывать экран подтверждения если тариф аккаунта не триал и заселений больше 1
          if (
            this.isMultipleContractType &&
            contractsCount > 1 &&
            billingState.tarif_type_id !== "trial"
          ) {
            var data = {
              message: ("С вашего пакета будет списано " + contractsCount + " удалённых подписаний для заселения.<br/>Готовы продолжить?"),
              onAccept: cb,
            };

            this.$eswStore.dispatch("showConfirmationScreen", data);
          } else {
            cb();
          }
        },
      },
    };

    /* script */
    var __vue_script__$9 = script$9;

    /* template */
    var __vue_render__$9 = function () {
      var _vm = this;
      var _h = _vm.$createElement;
      var _c = _vm._self._c || _h;
      return _c(
        "div",
        [
          _vm.isMultipleContract &&
          _vm.isMultipleContractServiceAvailable() &&
          _vm.isMultipleContractConditionsPass() &&
          !_vm.isBalanceEnoughtForMultipleContract()
            ? [
                _c("p", { staticClass: "g-alert" }, [
                  _vm._v(
                    "\n      В пакете недостаточно единиц для заселения всех гостей\n    "
                  ) ]),
                _vm._v(" "),
                _c(
                  "a",
                  {
                    staticClass: "switcher__item button m-primary",
                    attrs: {
                      href: _vm.billingState.payment_page_url,
                      target: "_blank",
                    },
                  },
                  [_vm._v("Купить")]
                ) ]
            : _vm._e(),
          _vm._v(" "),
          _c("div", { staticClass: "form__item" }, [
            _vm.partnerTemplates && _vm.partnerTemplates.length > 0
              ? _c(
                  "div",
                  [
                    _c(
                      "label",
                      {
                        staticClass: "form__radio-label fwb",
                        staticStyle: { "padding-left": "0px" },
                      },
                      [
                        _vm._v(
                          "\n        Выберите шаблон документа для подписания:\n      "
                        ) ]
                    ),
                    _vm._v(" "),
                    _c("Select", {
                      attrs: {
                        items: _vm.partnerTemplates,
                        selected: _vm.previousSelectedTemplate,
                      },
                      on: { select: _vm.setTemplate },
                    }) ],
                  1
                )
              : _c("div", [
                  _c("p", { staticClass: "g-alert" }, [
                    _vm._v("Отсутствует шаблон для выбора") ]),
                  _vm._v(" "),
                  _c("p", [
                    _vm._v(
                      "\n        Пожалуйста, загрузите пользовательский шаблон в настройках аккаунта.\n      "
                    ) ]) ]),
            _vm._v(" "),
            _vm.usingLockService
              ? _c(
                  "div",
                  [
                    _vm.partnerCheckinTemplates &&
                    _vm.partnerCheckinTemplates.length > 0
                      ? _c(
                          "div",
                          [
                            _c(
                              "label",
                              {
                                staticClass: "form__radio-label fwb",
                                staticStyle: { "padding-left": "0px" },
                              },
                              [
                                _vm._v(
                                  "\n          Выберите шаблон документа инструкции по заселению:\n        "
                                ) ]
                            ),
                            _vm._v(" "),
                            _c("Select", {
                              attrs: {
                                items: _vm.partnerTemplates,
                                selected: _vm.previousSelectedCheckinTemplate,
                              },
                              on: { select: _vm.setCheckinTemplate },
                            }) ],
                          1
                        )
                      : _c("div", [
                          _c("p", { staticClass: "g-alert" }, [
                            _vm._v("Отсутствует шаблон для выбора инструкции") ]),
                          _vm._v(" "),
                          _c("p", [
                            _vm._v(
                              "\n          Пожалуйста, загрузите пользовательский шаблон в настройках аккаунта.\n        "
                            ) ]) ]),
                    _vm._v(" "),
                    _vm.partnerCheckinTemplates &&
                    _vm.partnerCheckinTemplates.length > 0 &&
                    _vm.selectedCheckinTemplate == null
                      ? [
                          _c("p", { staticClass: "g-alert" }, [
                            _vm._v("Выберите шаблон инструкции") ]) ]
                      : _vm._e() ],
                  2
                )
              : _vm._e() ]),
          _vm._v(" "),
          _vm.usingLockService
            ? _c("LocksInfo", { attrs: { apartments: _vm.getApartments } })
            : _vm._e(),
          _vm._v(" "),
          _vm.isSmsServiceAvailable
            ? _c("div", { staticClass: "form__item" }, [
                _c("label", { staticClass: "form__radio-label fwb" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.sentMessageToCustomerOnRegister,
                        expression: "sentMessageToCustomerOnRegister",
                      } ],
                    staticClass: "form__checkbox",
                    attrs: { type: "checkbox" },
                    domProps: {
                      checked: Array.isArray(_vm.sentMessageToCustomerOnRegister)
                        ? _vm._i(_vm.sentMessageToCustomerOnRegister, null) > -1
                        : _vm.sentMessageToCustomerOnRegister,
                    },
                    on: {
                      change: function ($event) {
                        var $$a = _vm.sentMessageToCustomerOnRegister,
                          $$el = $event.target,
                          $$c = $$el.checked ? true : false;
                        if (Array.isArray($$a)) {
                          var $$v = null,
                            $$i = _vm._i($$a, $$v);
                          if ($$el.checked) {
                            $$i < 0 &&
                              (_vm.sentMessageToCustomerOnRegister = $$a.concat([
                                $$v ]));
                          } else {
                            $$i > -1 &&
                              (_vm.sentMessageToCustomerOnRegister = $$a
                                .slice(0, $$i)
                                .concat($$a.slice($$i + 1)));
                          }
                        } else {
                          _vm.sentMessageToCustomerOnRegister = $$c;
                        }
                      },
                    },
                  }),
                  _vm._v("\n      Отправить ссылку автоматически\n    ") ]) ])
            : _vm._e(),
          _vm._v(" "),
          _c("div", { staticClass: "form__item" }, [
            _c("div", { staticClass: "form__cell" }, [
              _c(
                "button",
                {
                  staticClass: "_esw-button",
                  class: {
                    primary:
                      !_vm.isMultipleContract ||
                      !_vm.isMultipleContractConditionsPass() ||
                      !_vm.isMultipleContractServiceAvailable() ||
                      !_vm.isBalanceEnoughtForMultipleContract(),
                  },
                  attrs: {
                    type: "submit",
                    disabled:
                      !_vm.isTemplatesExists ||
                      !_vm.canSentBookingRequest ||
                      !_vm.isTemplateSelected(_vm.selectedTemplate) ||
                      !(_vm.usingLockService
                        ? _vm.isTemplateSelected(_vm.selectedCheckinTemplate)
                        : true),
                  },
                  on: { click: _vm.registerBaseContract },
                },
                [_vm._v("\n        Подписать заказчиком\n      ")]
              ),
              _vm._v(" "),
              _vm.isMultipleContract &&
              _vm.isMultipleContractServiceAvailable() &&
              _vm.isMultipleContractConditionsPass() &&
              _vm.isBalanceEnoughtForMultipleContract()
                ? _c(
                    "button",
                    {
                      staticClass: "_esw-button primary",
                      attrs: {
                        type: "submit",
                        disabled:
                          !_vm.isTemplatesExists ||
                          !_vm.canSentBookingRequest ||
                          !_vm.isTemplateSelected(_vm.selectedTemplate) ||
                          !(_vm.usingLockService
                            ? _vm.isTemplateSelected(_vm.selectedCheckinTemplate)
                            : true),
                      },
                      on: { click: _vm.registerMultipleContract },
                    },
                    [_vm._v("\n        Подписать каждым гостем\n      ")]
                  )
                : _vm._e() ]),
            _vm._v(" "),
            _c("div", { staticClass: "form__cell" }) ]),
          _vm._v(" "),
          _vm.isMultipleContract &&
          _vm.isMultipleContractServiceAvailable() &&
          !_vm.isMultipleContractConditionsPass()
            ? _c("div", [_vm._m(0)])
            : _vm._e() ],
        2
      )
    };
    var __vue_staticRenderFns__$9 = [
      function () {
        var _vm = this;
        var _h = _vm.$createElement;
        var _c = _vm._self._c || _h;
        return _c("p", [
          _vm._v(
            "\n      Если требуется подписание договора каждым гостем, то необходимо"
          ),
          _c("br"),
          _vm._v(
            "добавить\n      в бронь всех гостей с указанием их уникальных номеров телефонов.\n    "
          ) ])
      } ];
    __vue_render__$9._withStripped = true;

      /* style */
      var __vue_inject_styles__$9 = function (inject) {
        if (!inject) { return }
        inject("data-v-38a94b5d_0", { source: "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", map: {"version":3,"sources":[],"names":[],"mappings":"","file":"ContractRegistrationScreen.vue"}, media: undefined });

      };
      /* scoped */
      var __vue_scope_id__$9 = undefined;
      /* module identifier */
      var __vue_module_identifier__$9 = undefined;
      /* functional template */
      var __vue_is_functional_template__$9 = false;
      /* style inject SSR */
      
      /* style inject shadow dom */
      

      
      var __vue_component__$9 = /*#__PURE__*/normalizeComponent(
        { render: __vue_render__$9, staticRenderFns: __vue_staticRenderFns__$9 },
        __vue_inject_styles__$9,
        __vue_script__$9,
        __vue_scope_id__$9,
        __vue_is_functional_template__$9,
        __vue_module_identifier__$9,
        false,
        createInjector,
        undefined,
        undefined
      );

    //

    var script$8 = {
        name: 'WelcomeScreen',
        data: function data() {
          return {
            isRemoteBookingPossible: true,
            isRegistrationScreen: false,
            isRegistrationWithLocks: false,
          };
        },
        components: {
          ContractRegistrationScreen: __vue_component__$9
        },
        computed: {
          contractNumber: function contractNumber() { return this.$eswStore.getters.getContractNumber; },
          clientData: function clientData() { return this.$eswStore.getters.getClientData; },
          // Флаг доступности сервиса. Определяется при загрузке виджета.
          isLockServiceAvailable: function isLockServiceAvailable() { return this.$eswStore.getters.isServiceAvailable('remote_lock_management'); },
          isLockServiceLoaded: function isLockServiceLoaded() { return this.$eswStore.getters.getServiceStatus('remote_lock_management') == 'ok'; },
          lockServiceStatus: function lockServiceStatus() { return this.$eswStore.getters.getServiceStatus('remote_lock_management'); },
          isLockServiceConfigured: function isLockServiceConfigured() { return this.$eswStore.getters.isLocksServiceConfigured; },
        },
        methods: {
          registerContract: function registerContract() {
            this.isRegistrationScreen = true;
            this.$emit('start-registration', {locks: false});
          },
          registerContractWithLocks: function registerContractWithLocks() {
            this.isRegistrationScreen = true;
            this.isRegistrationWithLocks = true;
            this.$emit('start-registration', {locks: true});
          }
        }
    };

    /* script */
    var __vue_script__$8 = script$8;

    /* template */
    var __vue_render__$8 = function () {
      var _vm = this;
      var _h = _vm.$createElement;
      var _c = _vm._self._c || _h;
      return _c(
        "div",
        { staticClass: "_esw-welcome" },
        [
          _c("div", { staticClass: "_esw-section" }, [
            _c("p", [
              _vm._v(
                "ФИО: " +
                  _vm._s(_vm.clientData.lastName) +
                  " " +
                  _vm._s(_vm.clientData.firstName) +
                  " " +
                  _vm._s(_vm.clientData.middleName)
              ) ]),
            _vm._v(" "),
            _c("p", [_vm._v("Номер телефона: " + _vm._s(_vm.clientData.phone))]),
            _vm._v(" "),
            _c("p", [
              _vm._v("Количество гостей: " + _vm._s(_vm.clientData.numberOfGuests)) ]) ]),
          _vm._v(" "),
          !_vm.isRegistrationScreen
            ? [
                _vm.isLockServiceAvailable && !_vm.isLockServiceConfigured
                  ? _c(
                      "div",
                      { staticClass: "_esw-section status _esw-status_orange" },
                      [_vm._m(0)]
                    )
                  : _vm.isLockServiceAvailable && _vm.lockServiceStatus !== "ok"
                  ? _c("div", { staticClass: "_esw-section _esw-status_orange" }, [
                      _c("p", {
                        domProps: {
                          innerHTML: _vm._s(
                            _vm.$tt("locks_status." + _vm.lockServiceStatus)
                          ),
                        },
                      }) ])
                  : !_vm.isLockServiceAvailable
                  ? _c("div", { staticClass: "_esw-section _esw-status_orange" }, [
                      _c("p", [
                        _vm._v("Сервис замков недоступен для данного тарифа") ]) ])
                  : _vm._e(),
                _vm._v(" "),
                _c("div", { staticClass: "_esw-section" }, [
                  _c("div", { staticClass: "_esw-grid" }, [
                    _c(
                      "button",
                      {
                        staticClass: "_esw-button primary",
                        attrs: { disabled: !_vm.isRemoteBookingPossible },
                        on: { click: _vm.registerContract },
                      },
                      [_vm._v("Удаленное подписание документов")]
                    ),
                    _vm._v(" "),
                    _c(
                      "button",
                      {
                        staticClass: "_esw-button primary",
                        attrs: {
                          disabled: !(
                            _vm.isLockServiceAvailable &&
                            _vm.isLockServiceLoaded &&
                            _vm.isLockServiceConfigured
                          ),
                        },
                        on: { click: _vm.registerContractWithLocks },
                      },
                      [_vm._v("Удалённое заселение с замками")]
                    ) ]) ]) ]
            : _vm._e(),
          _vm._v(" "),
          _vm.isRegistrationScreen
            ? _c("ContractRegistrationScreen", {
                attrs: { usingLockService: _vm.isRegistrationWithLocks },
              })
            : _vm._e() ],
        2
      )
    };
    var __vue_staticRenderFns__$8 = [
      function () {
        var _vm = this;
        var _h = _vm.$createElement;
        var _c = _vm._self._c || _h;
        return _c("p", [
          _vm._v("\n        Ваш аккаунт не синхронизирован с TTLock.\n        "),
          _c("br"),
          _vm._v(
            "\n        В данный момент доступно только подписание документов.\n      "
          ) ])
      } ];
    __vue_render__$8._withStripped = true;

      /* style */
      var __vue_inject_styles__$8 = function (inject) {
        if (!inject) { return }
        inject("data-v-6088ca50_0", { source: "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", map: {"version":3,"sources":[],"names":[],"mappings":"","file":"WelcomeScreen.vue"}, media: undefined });

      };
      /* scoped */
      var __vue_scope_id__$8 = undefined;
      /* module identifier */
      var __vue_module_identifier__$8 = undefined;
      /* functional template */
      var __vue_is_functional_template__$8 = false;
      /* style inject SSR */
      
      /* style inject shadow dom */
      

      
      var __vue_component__$8 = /*#__PURE__*/normalizeComponent(
        { render: __vue_render__$8, staticRenderFns: __vue_staticRenderFns__$8 },
        __vue_inject_styles__$8,
        __vue_script__$8,
        __vue_scope_id__$8,
        __vue_is_functional_template__$8,
        __vue_module_identifier__$8,
        false,
        createInjector,
        undefined,
        undefined
      );

    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //

    var script$7 = {
        props: [
            'error'
        ],
        mounted: function mounted() {
            if(window._eswc && window._eswc.dev) { console.log(this.error); }
        },
        data: function data() {
            return {};
        }
    };

    /* script */
    var __vue_script__$7 = script$7;

    /* template */
    var __vue_render__$7 = function () {
      var _vm = this;
      var _h = _vm.$createElement;
      var _c = _vm._self._c || _h;
      return _c("div", [
        _c("h4", [_vm._v("Ошибка в работе виджета")]),
        _vm._v(" "),
        _c("p", {
          staticClass: "g-alert",
          domProps: { innerHTML: _vm._s(_vm.error.message) },
        }),
        _vm._v(" "),
        _vm._m(0) ])
    };
    var __vue_staticRenderFns__$7 = [
      function () {
        var _vm = this;
        var _h = _vm.$createElement;
        var _c = _vm._self._c || _h;
        return _c("p", [
          _vm._v(
            "\n        Если возникли вопросы, пожалуйста, свяжитесь с нами удобным для Вас способом:\n        "
          ),
          _c("br"),
          _vm._v("\n        Эл. почта: "),
          _c("a", { attrs: { href: "email:tech@euler.solutions" } }, [
            _vm._v("tech@euler.solutions") ]),
          _vm._v(" "),
          _c("br"),
          _vm._v("\n        Горячая линия: "),
          _c("a", { attrs: { href: "tel:88002013063" } }, [
            _vm._v("8(800)201-30-63") ]) ])
      } ];
    __vue_render__$7._withStripped = true;

      /* style */
      var __vue_inject_styles__$7 = function (inject) {
        if (!inject) { return }
        inject("data-v-a58bed26_0", { source: "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", map: {"version":3,"sources":[],"names":[],"mappings":"","file":"FatalWidgetError.vue"}, media: undefined });

      };
      /* scoped */
      var __vue_scope_id__$7 = undefined;
      /* module identifier */
      var __vue_module_identifier__$7 = undefined;
      /* functional template */
      var __vue_is_functional_template__$7 = false;
      /* style inject SSR */
      
      /* style inject shadow dom */
      

      
      var __vue_component__$7 = /*#__PURE__*/normalizeComponent(
        { render: __vue_render__$7, staticRenderFns: __vue_staticRenderFns__$7 },
        __vue_inject_styles__$7,
        __vue_script__$7,
        __vue_scope_id__$7,
        __vue_is_functional_template__$7,
        __vue_module_identifier__$7,
        false,
        createInjector,
        undefined,
        undefined
      );

    //
    //
    //
    //
    //
    //
    //
    //
    //
    //

    var script$6 = {
        name: 'BillingWidgetError',
        data: function data() { return {} },
    };

    /* script */
    var __vue_script__$6 = script$6;

    /* template */
    var __vue_render__$6 = function () {
      var _vm = this;
      var _h = _vm.$createElement;
      _vm._self._c || _h;
      return _vm._m(0)
    };
    var __vue_staticRenderFns__$6 = [
      function () {
        var _vm = this;
        var _h = _vm.$createElement;
        var _c = _vm._self._c || _h;
        return _c("div", [
          _c("h4", [_vm._v("Услуги eSigner недоступны:")]),
          _vm._v(" "),
          _c("p", { staticClass: "g-alert" }, [
            _vm._v("\n      Пакет услуг закончился или истек пробный период.\n  ") ]),
          _vm._v(
            "\n  Если вы недавно приобрели новый пакет услуг, попробуйте обновить страницу.\n"
          ) ])
      } ];
    __vue_render__$6._withStripped = true;

      /* style */
      var __vue_inject_styles__$6 = function (inject) {
        if (!inject) { return }
        inject("data-v-586711ab_0", { source: "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", map: {"version":3,"sources":[],"names":[],"mappings":"","file":"BillingWidgetError.vue"}, media: undefined });

      };
      /* scoped */
      var __vue_scope_id__$6 = undefined;
      /* module identifier */
      var __vue_module_identifier__$6 = undefined;
      /* functional template */
      var __vue_is_functional_template__$6 = false;
      /* style inject SSR */
      
      /* style inject shadow dom */
      

      
      var __vue_component__$6 = /*#__PURE__*/normalizeComponent(
        { render: __vue_render__$6, staticRenderFns: __vue_staticRenderFns__$6 },
        __vue_inject_styles__$6,
        __vue_script__$6,
        __vue_scope_id__$6,
        __vue_is_functional_template__$6,
        __vue_module_identifier__$6,
        false,
        createInjector,
        undefined,
        undefined
      );

    //
    //
    //
    //

    var script$5 = {
        props: ['warning'],
        data: function data() {
            return {};
        }
    };

    /* script */
    var __vue_script__$5 = script$5;

    /* template */
    var __vue_render__$5 = function () {
      var _vm = this;
      var _h = _vm.$createElement;
      var _c = _vm._self._c || _h;
      return _c("div", {
        staticClass: "_es-warning",
        domProps: { innerHTML: _vm._s(_vm.warning) },
      })
    };
    var __vue_staticRenderFns__$5 = [];
    __vue_render__$5._withStripped = true;

      /* style */
      var __vue_inject_styles__$5 = function (inject) {
        if (!inject) { return }
        inject("data-v-1fae0926_0", { source: "\n._es-warning {\n    color: orange;\n    padding: 4px;\n    border: 1px solid grey;\n    border-radius: 4px;\n    text-align: center;\n    margin-bottom: 4px;\n}\n", map: {"version":3,"sources":["/Users/mike/Dev/repo/esigner-widget/src/components/WidgetWarning.vue"],"names":[],"mappings":";AAcA;IACA,aAAA;IACA,YAAA;IACA,sBAAA;IACA,kBAAA;IACA,kBAAA;IACA,kBAAA;AACA","file":"WidgetWarning.vue","sourcesContent":["<template>\n  <div class=\"_es-warning\" v-html=\"warning\"></div>\n</template>\n\n<script>\nexport default {\n    props: ['warning'],\n    data() {\n        return {};\n    }\n}\n</script>\n\n<style>\n    ._es-warning {\n        color: orange;\n        padding: 4px;\n        border: 1px solid grey;\n        border-radius: 4px;\n        text-align: center;\n        margin-bottom: 4px;\n    }\n</style>"]}, media: undefined });

      };
      /* scoped */
      var __vue_scope_id__$5 = undefined;
      /* module identifier */
      var __vue_module_identifier__$5 = undefined;
      /* functional template */
      var __vue_is_functional_template__$5 = false;
      /* style inject SSR */
      
      /* style inject shadow dom */
      

      
      var __vue_component__$5 = /*#__PURE__*/normalizeComponent(
        { render: __vue_render__$5, staticRenderFns: __vue_staticRenderFns__$5 },
        __vue_inject_styles__$5,
        __vue_script__$5,
        __vue_scope_id__$5,
        __vue_is_functional_template__$5,
        __vue_module_identifier__$5,
        false,
        createInjector,
        undefined,
        undefined
      );

    //

    var script$4 = {
      name: "ContractInfoScreen",
      data: function data() {
        return {
            accordion: {},
        };
      },
      computed: {
        contractState: function contractState() {
          return this.$eswStore.getters.getContractState;
        },
        timezone: function timezone() {
          return this.$eswStore.getters.getWidgetTimezone;
        },
        /**
         * Флаг доступности сервиса отправки СМС
         * 
         * Проверяет доступность сервиса отправки СМС на момент создания бронирования
         */
        isCanSentLink: function isCanSentLink() {
          var state = this.$eswStore.getters.getContractState;

          var available = state.available_services.includes(PartnerStoreHelper.SERVICES.SEND_SMS_LINK_TO_CLIENT); 

          return available;
        },
      },
      methods: {
        sendLink: function sendLink() {
          var this$1$1 = this;

          this.$eswStore.dispatch("togglePreloader", true);
          this.$eswStore.dispatch("sendLinkToGuest").then(
            function (r) {
              // Анимация загрузки уйдет при изменении состояния контракта (автоматически)
            },
            function () {
              this$1$1.$eswStore.dispatch(
                "handleWidgetWarning",
                "Ошибка при отправке СМС"
              );
              this$1$1.$eswStore.dispatch("togglePreloader", false);
            }
          );
        },
        copyLink: function copyLink(link) {
          navigator.clipboard.writeText(link).then(
            function () {
              /* clipboard successfully set */
              // window.alert('Success! The text was copied to your clipboard')
            },
            function () {
              /* clipboard write failed */
              // window.alert('Opps! Your browser does not support the Clipboard API')
            }
          );
        },
        isContractSigned: function isContractSigned(contract) {
          return PartnerStoreHelper.signedStatuses.includes(contract.code);
        },
        editGuestData: function editGuestData(contract) {
          contract.editing = true;
          this.$forceUpdate();
        },
        saveGuestData: function saveGuestData(contract) {
          var this$1$1 = this;

          var input = this.$refs["new_phone_" + contract.bnovo_customer_id];
          var phoneError =
            this.$refs["edit_phone_error_" + contract.bnovo_customer_id][0];
          var replaceRegex = /[^\d^\+]+/g;
          var value = input[0].value.replaceAll(replaceRegex, "");
          var isValid = false;

          var contractsWithSameNumber = this.contractState.bookings.filter(function (el) {
            return el.phone.replaceAll(replaceRegex, "") == value;
          });

          var phoneRegex = PartnerStoreHelper.CONTRACT_PHONE_REGEX;
          if (!value.match(phoneRegex))
            { phoneError.innerHTML =
              "Пожалуйста, укажите российский номер телефона (с префиксом +7 или 8)"; }
          else if (value == contract.phone)
            { phoneError.innerHTML = "Номер не был изменен"; }
          else if (contractsWithSameNumber.length > 0)
            { phoneError.innerHTML = "Совпадает с номером другого гостя"; }
          else { isValid = true; }

          if (isValid) {
            // Анимация загрузки уйдет при изменении состояния контракта (автоматически)
            this.$eswStore.dispatch("togglePreloader", true);
            this.$eswStore
              .dispatch("changeGuestPhone", [value, contract])
              .then(function (result) {
                switch (result.status) {
                  case "ok":
                    contract.phone = value;
                    contract.editing = false;
                    this$1$1.$forceUpdate();
                    break;
                }
              });
          }
        },
        cancelEditGuestData: function cancelEditGuestData(contract) {
          contract.editing = false;
          this.$forceUpdate();
        },

        /**
         * Группировка пасскодов по апартаментам для поблочного отображения
         * bookingInfo.passcodes -  массив пасскодов, пришедший с бекенда
         */
        contractPasscodes: function contractPasscodes(contract) {
          var passcodes = contract.passcodes || [];
          var grouped_passcodes = [];
          var result = [];

          for (var i = 0; i < passcodes.length; i++) {
            var el = passcodes[i];
            var room_id = (el.room_id) + "_" + (el.period_index);

            // Если массив с пасскодами лдя апартаментов уже есть в объекте группировки, то добавляем пасскод к апартаментам
            // Как один из кодов апартаментов
            if (grouped_passcodes[room_id]) { grouped_passcodes[room_id].push(el); }
            else { grouped_passcodes[room_id] = [el]; }
          }

          if (contract.initial_apartments) {
            var keys = Object.keys(contract.initial_apartments);

            for (var i$1 = 0; i$1 < keys.length; i$1++) {
              var el$1 = contract.initial_apartments[keys[i$1]];
              var passcodes$1 = grouped_passcodes[((el$1.room_id) + "_" + i$1)];

              if (passcodes$1)
                { for (var j = 0; j < passcodes$1.length; j++) {
                  var pc = passcodes$1[j];
                  result.push({
                    id: pc.id,
                    name:
                      el$1.room_name +
                      " / " +
                      el$1.room_type_name +
                      " (" +
                      pc.lock_name +
                      ")",
                    status_id: pc.status_id,
                    passcode: pc.passcode,
                    is_algorithmic: pc.is_algorithmic,
                    start_datetime: pc.start_datetime,
                    end_datetime: pc.end_datetime,
                  });
                } }
            }
          }

          return result;
        },
        formatDatetime: function formatDatetime(datetime, timezone) {
          return new Date(datetime).toLocaleDateString([], {
            hour: "2-digit",
            minute: "2-digit",
            timeZone: timezone,
          });
        },
        formatDate: function formatDate(date) {
          return new Date(date).toLocaleDateString([], {});
        },
        formatPasscodeStatus: function formatPasscodeStatus(code, status) {
          var content = "Ошибка";

          switch (status) {
            case "set":
              content = code;
              break;
            case "canceling":
              content = this.$tt("passcode_status." + status); // В процессе отмены
              break;
            case "canceled":
              content = this.$tt("passcode_status." + status); // Отменен
              break;
            case "failed":
              content = this.$tt("passcode_status." + status); // "Ошибка установки";
              break;
            case "expired":
              content = code + " (" + (this.$tt("passcode_status." + status)) + ")"; // "Истек";
              break;
            default:
              content = this.$tt("passcode_status.default");
              break;
          }

          return content;
        },
        toggleAccordion: function toggleAccordion(customerId) {
            Vue.set(this.accordion, customerId, !this.accordion[customerId]);
        },
        shiftPasscodeHour: async function shiftPasscodeHour(passcodeId, shiftType) {
          var this$1$1 = this;

          var $this = this;
          var showError = function (pcId, error) {
            $this.$refs["passcode_error_block_" + pcId][0].innerHTML = $this.$tt(
              "passcode_error." + error
            );
          };
          
          var cleanError = function(pcId) {
            $this.$refs["passcode_error_block_" + pcId][0].innerHTML = '';
          };

          var data = {
            id: passcodeId,
            shiftType: shiftType,
          };
          this.$eswStore.dispatch("togglePreloader", true);
          this.$eswStore.dispatch("shiftPasscodeHour", data).then(function (result) {
            if (result.status == "error") {
              switch (result.code) {
                case "lock_wo_gateway":
                case "gateway_busy":
                case "gateway_is_offline":
                case "ttlock_timeout":
                  showError(passcodeId, result.code);
                  break;

                default:
                  showError(passcodeId, "unexpected");
                  break;
              }
            } else {
              cleanError(passcodeId);
            }
            this$1$1.$eswStore.dispatch("togglePreloader", false);
          });
        },
      },
    };

    /* script */
    var __vue_script__$4 = script$4;

    /* template */
    var __vue_render__$4 = function () {
      var _vm = this;
      var _h = _vm.$createElement;
      var _c = _vm._self._c || _h;
      return _c("div", [
        _c(
          "div",
          { staticClass: "_esw-section _esw-contracts" },
          _vm._l(_vm.contractState.bookings, function (contract) {
            return _c(
              "div",
              {
                key: contract.code + "_" + contract.phone,
                staticClass: "_esw-contract-card _esw-accordion",
                class: {
                  "_esw-contract-card_signed": _vm.isContractSigned(contract),
                  "_esw-accordion-show": _vm.accordion[contract.bnovo_customer_id],
                },
                attrs: { id: "signer_booking_" + contract.bnovo_customer_id },
              },
              [
                _vm.isContractSigned(contract)
                  ? _c("div", {
                      staticClass: "_esw-accordion-icon",
                      class: {
                        "i-u-arr": _vm.accordion[contract.bnovo_customer_id],
                        "i-d-arr": !_vm.accordion[contract.bnovo_customer_id],
                      },
                      on: {
                        click: function ($event) {
                          return _vm.toggleAccordion(contract.bnovo_customer_id)
                        },
                      },
                    })
                  : _vm._e(),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "m-s" },
                  [
                    _c("b", {
                      domProps: {
                        innerHTML: _vm._s(_vm.$tt("status." + contract.code)),
                      },
                    }),
                    _vm._v(" "),
                    _c("br"),
                    _vm._v(
                      "\n        " +
                        _vm._s(contract.first_name) +
                        " " +
                        _vm._s(contract.last_name) +
                        "\n        " +
                        _vm._s(contract.middle_name) +
                        "\n\n        "
                    ),
                    !_vm.isContractSigned(contract)
                      ? [
                          _c(
                            "p",
                            {
                              staticStyle: { cursor: "pointer" },
                              attrs: { title: "Скопировать" },
                              on: {
                                click: function ($event) {
                                  return _vm.copyLink(contract.form_url)
                                },
                              },
                            },
                            [
                              _vm._v(
                                "\n            Ссылка: " +
                                  _vm._s(contract.form_url) +
                                  "  "
                              ),
                              _c("i", { staticClass: "i-open" }) ]
                          ) ]
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.isContractSigned(contract)
                      ? _c("span", [
                          _c("br"),
                          _vm._v("\n          тел.: "),
                          _c("b", [_vm._v(_vm._s(contract.phone))]) ])
                      : !contract.editing
                      ? _c("span", [
                          _vm._v("\n          тел.:\n          "),
                          _c(
                            "a",
                            {
                              staticClass: "signer_update_guest_show",
                              attrs: {
                                "data-bnovo-customer-id":
                                  contract.bnovo_customer_id,
                              },
                              on: {
                                click: function ($event) {
                                  return _vm.editGuestData(contract)
                                },
                              },
                            },
                            [
                              _c("b", { staticClass: "g-dotted" }, [
                                _vm._v(_vm._s(contract.phone)) ]),
                              _vm._v(" "),
                              _c("i", { staticClass: "i-edit" }) ]
                          ) ])
                      : _vm._e(),
                    _vm._v(" "),
                    !_vm.isContractSigned(contract) && contract.editing
                      ? _c("div", [
                          _c("input", {
                            ref: "new_phone_" + contract.bnovo_customer_id,
                            refInFor: true,
                            staticClass: "form__input input ml10",
                            attrs: { type: "tel" },
                            domProps: { value: contract.phone },
                          }),
                          _vm._v(" "),
                          _c(
                            "button",
                            {
                              staticClass:
                                "\n              signer_update_guest_submit\n              form__button\n              button\n              m-s m-primary\n            ",
                              attrs: {
                                "data-bnovo-customer-id":
                                  contract.bnovo_customer_id,
                              },
                              on: {
                                click: function ($event) {
                                  return _vm.saveGuestData(contract)
                                },
                              },
                            },
                            [_vm._v("\n            Сохранить\n          ")]
                          ),
                          _vm._v(" "),
                          _c(
                            "button",
                            {
                              staticClass:
                                "\n              signer_update_guest_cancel\n              form__button\n              button\n              m-s m-secondary\n            ",
                              on: {
                                click: function ($event) {
                                  return _vm.cancelEditGuestData(contract)
                                },
                              },
                            },
                            [_vm._v("\n            Отменить\n          ")]
                          ) ])
                      : _vm._e(),
                    _vm._v(" "),
                    contract.editing
                      ? _c("small", {
                          ref: "edit_phone_error_" + contract.bnovo_customer_id,
                          refInFor: true,
                          staticClass: "g-alert",
                        })
                      : _vm._e() ],
                  2
                ),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    staticClass: "_esw-accordion-content",
                    attrs: {
                      id: "signer_booking_details_" + contract.bnovo_customer_id,
                    },
                  },
                  [
                    _vm.isContractSigned(contract)
                      ? [
                          _vm._v(
                            "\n          Серия и номер паспорта: " +
                              _vm._s(contract.passport_series) +
                              " " +
                              _vm._s(contract.passport_number) +
                              "\n          "
                          ),
                          _c("br"),
                          _vm._v(
                            "\n          Дата выдачи: " +
                              _vm._s(_vm.formatDate(contract.passport_date)) +
                              "\n          "
                          ),
                          _c("br"),
                          _vm._v(
                            "\n          Кем выдан: " +
                              _vm._s(contract.passport_issued_by) +
                              "\n          "
                          ),
                          _c("br"),
                          _vm._v(" "),
                          _c("br"),
                          _vm._v(" "),
                          _c("h4", [_vm._v("Информация по подписанию:")]),
                          _vm._v(" "),
                          _c("p", [
                            _vm._v(
                              "\n            Дата подписания: " +
                                _vm._s(
                                  _vm.formatDatetime(
                                    contract.sign_datetime,
                                    _vm.timezone
                                  )
                                ) +
                                "\n          "
                            ) ]),
                          _vm._v(" "),
                          _c("h4", [_vm._v("Подписанные документы:")]),
                          _vm._v(" "),
                          _vm._l(contract.signed_files, function (doc) {
                            return _c("div", { key: doc.title }, [
                              _c(
                                "a",
                                { attrs: { target: "_blank", href: doc.path } },
                                [
                                  _vm._v(
                                    "\n              " +
                                      _vm._s(doc.title) +
                                      "\n              "
                                  ) ]
                              ) ])
                          }),
                          _vm._v(" "),
                          contract.is_lock_management && contract.passcodes
                            ? [
                                _c("br"),
                                _vm._v(" "),
                                _c("h4", [
                                  _vm._v(
                                    "Статус генерации кодов доступов для апартаментов"
                                  ) ]),
                                _vm._v(" "),
                                _vm._l(
                                  _vm.contractPasscodes(contract),
                                  function (pc) {
                                    return _c(
                                      "div",
                                      {
                                        key:
                                          pc.id +
                                          "_" +
                                          pc.status_id +
                                          "_" +
                                          pc.passcode,
                                        staticClass: "s_passcode",
                                      },
                                      [
                                        _c(
                                          "h5",
                                          { staticClass: "s_passcode-title" },
                                          [_vm._v(_vm._s(pc.name))]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "p",
                                          { staticClass: "s_passcode-content" },
                                          [
                                            _vm._v(
                                              "\n                Код от замка:\n                "
                                            ),
                                            _c(
                                              "span",
                                              { staticClass: "s_passcode-end" },
                                              [
                                                _vm._v(
                                                  _vm._s(
                                                    _vm.formatPasscodeStatus(
                                                      pc.passcode,
                                                      pc.status_id
                                                    )
                                                  )
                                                ) ]
                                            ),
                                            _vm._v(" "),
                                            _c("br"),
                                            _vm._v(" "),
                                            !["cancelling", "canceled"].includes(
                                              pc.status_id
                                            )
                                              ? _c(
                                                  "span",
                                                  {
                                                    staticClass:
                                                      "s-passcode_availability",
                                                  },
                                                  [
                                                    _vm._v(
                                                      "\n                  Действует с:\n                  "
                                                    ),
                                                    _c(
                                                      "span",
                                                      {
                                                        staticClass:
                                                          "s_passcode-start",
                                                      },
                                                      [
                                                        _vm._v(
                                                          _vm._s(
                                                            _vm.formatDatetime(
                                                              pc.start_datetime,
                                                              _vm.timezone
                                                            )
                                                          )
                                                        ) ]
                                                    ),
                                                    _vm._v(" "),
                                                    !pc.is_algorithmic &&
                                                    pc.status_id == "set"
                                                      ? _c(
                                                          "a",
                                                          {
                                                            staticClass:
                                                              "s_passcode-shift_hour",
                                                            attrs: {
                                                              href: "javascript:void(0);",
                                                            },
                                                            on: {
                                                              click: function (
                                                                $event
                                                              ) {
                                                                return _vm.shiftPasscodeHour(
                                                                  pc.id,
                                                                  "start_minus_hour"
                                                                )
                                                              },
                                                            },
                                                          },
                                                          [
                                                            _vm._v(
                                                              "\n                    (-1 час)\n                  "
                                                            ) ]
                                                        )
                                                      : _vm._e(),
                                                    _vm._v(
                                                      "\n                  по\n                  "
                                                    ),
                                                    _c(
                                                      "span",
                                                      {
                                                        staticClass:
                                                          "s_passcode-end",
                                                      },
                                                      [
                                                        _vm._v(
                                                          _vm._s(
                                                            _vm.formatDatetime(
                                                              pc.end_datetime,
                                                              _vm.timezone
                                                            )
                                                          )
                                                        ) ]
                                                    ),
                                                    _vm._v(" "),
                                                    !pc.is_algorithmic &&
                                                    pc.status_id == "set"
                                                      ? _c(
                                                          "a",
                                                          {
                                                            staticClass:
                                                              "s_passcode-shift_hour",
                                                            attrs: {
                                                              href: "javascript:void(0);",
                                                            },
                                                            on: {
                                                              click: function (
                                                                $event
                                                              ) {
                                                                return _vm.shiftPasscodeHour(
                                                                  pc.id,
                                                                  "end_plus_hour"
                                                                )
                                                              },
                                                            },
                                                          },
                                                          [
                                                            _vm._v(
                                                              "\n                    (+1 час)\n                  "
                                                            ) ]
                                                        )
                                                      : _vm._e() ]
                                                )
                                              : _vm._e(),
                                            _vm._v(" "),
                                            _c("span", {
                                              ref: "passcode_error_block_" + pc.id,
                                              refInFor: true,
                                              staticClass: "s_passcode-error",
                                            }) ]
                                        ) ]
                                    )
                                  }
                                ) ]
                            : _vm._e() ]
                      : _vm._e() ],
                  2
                ) ]
            )
          }),
          0
        ),
        _vm._v(" "),
        _vm.contractState.code == "new" && _vm.isCanSentLink
          ? _c("div", { staticClass: "form__item" }, [
              _c("div", { staticClass: "form__cell" }, [
                _c(
                  "a",
                  {
                    staticClass: "form__button button m-s",
                    attrs: { href: "#" },
                    on: { click: _vm.sendLink },
                  },
                  [_vm._v("Отправить приглашение")]
                ) ]) ])
          : _vm._e() ])
    };
    var __vue_staticRenderFns__$4 = [];
    __vue_render__$4._withStripped = true;

      /* style */
      var __vue_inject_styles__$4 = function (inject) {
        if (!inject) { return }
        inject("data-v-69093da2_0", { source: "\n.icon-action {\n  cursor: pointer;\n}\nsmall.alert.m-red:empty {\n  display: none;\n}\n.s_passcode-error:not(:empty) {\n  color: red;\n  display: block;\n}\n", map: {"version":3,"sources":["/Users/mike/Dev/repo/esigner-widget/src/components/ContractInfoScreen.vue"],"names":[],"mappings":";AAgaA;EACA,eAAA;AACA;AAEA;EACA,aAAA;AACA;AAEA;EACA,UAAA;EACA,cAAA;AACA","file":"ContractInfoScreen.vue","sourcesContent":["<template>\n  <div>\n    <div class=\"_esw-section _esw-contracts\">\n      <div\n        v-for=\"contract in contractState.bookings\"\n        v-bind:key=\"contract.code + '_' + contract.phone\"\n        class=\"_esw-contract-card _esw-accordion\"\n        :id=\"'signer_booking_' + contract.bnovo_customer_id\"\n        :class=\"{\n            '_esw-contract-card_signed': isContractSigned(contract),\n            '_esw-accordion-show': accordion[contract.bnovo_customer_id]\n        }\"\n      >\n        <div\n            v-if=\"isContractSigned(contract)\"\n            class=\"_esw-accordion-icon\"\n            :class=\"{\n                'i-u-arr': accordion[contract.bnovo_customer_id],\n                'i-d-arr': !accordion[contract.bnovo_customer_id]\n            }\"\n            v-on:click=\"toggleAccordion(contract.bnovo_customer_id)\"\n        >\n        </div>\n        <div class=\"m-s\">\n          <b v-html=\"$tt('status.' + contract.code)\"></b>\n          <br />\n          {{ contract.first_name }} {{ contract.last_name }}\n          {{ contract.middle_name }}\n\n          <template v-if=\"!isContractSigned(contract)\">\n            <p\n              style=\"cursor: pointer\"\n              v-on:click=\"copyLink(contract.form_url)\"\n              title=\"Скопировать\"\n            >\n              Ссылка: {{ contract.form_url }}&nbsp;&nbsp;<i class=\"i-open\"></i>\n            </p>\n          </template>\n\n          <span v-if=\"isContractSigned(contract)\">\n            <br />\n            тел.: <b>{{ contract.phone }}</b>\n          </span>\n          <span v-else-if=\"!contract.editing\">\n            тел.:\n            <a\n              class=\"signer_update_guest_show\"\n              :data-bnovo-customer-id=\"contract.bnovo_customer_id\"\n              v-on:click=\"editGuestData(contract)\"\n            >\n              <b class=\"g-dotted\">{{ contract.phone }}</b\n              >&nbsp;<i class=\"i-edit\"></i>\n            </a>\n          </span>\n\n          <div v-if=\"!isContractSigned(contract) && contract.editing\">\n            <input\n              type=\"tel\"\n              class=\"form__input input ml10\"\n              :ref=\"'new_phone_' + contract.bnovo_customer_id\"\n              :value=\"contract.phone\"\n            />\n            <button\n              class=\"\n                signer_update_guest_submit\n                form__button\n                button\n                m-s m-primary\n              \"\n              :data-bnovo-customer-id=\"contract.bnovo_customer_id\"\n              v-on:click=\"saveGuestData(contract)\"\n            >\n              Сохранить\n            </button>\n            <button\n              class=\"\n                signer_update_guest_cancel\n                form__button\n                button\n                m-s m-secondary\n              \"\n              v-on:click=\"cancelEditGuestData(contract)\"\n            >\n              Отменить\n            </button>\n          </div>\n          <small\n            v-if=\"contract.editing\"\n            class=\"g-alert\"\n            :ref=\"'edit_phone_error_' + contract.bnovo_customer_id\"\n          ></small>\n        </div>\n\n        <div class=\"_esw-accordion-content\" :id=\"'signer_booking_details_' + contract.bnovo_customer_id\">\n          <template v-if=\"isContractSigned(contract)\">\n            Серия и номер паспорта: {{ contract.passport_series }} {{ contract.passport_number }}\n            <br />\n            Дата выдачи: {{ formatDate(contract.passport_date) }}\n            <br />\n            Кем выдан: {{ contract.passport_issued_by }}\n            <br />\n            <br />\n            <h4>Информация по подписанию:</h4>\n            <p>\n              Дата подписания: {{ formatDatetime(contract.sign_datetime, timezone) }}\n            </p>\n            <h4>Подписанные документы:</h4>\n            <div v-for=\"doc in contract.signed_files\" v-bind:key=\"doc.title\">\n                <a target=\"_blank\" :href=\"doc.path\">\n                {{ doc.title }}\n                </a>\n            </div>\n            <template v-if=\"contract.is_lock_management && contract.passcodes\">\n              <br />\n              <h4>Статус генерации кодов доступов для апартаментов</h4>\n              <div\n                v-for=\"pc in contractPasscodes(contract)\"\n                :key=\"pc.id + '_' + pc.status_id + '_' + pc.passcode\"\n                class=\"s_passcode\"\n              >\n                <h5 class=\"s_passcode-title\">{{ pc.name }}</h5>\n                <p class=\"s_passcode-content\">\n                  Код от замка:\n                  <span class=\"s_passcode-end\">{{\n                    formatPasscodeStatus(pc.passcode, pc.status_id)\n                  }}</span>\n                  <br />\n                  <span\n                    v-if=\"!['cancelling', 'canceled'].includes(pc.status_id)\"\n                    class=\"s-passcode_availability\"\n                  >\n                    Действует с:\n                    <span class=\"s_passcode-start\">{{\n                      formatDatetime(pc.start_datetime, timezone)\n                    }}</span>\n                    <a\n                      v-if=\"!pc.is_algorithmic && pc.status_id == 'set'\"\n                      href=\"javascript:void(0);\"\n                      class=\"s_passcode-shift_hour\"\n                      v-on:click=\"shiftPasscodeHour(pc.id, 'start_minus_hour')\"\n                    >\n                      (-1 час)\n                    </a>\n                    по\n                    <span class=\"s_passcode-end\">{{\n                      formatDatetime(pc.end_datetime, timezone)\n                    }}</span>\n                    <a\n                      v-if=\"!pc.is_algorithmic && pc.status_id == 'set'\"\n                      href=\"javascript:void(0);\"\n                      class=\"s_passcode-shift_hour\"\n                      v-on:click=\"shiftPasscodeHour(pc.id, 'end_plus_hour')\"\n                    >\n                      (+1 час)\n                    </a>\n                  </span>\n                  <span\n                    class=\"s_passcode-error\"\n                    :ref=\"'passcode_error_block_' + pc.id\"\n                  ></span>\n                </p>\n              </div>\n            </template>\n          </template>\n        </div>\n      </div>\n    </div>\n    <div v-if=\"contractState.code == 'new' && isCanSentLink\" class=\"form__item\">\n      <div class=\"form__cell\">\n        <a href=\"#\" class=\"form__button button m-s\" v-on:click=\"sendLink\"\n          >Отправить приглашение</a\n        >\n      </div>\n    </div>\n  </div>\n</template>\n\n<script>\nimport ESignerHelper from \"../store/partners/bnovo_helper\";\n\nexport default {\n  name: \"ContractInfoScreen\",\n  data() {\n    return {\n        accordion: {},\n    };\n  },\n  computed: {\n    contractState() {\n      return this.$eswStore.getters.getContractState;\n    },\n    timezone() {\n      return this.$eswStore.getters.getWidgetTimezone;\n    },\n    /**\n     * Флаг доступности сервиса отправки СМС\n     * \n     * Проверяет доступность сервиса отправки СМС на момент создания бронирования\n     */\n    isCanSentLink() {\n      let state = this.$eswStore.getters.getContractState;\n\n      let available = state.available_services.includes(ESignerHelper.SERVICES.SEND_SMS_LINK_TO_CLIENT); \n\n      return available;\n    },\n  },\n  methods: {\n    sendLink() {\n      this.$eswStore.dispatch(\"togglePreloader\", true);\n      this.$eswStore.dispatch(\"sendLinkToGuest\").then(\n        (r) => {\n          // Анимация загрузки уйдет при изменении состояния контракта (автоматически)\n        },\n        () => {\n          this.$eswStore.dispatch(\n            \"handleWidgetWarning\",\n            \"Ошибка при отправке СМС\"\n          );\n          this.$eswStore.dispatch(\"togglePreloader\", false);\n        }\n      );\n    },\n    copyLink(link) {\n      navigator.clipboard.writeText(link).then(\n        function () {\n          /* clipboard successfully set */\n          // window.alert('Success! The text was copied to your clipboard')\n        },\n        function () {\n          /* clipboard write failed */\n          // window.alert('Opps! Your browser does not support the Clipboard API')\n        }\n      );\n    },\n    isContractSigned(contract) {\n      return ESignerHelper.signedStatuses.includes(contract.code);\n    },\n    editGuestData(contract) {\n      contract.editing = true;\n      this.$forceUpdate();\n    },\n    saveGuestData(contract) {\n      let input = this.$refs[\"new_phone_\" + contract.bnovo_customer_id];\n      let phoneError =\n        this.$refs[\"edit_phone_error_\" + contract.bnovo_customer_id][0];\n      let replaceRegex = /[^\\d^\\+]+/g;\n      let value = input[0].value.replaceAll(replaceRegex, \"\");\n      let isValid = false;\n\n      let contractsWithSameNumber = this.contractState.bookings.filter((el) => {\n        return el.phone.replaceAll(replaceRegex, \"\") == value;\n      });\n\n      let phoneRegex = ESignerHelper.CONTRACT_PHONE_REGEX;\n      if (!value.match(phoneRegex))\n        phoneError.innerHTML =\n          \"Пожалуйста, укажите российский номер телефона (с префиксом +7 или 8)\";\n      else if (value == contract.phone)\n        phoneError.innerHTML = \"Номер не был изменен\";\n      else if (contractsWithSameNumber.length > 0)\n        phoneError.innerHTML = \"Совпадает с номером другого гостя\";\n      else isValid = true;\n\n      if (isValid) {\n        // Анимация загрузки уйдет при изменении состояния контракта (автоматически)\n        this.$eswStore.dispatch(\"togglePreloader\", true);\n        this.$eswStore\n          .dispatch(\"changeGuestPhone\", [value, contract])\n          .then((result) => {\n            switch (result.status) {\n              case \"ok\":\n                contract.phone = value;\n                contract.editing = false;\n                this.$forceUpdate();\n                break;\n            }\n          });\n      }\n    },\n    cancelEditGuestData(contract) {\n      contract.editing = false;\n      this.$forceUpdate();\n    },\n\n    /**\n     * Группировка пасскодов по апартаментам для поблочного отображения\n     * bookingInfo.passcodes -  массив пасскодов, пришедший с бекенда\n     */\n    contractPasscodes(contract) {\n      let passcodes = contract.passcodes || [];\n      let grouped_passcodes = [];\n      let result = [];\n\n      for (var i = 0; i < passcodes.length; i++) {\n        const el = passcodes[i];\n        var room_id = `${el.room_id}_${el.period_index}`;\n\n        // Если массив с пасскодами лдя апартаментов уже есть в объекте группировки, то добавляем пасскод к апартаментам\n        // Как один из кодов апартаментов\n        if (grouped_passcodes[room_id]) grouped_passcodes[room_id].push(el);\n        else grouped_passcodes[room_id] = [el];\n      }\n\n      if (contract.initial_apartments) {\n        let keys = Object.keys(contract.initial_apartments);\n\n        for (let i = 0; i < keys.length; i++) {\n          const el = contract.initial_apartments[keys[i]];\n          const passcodes = grouped_passcodes[`${el.room_id}_${i}`];\n\n          if (passcodes)\n            for (let j = 0; j < passcodes.length; j++) {\n              const pc = passcodes[j];\n              result.push({\n                id: pc.id,\n                name:\n                  el.room_name +\n                  \" / \" +\n                  el.room_type_name +\n                  \" (\" +\n                  pc.lock_name +\n                  \")\",\n                status_id: pc.status_id,\n                passcode: pc.passcode,\n                is_algorithmic: pc.is_algorithmic,\n                start_datetime: pc.start_datetime,\n                end_datetime: pc.end_datetime,\n              });\n            }\n        }\n      }\n\n      return result;\n    },\n    formatDatetime(datetime, timezone) {\n      return new Date(datetime).toLocaleDateString([], {\n        hour: \"2-digit\",\n        minute: \"2-digit\",\n        timeZone: timezone,\n      });\n    },\n    formatDate(date) {\n      return new Date(date).toLocaleDateString([], {});\n    },\n    formatPasscodeStatus(code, status) {\n      let content = \"Ошибка\";\n\n      switch (status) {\n        case \"set\":\n          content = code;\n          break;\n        case \"canceling\":\n          content = this.$tt(\"passcode_status.\" + status); // В процессе отмены\n          break;\n        case \"canceled\":\n          content = this.$tt(\"passcode_status.\" + status); // Отменен\n          break;\n        case \"failed\":\n          content = this.$tt(\"passcode_status.\" + status); // \"Ошибка установки\";\n          break;\n        case \"expired\":\n          content = code + ` (${this.$tt(\"passcode_status.\" + status)})`; // \"Истек\";\n          break;\n        default:\n          content = this.$tt(\"passcode_status.default\");\n          break;\n      }\n\n      return content;\n    },\n    toggleAccordion(customerId) {\n        Vue.set(this.accordion, customerId, !this.accordion[customerId]);\n    },\n    async shiftPasscodeHour(passcodeId, shiftType) {\n      const $this = this;\n      let showError = function (pcId, error) {\n        $this.$refs[\"passcode_error_block_\" + pcId][0].innerHTML = $this.$tt(\n          \"passcode_error.\" + error\n        );\n      };\n      \n      let cleanError = function(pcId) {\n        $this.$refs[\"passcode_error_block_\" + pcId][0].innerHTML = '';\n      };\n\n      let data = {\n        id: passcodeId,\n        shiftType: shiftType,\n      };\n      this.$eswStore.dispatch(\"togglePreloader\", true);\n      this.$eswStore.dispatch(\"shiftPasscodeHour\", data).then((result) => {\n        if (result.status == \"error\") {\n          switch (result.code) {\n            case \"lock_wo_gateway\":\n            case \"gateway_busy\":\n            case \"gateway_is_offline\":\n            case \"ttlock_timeout\":\n              showError(passcodeId, result.code);\n              break;\n\n            default:\n              showError(passcodeId, \"unexpected\");\n              break;\n          }\n        } else {\n          cleanError(passcodeId);\n        }\n        this.$eswStore.dispatch(\"togglePreloader\", false);\n      });\n    },\n  },\n};\n</script>\n\n<style scope>\n.icon-action {\n  cursor: pointer;\n}\n\nsmall.alert.m-red:empty {\n  display: none;\n}\n\n.s_passcode-error:not(:empty) {\n  color: red;\n  display: block;\n}\n</style>"]}, media: undefined });

      };
      /* scoped */
      var __vue_scope_id__$4 = undefined;
      /* module identifier */
      var __vue_module_identifier__$4 = undefined;
      /* functional template */
      var __vue_is_functional_template__$4 = false;
      /* style inject SSR */
      
      /* style inject shadow dom */
      

      
      var __vue_component__$4 = /*#__PURE__*/normalizeComponent(
        { render: __vue_render__$4, staticRenderFns: __vue_staticRenderFns__$4 },
        __vue_inject_styles__$4,
        __vue_script__$4,
        __vue_scope_id__$4,
        __vue_is_functional_template__$4,
        __vue_module_identifier__$4,
        false,
        createInjector,
        undefined,
        undefined
      );

    //
    //
    //
    //

    var script$3 = {
        name: 'ContractExpiredScreen',
        data: function data() {
            return {};
        }
    };

    /* script */
    var __vue_script__$3 = script$3;

    /* template */
    var __vue_render__$3 = function () {
      var _vm = this;
      var _h = _vm.$createElement;
      var _c = _vm._self._c || _h;
      return _c("div")
    };
    var __vue_staticRenderFns__$3 = [];
    __vue_render__$3._withStripped = true;

      /* style */
      var __vue_inject_styles__$3 = function (inject) {
        if (!inject) { return }
        inject("data-v-035438d8_0", { source: "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", map: {"version":3,"sources":[],"names":[],"mappings":"","file":"ContractExpiredScreen.vue"}, media: undefined });

      };
      /* scoped */
      var __vue_scope_id__$3 = undefined;
      /* module identifier */
      var __vue_module_identifier__$3 = undefined;
      /* functional template */
      var __vue_is_functional_template__$3 = false;
      /* style inject SSR */
      
      /* style inject shadow dom */
      

      
      var __vue_component__$3 = /*#__PURE__*/normalizeComponent(
        { render: __vue_render__$3, staticRenderFns: __vue_staticRenderFns__$3 },
        __vue_inject_styles__$3,
        __vue_script__$3,
        __vue_scope_id__$3,
        __vue_is_functional_template__$3,
        __vue_module_identifier__$3,
        false,
        createInjector,
        undefined,
        undefined
      );

    //
    //
    //
    //
    //
    //
    //
    //

    var script$2 = {
        name: 'ActionConfirmationScreen',
        props: ['request'],
        data: function data() {
            return {};
        },
        methods: {
            decline: function decline() {
                this.request.isFulfilled = true;
                this.request.action(false);
            },
            accept: function accept() {
                this.request.isFulfilled = true;
                this.request.action(true);
            }
        },
    };

    /* script */
    var __vue_script__$2 = script$2;

    /* template */
    var __vue_render__$2 = function () {
      var _vm = this;
      var _h = _vm.$createElement;
      var _c = _vm._self._c || _h;
      return _c("div", [
        _c("p", { domProps: { innerHTML: _vm._s(_vm.request.message) } }),
        _vm._v(" "),
        _c("button", { staticClass: "_esw-button", on: { click: _vm.decline } }, [
          _vm._v("Отменить") ]),
        _vm._v(" "),
        _c(
          "button",
          { staticClass: "_esw-button primary", on: { click: _vm.accept } },
          [_vm._v("Продолжить")]
        ) ])
    };
    var __vue_staticRenderFns__$2 = [];
    __vue_render__$2._withStripped = true;

      /* style */
      var __vue_inject_styles__$2 = function (inject) {
        if (!inject) { return }
        inject("data-v-19db620f_0", { source: "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", map: {"version":3,"sources":[],"names":[],"mappings":"","file":"ActionConfirmationScreen.vue"}, media: undefined });

      };
      /* scoped */
      var __vue_scope_id__$2 = undefined;
      /* module identifier */
      var __vue_module_identifier__$2 = undefined;
      /* functional template */
      var __vue_is_functional_template__$2 = false;
      /* style inject SSR */
      
      /* style inject shadow dom */
      

      
      var __vue_component__$2 = /*#__PURE__*/normalizeComponent(
        { render: __vue_render__$2, staticRenderFns: __vue_staticRenderFns__$2 },
        __vue_inject_styles__$2,
        __vue_script__$2,
        __vue_scope_id__$2,
        __vue_is_functional_template__$2,
        __vue_module_identifier__$2,
        false,
        createInjector,
        undefined,
        undefined
      );

    //
    //

    var script$1 = {
      mounted: async function mounted() {
        var esApiOpts = {
          token: window._eswc.token,
          domain: window._eswc.domain,
          partnerApi: window._eswc.partnerApi
        };

        var startupOptions = {
            contractId: window._eswc.contractId,
            contractPhone: window._eswc.contractPhone,
            partner: 'bnovo',
            esApi: esApiOpts
        };

        var socketOpts = {
          token: window._eswc.token,
          libPath: '//' + window._eswc.domain + window._eswc.socketLibPath,
          wssUrl: 'wss://' + window._eswc.domain + window._eswc.socketPath,
        };
        
        // Загрузка основных данных
        // Получение данных по аккаунту пользователя виджета, биллинга, запрос на получение контракта в системе
        // Получение данных по пользователю партнера, бронированию партнера
        await this.$eswStore.dispatch('startup', startupOptions);
        
        // Обновление состояния виджета посредством "пинга" бекенда
        // Может использоваться только если сокет недоступен
        // this.$eswStore.dispatch('updateState');

        // Инициализация подключения к вебсокету
        await this.$eswStore.dispatch('initSocket', socketOpts);

        // Установка флага что виджет готов(прошли все операции загрузки)
        // и отключение оверфлоу прелоадера
        this.$eswStore.dispatch('setWidgetReady');
        this.$eswStore.dispatch('togglePreloader', false);

        // Загрузка информации по замкам для апартаментов бронирования
        var isLocksConfigured = this.$eswStore.getters.isLocksServiceConfigured;

        if(isLocksConfigured)
          { this.$eswStore.dispatch('loadApartmentsLocks'); }
      }
    };

    /* script */
    var __vue_script__$1 = script$1;

    /* template */
    var __vue_render__$1 = function () {
      var _vm = this;
      var _h = _vm.$createElement;
      var _c = _vm._self._c || _h;
      return _c("div")
    };
    var __vue_staticRenderFns__$1 = [];
    __vue_render__$1._withStripped = true;

      /* style */
      var __vue_inject_styles__$1 = function (inject) {
        if (!inject) { return }
        inject("data-v-c16edad6_0", { source: "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", map: {"version":3,"sources":[],"names":[],"mappings":"","file":"Startup.vue"}, media: undefined });

      };
      /* scoped */
      var __vue_scope_id__$1 = undefined;
      /* module identifier */
      var __vue_module_identifier__$1 = undefined;
      /* functional template */
      var __vue_is_functional_template__$1 = false;
      /* style inject SSR */
      
      /* style inject shadow dom */
      

      
      var __vue_component__$1 = /*#__PURE__*/normalizeComponent(
        { render: __vue_render__$1, staticRenderFns: __vue_staticRenderFns__$1 },
        __vue_inject_styles__$1,
        __vue_script__$1,
        __vue_scope_id__$1,
        __vue_is_functional_template__$1,
        __vue_module_identifier__$1,
        false,
        createInjector,
        undefined,
        undefined
      );

    //

    var script = {
      data: function() {
          return {
            isRegistrationStep: false,
            isRegistrationWithLocks: false,
          };
      },
      name: "ESWidget",
      components: {
        ActionConfirmationScreen: __vue_component__$2,
        WelcomeScreen: __vue_component__$8,
        ContractInfoScreen: __vue_component__$4,
        ContractExpiredScreen: __vue_component__$3,
        FatalWidgetError: __vue_component__$7,
        BillingWidgetError: __vue_component__$6,
        WidgetWarning: __vue_component__$5,
        Startup: __vue_component__$1
      },
      computed: {
        widgetTitle: function widgetTitle() {
          var contractNumber = this.$eswStore.getters.getContractNumber;
          var state = this.$eswStore.getters.getContractState;
          var title = 'Удалённое заселение';
          var contractTitle = 'Удалённое заселение для бронирования ' + contractNumber;
          var contractWithLocksTitle = 'Удалённое подписание документов бронирования ' + contractNumber;

          // Если контракт еще не создан, то смотреть на внутренние пропсы изменяемые событием start-registration
          // из компонента WelcomeScreen
          if(this.isRegistrationStep) {
            if(this.isRegistrationWithLocks) {
              title = contractWithLocksTitle;
            } else {
              title = contractTitle;
            }
          // Если данные по контракту были получены и он создан то смотреть на флаг "использует сервис замков"
          } else if(state && state.is_lock_management !== undefined) {
            if(!state.is_lock_management) {
              title = contractWithLocksTitle;
            } else if(state.is_lock_management) {
              title = contractTitle;
            }
          }
          return title;
        },
        isReady: function isReady() { return this.$eswStore.getters.isReady },
        isLoadingData: function isLoadingData() {
          var isLoadingData = this.$eswStore.getters.isLoadingData;
          this.$emit('showpreloader', {value: isLoadingData});
          return isLoadingData;
        },
        isContractCreating: function isContractCreating() { return this.$eswStore.getters.isContractCreating },
        hasFatalError: function hasFatalError() {
          var hasFatalError = this.$eswStore.getters.hasFatalError;
          if(hasFatalError) { this.$emit('fatalerror', hasFatalError); }
          return hasFatalError;
        },
        fatalError: function fatalError() { return this.$eswStore.getters.getFatalError },
        hasWarning: function hasWarning() {  return this.$eswStore.getters.hasWidgetWarning },
        widgetWarning: function widgetWarning() { return this.$eswStore.getters.getWidgetWarning },
        isConfirmationRequired: function isConfirmationRequired() { return this.$eswStore.getters.isConfirmationRequired },
        confirmationRequest: function confirmationRequest() { return this.$eswStore.getters.getConfirmationRequest },
        contractState: function contractState() {
          var contract = this.$eswStore.getters.getContractState;
          var outerState = {
            status: contract.code
          };
          // Выброс события с обновленным состоянием бронирования
          this.$emit('updatestate', outerState);
          return contract;
        },
        billingState: function billingState() {
          var billingState = this.$eswStore.getters.getWidgetBillingState;
          this.$emit('updatebilling', billingState);
          return billingState;
        },
        isNewContract: function isNewContract() {
          var contract = this.$eswStore.getters.getContractState;
          return contract.code == 0;
        },
        isCanceledContract: function isCanceledContract() {
          var contract = this.$eswStore.getters.getContractState;
          return contract.code == 'canceled';
        },
        isExpiredContract: function isExpiredContract() {
          var contract = this.$eswStore.getters.getContractState;
          return contract.code == 'expired';
        },
        isTariffAllows: function isTariffAllows() {
          var billing = this.$eswStore.getters.getWidgetBillingState;
          if(!billing) {
            return false;
          }
          var isServiceAllowed = billing.available_services.includes(PartnerStoreHelper.SERVICES.REMOTE_CONTRACT_SIGNING);
          var isEnoughtBalance = false;
          if(billing.tarif_type_id == 'trial') {
            isEnoughtBalance = Math.round((new Date(billing.trial_ends_at) - new Date()) / (1000*60*60*24)) >= 0;
          } else {
            isEnoughtBalance = billing.balance >= 1;
          }
          return isServiceAllowed && isEnoughtBalance;
        }
      },
      beforeMount: function beforeMount() {
        this.$eswStore.dispatch('setWidgetTitle', 'Удалённое заселение');
      },
      errorCaptured: function(err) {
        this.$eswStore.dispatch('handleFatalError', err);
        return false;
      },
      methods: {
        startRegistration: function startRegistration(opts) {
          this.isRegistrationStep = true;
          this.isRegistrationWithLocks = opts && opts.locks;
        }
      }
    };

    /* script */
    var __vue_script__ = script;

    /* template */
    var __vue_render__ = function () {
      var _vm = this;
      var _h = _vm.$createElement;
      var _c = _vm._self._c || _h;
      return _c(
        "div",
        { staticClass: "_esw-body", attrs: { id: "_esw" } },
        [
          _c("Startup"),
          _vm._v(" "),
          _vm.isLoadingData && _vm.billingState ? void 0 : _vm._e(),
          _vm._v(" "),
          _c("div", { staticClass: "_esw-header" }, [
            _c("h3", [_vm._v(_vm._s(_vm.widgetTitle))]) ]),
          _vm._v(" "),
          _vm.hasWarning
            ? _c("WidgetWarning", { attrs: { warning: _vm.widgetWarning } })
            : _vm._e(),
          _vm._v(" "),
          _vm.hasFatalError
            ? _c("FatalWidgetError", { attrs: { error: _vm.fatalError } })
            : _vm._e(),
          _vm._v(" "),
          !_vm.hasFatalError
            ? _c(
                "div",
                { staticClass: "widget-content" },
                [
                  _vm.isReady && _vm.contractState
                    ? [
                        _vm.isConfirmationRequired
                          ? _c(
                              "div",
                              [
                                _c("ActionConfirmationScreen", {
                                  attrs: { request: _vm.confirmationRequest },
                                }) ],
                              1
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        _c(
                          "div",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value: _vm.isContractCreating,
                                expression: "isContractCreating",
                              } ],
                          },
                          [_c("p", [_vm._v("Создается удалённое заселение...")])]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          {
                            directives: [
                              {
                                name: "show",
                                rawName: "v-show",
                                value:
                                  !_vm.isConfirmationRequired &&
                                  !_vm.isContractCreating,
                                expression:
                                  "!isConfirmationRequired && !isContractCreating",
                              } ],
                          },
                          [
                            _vm.isNewContract && _vm.isTariffAllows
                              ? _c("WelcomeScreen", {
                                  on: {
                                    "start-registration": _vm.startRegistration,
                                  },
                                })
                              : _vm.isNewContract && !_vm.isTariffAllows
                              ? _c("BillingWidgetError")
                              : _vm.isExpiredContract
                              ? _c("ContractExpiredScreen")
                              : _vm.isCanceledContract
                              ? _c("ContractCancelledScreen")
                              : _c("ContractInfoScreen") ],
                          1
                        ) ]
                    : !_vm.isReady
                    ? [_c("p", [_vm._v("Загрузка виджета...")])]
                    : _vm._e() ],
                2
              )
            : _vm._e() ],
        2
      )
    };
    var __vue_staticRenderFns__ = [];
    __vue_render__._withStripped = true;

      /* style */
      var __vue_inject_styles__ = function (inject) {
        if (!inject) { return }
        inject("data-v-e8cdacb2_0", { source: "\n._esw-body {\n  min-width: 550px;\n  position: relative;\n}\n._esw-header {\n  position: relative;\n  display: block;\n  padding: 0 0 10px 0;\n  font-family: \"Cera Pro Medium\", Arial, sans-serif;\n}\n.widget-content p {\n  font-size: 1rem;\n}\n.widget-content p:not(:last-child) {\n  margin-bottom: 0.6rem;\n}\n._esw-button {\n  cursor: pointer;\n  border: none;\n  border-radius: 5px;\n  font-size: 14px;\n  padding: 6px 20px;\n  color: rgba(17, 34, 62, 1);\n  background-color: rgba(237, 240, 244, 1);\n  transition: opacity .3s linear;\n  margin-right: 16px;\n}\n._esw-button:last-child {\n  margin-right: 0;\n}\n._esw-button.primary {\n  background-color: rgba(49, 106, 239, 1);\n  color: white;\n}\n._esw-section {\n  display: block;\n  position: relative;\n  width: 100%;\n  padding: 0 0 8px 0;\n}\n._esw-button:hover:not(disabled) {\n  opacity: 50%;\n}\n._esw-status_orange {\n  color: orange;\n}\n._esw-grid {\n  display: flex;\n}\n._esw-contract-card {\n  position: relative;\n  padding-bottom: 24px;\n}\n._esw-accordion-icon {\n  position: absolute;\n  opacity: 70%;\n  top: 2px;\n  right: 2px;\n}\n._esw-accordion ._esw-accordion-content {\n  display: none;\n}\n._esw-accordion._esw-accordion-show ._esw-accordion-content {\n  display: block;\n}\n", map: {"version":3,"sources":["/Users/mike/Dev/repo/esigner-widget/src/ESWidget.vue"],"names":[],"mappings":";AAmKA;EACA,gBAAA;EACA,kBAAA;AACA;AACA;EACA,kBAAA;EACA,cAAA;EACA,mBAAA;EACA,iDAAA;AACA;AACA;EACA,eAAA;AACA;AACA;EACA,qBAAA;AACA;AACA;EACA,eAAA;EACA,YAAA;EACA,kBAAA;EACA,eAAA;EACA,iBAAA;EACA,0BAAA;EACA,wCAAA;EACA,8BAAA;EACA,kBAAA;AACA;AACA;EACA,eAAA;AACA;AACA;EACA,uCAAA;EACA,YAAA;AACA;AACA;EACA,cAAA;EACA,kBAAA;EACA,WAAA;EACA,kBAAA;AACA;AACA;EACA,YAAA;AACA;AACA;EACA,aAAA;AACA;AACA;EACA,aAAA;AACA;AACA;EACA,kBAAA;EACA,oBAAA;AACA;AACA;EACA,kBAAA;EACA,YAAA;EACA,QAAA;EACA,UAAA;AACA;AACA;EACA,aAAA;AACA;AACA;EACA,cAAA;AACA","file":"ESWidget.vue","sourcesContent":["<template>\n  <div class=\"_esw-body\" id=\"_esw\">\n    <Startup></Startup>\n    <template v-if=\"isLoadingData && billingState\"></template>\n    <div class=\"_esw-header\">\n        <h3>{{ widgetTitle }}</h3>\n    </div>\n    <WidgetWarning v-if=\"hasWarning\" v-bind:warning=\"widgetWarning\"></WidgetWarning>\n    <FatalWidgetError v-if=\"hasFatalError\" v-bind:error=\"fatalError\"></FatalWidgetError>\n    <div v-if=\"!hasFatalError\" class=\"widget-content\">\n      <template v-if=\"isReady && contractState\">\n        <div v-if=\"isConfirmationRequired\">\n          <ActionConfirmationScreen v-bind:request=\"confirmationRequest\"></ActionConfirmationScreen>\n        </div>\n        <div v-show=\"isContractCreating\">\n          <p>Создается удалённое заселение...</p>\n        </div>\n        <div v-show=\"!isConfirmationRequired && !isContractCreating\">\n          <WelcomeScreen\n            v-if=\"isNewContract && isTariffAllows\"\n            v-on:start-registration=\"startRegistration\"\n          ></WelcomeScreen>\n          <BillingWidgetError v-else-if=\"isNewContract && !isTariffAllows\"></BillingWidgetError>\n          <ContractExpiredScreen v-else-if=\"isExpiredContract\"></ContractExpiredScreen>\n          <ContractCancelledScreen v-else-if=\"isCanceledContract\"></ContractCancelledScreen>\n          <ContractInfoScreen v-else></ContractInfoScreen>\n        </div>\n      </template>\n      <template v-else-if=\"!isReady\"><p>Загрузка виджета...</p></template>\n    </div>\n  </div>\n</template>\n\n<script>\nimport WelcomeScreen from './components/WelcomeScreen.vue';\nimport FatalWidgetError from './components/FatalWidgetError.vue';\nimport BillingWidgetError from './components/BillingWidgetError.vue';\nimport WidgetWarning from './components/WidgetWarning.vue';\nimport ContractInfoScreen from './components/ContractInfoScreen.vue';\nimport ContractExpiredScreen from './components/ContractExpiredScreen.vue';\nimport ActionConfirmationScreen from './components/ActionConfirmationScreen.vue';\nimport ESignerHelper from './store/partners/bnovo_helper';\nimport Startup from './Startup.vue';\n\nexport default {\n  data: function() {\n      return {\n        isRegistrationStep: false,\n        isRegistrationWithLocks: false,\n      };\n  },\n  name: \"ESWidget\",\n  components: {\n    ActionConfirmationScreen: ActionConfirmationScreen,\n    WelcomeScreen: WelcomeScreen,\n    ContractInfoScreen: ContractInfoScreen,\n    ContractExpiredScreen: ContractExpiredScreen,\n    FatalWidgetError: FatalWidgetError,\n    BillingWidgetError: BillingWidgetError,\n    WidgetWarning: WidgetWarning,\n    Startup: Startup\n  },\n  computed: {\n    widgetTitle() {\n      let contractNumber = this.$eswStore.getters.getContractNumber;\n      let state = this.$eswStore.getters.getContractState;\n      let title = 'Удалённое заселение';\n      let contractTitle = 'Удалённое заселение для бронирования ' + contractNumber;\n      let contractWithLocksTitle = 'Удалённое подписание документов бронирования ' + contractNumber;\n\n      // Если контракт еще не создан, то смотреть на внутренние пропсы изменяемые событием start-registration\n      // из компонента WelcomeScreen\n      if(this.isRegistrationStep) {\n        if(this.isRegistrationWithLocks) {\n          title = contractWithLocksTitle;\n        } else {\n          title = contractTitle;\n        }\n      // Если данные по контракту были получены и он создан то смотреть на флаг \"использует сервис замков\"\n      } else if(state && state.is_lock_management !== undefined) {\n        if(!state.is_lock_management) {\n          title = contractWithLocksTitle;\n        } else if(state.is_lock_management) {\n          title = contractTitle;\n        }\n      }\n      return title;\n    },\n    isReady() { return this.$eswStore.getters.isReady },\n    isLoadingData() {\n      let isLoadingData = this.$eswStore.getters.isLoadingData;\n      this.$emit('showpreloader', {value: isLoadingData});\n      return isLoadingData;\n    },\n    isContractCreating() { return this.$eswStore.getters.isContractCreating },\n    hasFatalError() {\n      let hasFatalError = this.$eswStore.getters.hasFatalError;\n      if(hasFatalError) this.$emit('fatalerror', hasFatalError);\n      return hasFatalError;\n    },\n    fatalError() { return this.$eswStore.getters.getFatalError },\n    hasWarning() {  return this.$eswStore.getters.hasWidgetWarning },\n    widgetWarning() { return this.$eswStore.getters.getWidgetWarning },\n    isConfirmationRequired() { return this.$eswStore.getters.isConfirmationRequired },\n    confirmationRequest() { return this.$eswStore.getters.getConfirmationRequest },\n    contractState() {\n      let contract = this.$eswStore.getters.getContractState;\n      let outerState = {\n        status: contract.code\n      };\n      // Выброс события с обновленным состоянием бронирования\n      this.$emit('updatestate', outerState);\n      return contract;\n    },\n    billingState() {\n      let billingState = this.$eswStore.getters.getWidgetBillingState;\n      this.$emit('updatebilling', billingState);\n      return billingState;\n    },\n    isNewContract() {\n      let contract = this.$eswStore.getters.getContractState;\n      return contract.code == 0;\n    },\n    isCanceledContract() {\n      let contract = this.$eswStore.getters.getContractState;\n      return contract.code == 'canceled';\n    },\n    isExpiredContract() {\n      let contract = this.$eswStore.getters.getContractState;\n      return contract.code == 'expired';\n    },\n    isTariffAllows() {\n      let billing = this.$eswStore.getters.getWidgetBillingState;\n      if(!billing) {\n        return false;\n      }\n      let isServiceAllowed = billing.available_services.includes(ESignerHelper.SERVICES.REMOTE_CONTRACT_SIGNING);\n      let isEnoughtBalance = false;\n      if(billing.tarif_type_id == 'trial') {\n        isEnoughtBalance = Math.round((new Date(billing.trial_ends_at) - new Date()) / (1000*60*60*24)) >= 0;\n      } else {\n        isEnoughtBalance = billing.balance >= 1;\n      }\n      return isServiceAllowed && isEnoughtBalance;\n    }\n  },\n  beforeMount() {\n    this.$eswStore.dispatch('setWidgetTitle', 'Удалённое заселение');\n  },\n  errorCaptured: function(err) {\n    this.$eswStore.dispatch('handleFatalError', err);\n    return false;\n  },\n  methods: {\n    startRegistration(opts) {\n      this.isRegistrationStep = true;\n      this.isRegistrationWithLocks = opts && opts.locks;\n    }\n  }\n};\n</script>\n\n<style scope>\n  ._esw-body {\n    min-width: 550px;\n    position: relative;\n  }\n  ._esw-header {\n    position: relative;\n    display: block;\n    padding: 0 0 10px 0;\n    font-family: \"Cera Pro Medium\", Arial, sans-serif;\n  }\n  .widget-content p {\n    font-size: 1rem;\n  }\n  .widget-content p:not(:last-child) {\n    margin-bottom: 0.6rem;\n  }\n  ._esw-button {\n    cursor: pointer;\n    border: none;\n    border-radius: 5px;\n    font-size: 14px;\n    padding: 6px 20px;\n    color: rgba(17, 34, 62, 1);\n    background-color: rgba(237, 240, 244, 1);\n    transition: opacity .3s linear;\n    margin-right: 16px;\n  }\n  ._esw-button:last-child {\n    margin-right: 0;\n  }\n  ._esw-button.primary {\n    background-color: rgba(49, 106, 239, 1);\n    color: white;\n  }\n  ._esw-section {\n    display: block;\n    position: relative;\n    width: 100%;\n    padding: 0 0 8px 0;\n  }\n  ._esw-button:hover:not(disabled) {\n    opacity: 50%;\n  }\n  ._esw-status_orange {\n    color: orange;\n  }\n  ._esw-grid {\n    display: flex;\n  }\n  ._esw-contract-card {\n    position: relative;\n    padding-bottom: 24px;\n  }\n  ._esw-accordion-icon {\n    position: absolute;\n    opacity: 70%;\n    top: 2px;\n    right: 2px;\n  }\n  ._esw-accordion ._esw-accordion-content {\n    display: none;\n  }\n  ._esw-accordion._esw-accordion-show ._esw-accordion-content {\n    display: block;\n  }\n</style>"]}, media: undefined });

      };
      /* scoped */
      var __vue_scope_id__ = undefined;
      /* module identifier */
      var __vue_module_identifier__ = undefined;
      /* functional template */
      var __vue_is_functional_template__ = false;
      /* style inject SSR */
      
      /* style inject shadow dom */
      

      
      var __vue_component__ = /*#__PURE__*/normalizeComponent(
        { render: __vue_render__, staticRenderFns: __vue_staticRenderFns__ },
        __vue_inject_styles__,
        __vue_script__,
        __vue_scope_id__,
        __vue_is_functional_template__,
        __vue_module_identifier__,
        false,
        createInjector,
        undefined,
        undefined
      );

    var ServiceLocksStatus = {
        OK: 'ok',
        LOADING: 'loading',
        // Период для замков неопределен
        PERIOD_NOT_SET: 'period_not_set',
        // Требуется синхронизация апартаментов
        REQUIRED_SYNCH: 'required_synchronization',
        // У апартаментов нет замков
        EMPTY_LOCKS: 'locks_not_exists',
        // Таймаут подключения к TTLock
        TIMEOUT: 'ttlock_timeout',
    };

    function ESignerSocket(authToken, wssUrl, store) {
        var socket = null;
        var connection = {
            info: null,
        };
        var channels = {
            account: null,
            widget: null,
        };

        function onAccountChannelMessage(channel) {
            // channel.on("notification", payload => {
            //     if (signerNotifications) {
            //         signerNotifications.pushNotification(null, payload);
            //     }
            // });

            channel.on("ttlock_update", function (payload) {
                switch (payload.type) {
                    case "passcode_update":
                        store.dispatch('updatePasscode', payload);
                        break;
                }
            });

            channel.on("account_billing_update", function (payload) {
                store.dispatch("updateWidgetBillingState", payload);
            });

            channel.on("application_created", function (payload) {
                store.dispatch("contractCreated", payload);
            });
            
            channel.on("application_updated", function (payload) {
                store.dispatch("updateContract", payload);
            });
        }

        function onLoad() {
            socket = new Phoenix.Socket(wssUrl, { params: { 'token': authToken } });

            socket.onOpen(function () {
                var _channels = channels;
                if (!channels.widget) {
                    var channel = socket.channel(PartnerStoreHelper.CHANNELS.WIDGET_CHANNEL, {});

                    channel.join()
                        .receive("ok", function (_) {
                            if (!_channels.widget) {
                                console.info("[ESigner] Joined widget channel");
                                getWidgetSocketData(channel);
                                _channels.widget = channel;
                            } else {
                                console.info("[ESigner] Re-joined widget channel");
                            }
                        });
                }
            });

            socket.connect();
        }

        function getWidgetSocketData(channel) {
            channel.push("info", {})
                .receive("ok", function (resp) {
                    connection.info = resp;
                    joinOtherChannels();
                });
        }

        function joinOtherChannels() {
            var accountId = connection.info.legal_entity_id;

            if(!channels.account) {
                var channel = socket.channel(PartnerStoreHelper.CHANNELS.ACCOUNT + ':' + accountId, {});
                var _channels = channels;

                // Подключение к каналу текущего аккаунта
                channel.join()
                    .receive("ok", function (_) {
                        if (!_channels.account) {
                            console.info(("[ESigner] Joined account#" + accountId + " channel"));
                            _channels.account = channel;
                            onAccountChannelMessage(channel);
                        } else {
                            console.info(("[ESigner] Re-joined account#" + accountId + " channel"));
                        }
                    });
            }
        }

        return {
            isConnected: false,

            socket: socket,
            channels: channels,
            connection: connection,

            start: function start(libPath) {
                var s = document.createElement('script');
                s.src = libPath;
                s.onload = function() {
                    onLoad();
                    this.remove();
                };
                (document.head || document.documentElement).appendChild(s);
            }
        };
    }

    function BnovoApi() {

        function getInputValue(inputSelector) {
            var el = document.querySelector(inputSelector);
            return el.value;
        }

        return {
            /**
             * Возвращает данные по партнеру(аккаунту бново) для запуска виджета
             * 
             * @return {Promise<Response>}
             */
            partnerUser: function partnerUser() {
                var req =
                    fetch('/account/current')
                    .then(function(response) {
                        return response.json();
                    })
                    .then(function(json) {
                        var partnerData = {
                            // TODO: Переименовать в user_email
                            bnovo_user_email: json.current_user.email,
                            notify_list: [
                                json.current_user.email,
                                json.hotel.owner_email
                            ],
                            hotel_id: json.hotel.id,
                            timezone: json.hotel.timezone,
                            currency: json.hotel.currency,
                            language: json.hotel.language
                        };

                        return partnerData;
                    });

                return req;
            },

            /**
             * TODO: агрегация данных для работы через API биново
             */
            partnerData: async function partnerData() {
                var adults_number = parseInt(getInputValue('[name="adults"]'));
                var childrens_number = parseInt(getInputValue('[name="children"]'));

                var data = {
                    contract_number: '',
                    bnovo_booking_id: getInputValue('[name="booking_id"]'),
                    bnovo_customer_id: getInputValue('[name="customer_id"]'),
                    bnovo_hotel_id: current_hotel_id, // global
                    bnovo_user_id: current_user_id, // global
                    total_price: getInputValue('#booking_initial_total_amount'),
                    // daily_price: getInputValue(''),
                    // status: getInputValue('.switcher .switcher__item.m-current').attr('data-status-id') || 1,
                    checkin_datetime: getInputValue('[name="date_from"]') + ' ' + getInputValue('[name="hour_from"]'),
                    checkout_datetime: getInputValue('[name="date_to"]') + ' ' + getInputValue('[name="hour_to"]'),
                    first_name: getInputValue('[name="name"]'),
                    last_name: getInputValue('[name="surname"]'),
                    middle_name: getInputValue('[name="middlename"]'),
                    phone: getInputValue('[name="phone"]'),
                    email: getInputValue('[name="email"]'),
                    number_of_adults: adults_number,
                    number_of_guests: adults_number + childrens_number,
                    services: services_existing_prices || [] // global
                };

                var res = await this.bookingData(data.bnovo_booking_id).then(function (r) { return r.json(); });
                data.contract_number = res.booking.number;
                data.contract_id = res.booking.number;
                data.arrival_datetime = res.booking.arrival;

                var templates = [];
                Object.entries(res.templates).forEach(function (ref) {
                    ref[0];
                    var item = ref[1];

                    templates.push({name: item.name, id: item.name});
                });
                data.templates = templates;
                data.checkinTemplates = templates;
                
                data.customer_id = res.booking.customer_id;
                data.customers = res.booking.customers;
                
                data.apartments = res.merged_rooms;

                return data;
            },

            bookingData: function bookingData(bookingId) {
                return fetch('/booking/general/' + bookingId, {headers: {Accept: 'application/json'}});
            },
        }
    }

    function ESWidgetApi(token, url, partnerApi) {
        
        var _authToken = token;
        var _domain = url;
        var _partnerApi = partnerApi;

        function get(query, headers) {
            var requestOptions = {
                method: 'GET',
                headers: (headers || {})
            };

            return req(query, requestOptions);
        }

        function post(query, headers, body) {
            var requestOptions = {
                method: 'POST',
                headers: (headers || {})
            };

            return req(query, requestOptions, body);
        }
        
        function put(query, headers, body) {
            var requestOptions = {
                method: 'PUT',
                headers: (headers || {})
            };

            return req(query, requestOptions, body);
        }

        function req(query, requestOptions, body) {
            requestOptions.headers = Object.assign({
                    'Authorization': 'Bearer ' + _authToken,
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                }, requestOptions.headers);

            if(typeof body !== undefined) {
                requestOptions.body = JSON.stringify(body);
            }

            return fetch('//' + _domain + _partnerApi + query, requestOptions).then(function (res) {
                switch(res.status) {
                    case 401:
                        throw new Error("Пользователь виджета не может быть авторизован");
                    case 404:
                        throw new Error("Запрашиваемый эндпоинт несуществует");
                    case 500:
                        console.error("[ESigner] Ошибка сервиса", res.body);
                        throw new Error("Ошибка сервиса ESigner");
                }

                return new Promise(function (resolve) { return resolve(res); });
            });
        }

        return {
            getWidgetState: function getWidgetState(hotelId) {
                var urlPath = "/widget";
                var headers = {
                    cache: "no-store"
                };

                return get((urlPath + "?hotel_id=" + hotelId), headers);
            },
            getContractState: function getContractState(contractNumber) {
                var urlPath = "/bookings";
                var headers = {
                    cache: "no-store"
                };

                return get((urlPath + "/" + contractNumber), headers);
            },
            getApartmentsLocksData: function getApartmentsLocksData(apartments) {
                var apartments_ids = [];
                for (var i = 0; i < apartments.length; i++) {
                    var el = apartments[i];
                    apartments_ids.push('apartments[]=' + el.room_id);
                }

                var urlPath = '/lock_managment/apartments_locks?' + apartments_ids.join('&');
                var headers = {
                    cache: "no-store"
                };

                return get(("" + urlPath), headers);
            },

            registerContract: function registerContract(payload) {
                var urlPath = '/bookings';
                var headers = {};
                var body = payload;
                
                return post(("" + urlPath), headers, body);
            },

            registerContractWithLocks: function registerContractWithLocks(payload) {
                var urlPath = '/bookings-locks';
                var headers = {};
                var body = payload;

                return post(("" + urlPath), headers, body);
            },
            
            sendLinkToGuest: function sendLinkToGuest(contractNumber) {
                var urlPath = '/send-link';
                var headers = {
                    cache: "no-store",
                    pragma: "no-cache"
                };

                return get((urlPath + "/" + contractNumber), headers);
            },

            updateGuestData: function updateGuestData(contractNumber, data, customer_id) {
                var urlPath = '/bookings';
                var headers = {};
                var body = data;

                return put((urlPath + "/" + contractNumber + "/guests/" + customer_id), headers, body);
            },

            shiftPasscodeHour: function shiftPasscodeHour(passcodeId, shiftType) {
                var urlPath = '/passcodes';
                var headers = {};
                var body = {
                    edit_value: shiftType
                };

                return post((urlPath + "/" + passcodeId + "/shift-period"), headers, body);
            }
        };
    }

    var StartupChecks = {
        OK: 0,
        ERROR_CREDENTIALS_INVALID: 1,
        ERROR_ACCOUNT_WO_PERMISSIONS: 2,
        ERROR_PHONE_NOT_FOUND: 3,
        ERROR_PHONE_RESTRICTED: 4,
        ERROR_CHECKINDATE_IN_PAST: 5,
        ERROR_FULLNAME_INVALID: 6,
    };

    function translateStartupError(startupCheck) {
        var message = '';
        switch (startupCheck) {
            case StartupChecks.ERROR_PHONE_NOT_FOUND:
                message = 'Не указан номер телефона у гостя';
                break;
            case StartupChecks.ERROR_PHONE_RESTRICTED:
                message = 'Номер телефона гостя не принадлежит Российским операторам сотовой связи';
                break;
            case StartupChecks.ERROR_ACCOUNT_WO_PERMISSIONS:
                message = "\n          Указанные в системе Signer логин-пароль от аккаунта в Bnovo не имеют доступа к данному отелю\n        ";
                break;
            case StartupChecks.ERROR_CREDENTIALS_INVALID:
                message = "\n          Signer не может подключиться к API Bnovo с указанными логином-паролем в системе Signer.\n        ";
                break;
            case StartupChecks.ERROR_CHECKINDATE_IN_PAST:
                message = "\n          Дата заезда должна быть в будущем времени\n        ";
                break;
            case StartupChecks.ERROR_FULLNAME_INVALID:
                message = "\n          В ФИО гостя присутствуют недопустимые символы\n        ";
                break;
        }
        return message;
    }

    var state = {
        isReady: false,
        startupCheck: null,
    };

    var getters = {
        isReady: function (state) { return state.isReady; },
    };

    var actions = {
        setWidgetReady: function setWidgetReady(ref) {
            var commit = ref.commit;

            commit('WIDGET_READY');
        },

        /**
         * Экшен, запускающий подругзку всех необходимых для работы данных
         */
        startup: async function startup(ref, opts) {
            var commit = ref.commit;

            var state = this.state;

            var contractId = opts.contractId;
            commit('COMMIT_CONTRACT_ID', contractId);

            var contractPhone = opts.contractPhone;
            commit('COMMIT_CONTRACT_PHONE', contractPhone);

            // Создания инстансов АПИ
            var esApiOpts = opts.esApi;
            commit('REGISTER_PARTNER_API', new BnovoApi());
            commit('REGISTER_ESWIDGET_API', new ESWidgetApi(esApiOpts.token, esApiOpts.domain, esApiOpts.partnerApi));

            // Получение необходимой информации пользователя виджета
            var partnerUser = await this.processApiRequest(commit, state.partnerApi.partnerUser());
            commit('COMMIT_PARTNER_USER', partnerUser);
            // Получение данных по бронированию, для которого оформляется Удалённое заселение
            var partnerData = await state.partnerApi.partnerData();
            partnerData.phone = contractPhone;
            commit('COMMIT_PARTNER_DATA', partnerData);

            // Получение стейта виджета с бекенда eSigner
            // hotel_id - ИД отеля, который привязан к аккаунту eSigner
            var hotel_id = state.partnerUser.hotel_id;
            var widgetState =
                await this.processApiRequest(commit, state.eswidgetApi.getWidgetState(hotel_id).then(function (r) { return r.json(); }));
            commit('COMMIT_WIDGET_STATE', widgetState.data);
            commit('COMMIT_WIDGET_BILLING_STATE', widgetState.data.billing_info);

            await this.dispatch('getWidgetContractState');

            await this.dispatch('startupChecks');
        },

        /**
         * Экшен, запускающий проверки данных для запуска виджета
         */
        startupChecks: async function startupChecks(ref) {
            var commit = ref.commit;

            var state = this.state;

            var checksStatus = StartupChecks.OK;
            // Если с бекенда пришло значение credentials_valid = false
            // То виджет должен приостановить работу
            if (!state.widgetState.credentials_valid) {
                checksStatus = StartupChecks.ERROR_CREDENTIALS_INVALID;
            }
            // Если с бекенда пришла ошибка о том, что бекенд не может работать с текущим отелем, т.к. логин пароль в системе Signer
            // для работы с АПИ bnovo корректны, но не имеют доступ к текущему отелю.
            if (!state.widgetState.enough_permissions) {
                checksStatus = StartupChecks.ERROR_ACCOUNT_WO_PERMISSIONS;
            }
            // Телефон должен быть заполнен в форме гостя
            if (!state.contractPhone) {
                checksStatus = StartupChecks.ERROR_PHONE_NOT_FOUND;
            } else {
                var phone = state.contractPhone.replaceAll(/[^\d^\+]+/g, '');
                var phoneCheck = phone.match(PartnerStoreHelper.CONTRACT_PHONE_REGEX);

                // Ограничение на формат номера телефона
                if (!phoneCheck) {
                    checksStatus = StartupChecks.ERROR_PHONE_RESTRICTED;
                }
            }

            // Дата заезда должна быть на текущий момент или позже
            if (state.esContractState.code === 0 && new Date(state.partnerData.arrival_datetime) < new Date()) {
                checksStatus = StartupChecks.ERROR_CHECKINDATE_IN_PAST;
            }

            // В ФИО гостя не должно быть недопустимых символов
            if ( state.partnerData ) {
                var fio = state.partnerData.first_name + state.partnerData.middle_name + state.partnerData.last_name;

                var fioCheck = fio.match(PartnerStoreHelper.GUEST_FULLNAME_REGEX);
                if (!fioCheck) {
                    checksStatus = StartupChecks.ERROR_FULLNAME_INVALID;
                }
            }

            // Пуш проваленной проверки как фатальной ошибки
            if (checksStatus != StartupChecks.OK) {
                commit('HANDLE_FATAL_ERROR', new Error(translateStartupError(checksStatus)));
            }

            commit('COMMIT_STARTUP_CHECKS', checksStatus);
        },
    };

    var mutations = {
        WIDGET_READY: function (state) { return state.isReady = true; },
        COMMIT_STARTUP_CHECKS: function (state, startupCheck) {
            state.startupCheck = startupCheck;
        },
    };

    var Startup = {
        state: state,
        getters: getters,
        actions: actions,
        mutations: mutations
    };

    function http (store) {
        store.handleApiRequestError = function (commit, request) {
            if (request && request.status) { switch (request.status) {
                case 401: throw new Error('Ошибка запроса: пользователь не авторизован в системе eSigner');
                case 404:
                    commit('HANDLE_WIDGET_WARNING', 'Запрашиваемый эндпоинт не существует');
                    break;
                case 500: throw new Error('Ошибка на стороне сервера eSigner');
            } }

            return request;
        };

        store.processApiRequest = function (commit, apiPromise) {
            return apiPromise
                .then(function (request) { return store.handleApiRequestError(commit, request); });
        };
    }

    function getAdultsFromCustomers(customers) {
      return customers.filter(function (customer) { return !(customer.extra && customer.extra.guest_type == 2); });
    }

    var store = new Vuex__default["default"].Store({
      plugins: [
        http
      ],
      modules: [
        Startup
      ],
      state: function () {
        return {
          /**
           * Флаг, при котором(по идее) отображается оверфлоу предзагрузчика
           */
          isLoadingData: true,

          /**
           * Флаг отвечающий за состояние виджета "в процессе создания контракта"
           */
          isContractCreating: false,

          isUsingLockService: false,

          hasFatalError: false,
          fatalError: null,

          hasWarning: false,
          warning: null,

          confirmationPromise: null,

          /**
           * 
           */
          partnerApi: null,
          eswidgetApi: null,

          /**
           * Информация по сущности для которой оформляется удаленное заселение
           */
          partnerData: {
            customers: []
          },
          /**
           * Информация по текущему пользователю.
           */
          partnerUser: {},

          /**
           * ID бронирования
           */
          contractId: null,
          /**
           * Телефон гостя, для которого создается удаленное заселение
           */
          contractPhone: null,
          /**
           * Номер удаленного заселения, который используется как уникальный идентификатор в системе eSigner 
           */
          contractNumber: null,

          /**
           * Данные по удаленному заселению в системе eSigner
           */
          esContractState: {
            code: 'loading'
          },

          /**
           * Данные по биллингу аккаунта пользователя
           */
          billingState: null,

          /**
           * Данные, полученные с бекенда eSigner
           * В данном массиве содержится информация о том, может ли клиент(!), у которого работает
           * виджет, с ним работать.
           */
          widgetState: {},

          /**
           * Объект интервала обновления состояния виджета
           */
          updaterInterval: null,

          /**
           * Список сервисов виджета
           */
          services: {
            remote_lock_management: {
              isAvailable: false,
              status: 'loading',
              locks: []
            },
            send_sms_link_to_client: {
              isAvailable: false,
            },
            multi_guest_signing: {
              isAvailable: false,
            },
            passport_ocr: {
              isAvailable: false,
            }
          }
        };
      },
      getters: {
        getWidgetApi: function (state) { return state.eswidgetApi; },
        getWidgetBillingState: function (state) { return state.billingState; },
        getPartnerData: function (state) { return state.partnerData; },
        getPartnerUser: function (state) { return state.partnerUser; },
        getWidgetTimezone: function (state) { return state.partnerUser.timezone; },

        isLoadingData: function (state) { return state.isLoadingData; },
        isContractCreating: function (state) { return state.isContractCreating; },
        isConfirmationRequired: function (state) {
          return state.confirmationPromise;
        },
        getConfirmationRequest: function (state) {
          return state.confirmationPromise;
        },
        getClientData: function getClientData(state) {
          return {
            phone: state.contractPhone,
            firstName: state.partnerData.first_name,
            lastName: state.partnerData.last_name,
            middleName: state.partnerData.middle_name,
            numberOfGuests: state.partnerData.number_of_guests
          };
        },
        isServiceAvailable: function isServiceAvailable(state) {
          return function (service) { return state.services[service].isAvailable; };
        },
        getServiceStatus: function getServiceStatus(state) {
          return function (service) { return state.services[service].status; };
        },
        isLocksServiceConfigured: function (state) { return state.widgetState.ttlock_account_is_set; },

        getAdultsCount: function (state) { return state.partnerData.number_of_adults; },
        getPrimaryCustomerId: function (state) { return state.partnerData.customer_id; },
        getAdultCustomers: function (state) {
          return getAdultsFromCustomers(state.partnerData.customers);
        },
        getAdultCustomersWithPhones: function (state) {
          var regex = PartnerStoreHelper.CONTRACT_PHONE_REGEX;
          var primaryCustomerId = state.partnerData.customer_id;

          return getAdultsFromCustomers(state.partnerData.customers).filter(function (customer) {
            return customer.id != primaryCustomerId
              && customer.phone.length > 0
              && customer.phone.replaceAll(/[^\d]+/g, '').match(regex);
          });
        },

        getContractId: function (state) { return state.contractId; },
        getContractNumber: function (state) { return state.contractNumber; },
        getContractState: function (state) { return state.esContractState; },
        isContractSigned: function (state) { return PartnerStoreHelper.signedStatuses.includes(state.esContractState.code); },

        hasFatalError: function (state) { return state.hasFatalError; },
        getFatalError: function (state) { return state.fatalError; },
        hasWidgetWarning: function (state) { return state.hasWarning; },
        getWidgetWarning: function (state) { return state.warning; },

        getPartnerTemplates: function (state) { return state.partnerData.templates; },
        getPartnerCheckinTemplates: function (state) { return state.partnerData.checkinTemplates; },
        getFirstRoomId: function (state) { return state.partnerData.apartments[0].room_id; },
        getApartments: function (state) { return state.partnerData.apartments; },
        getPasscodes: function (state) {
          var contracts = state.esContractState.bookings;
          var passcodes = [];
          if (contracts) { for (var i = 0; i < contracts.length; i++) {
            var contract = contracts[i];
            if (contract.is_lock_management) { for (var j = 0; j < contract.passcodes.length; j++) {
              var passcode = contract.passcodes[j];
              passcodes.push(passcode);
            } }
          } }
          return passcodes;
        }
      },
      mutations: {
        REGISTER_PARTNER_API: function (state, api) { return state.partnerApi = api; },
        REGISTER_ESWIDGET_API: function (state, api) { return state.eswidgetApi = api; },
        COMMIT_WIDGET_BILLING_STATE: function (state, billingState) {
          if (!state.billingState) {
            state.billingState = billingState;
          } else {
            Object.assign(state.billingState, billingState);
          }

          // Изменение флага доступности сервиса замков
          state.services.remote_lock_management.isAvailable = billingState.available_services.includes(PartnerStoreHelper.SERVICES.REMOTE_LOCK_MANAGEMENT);
          state.services.send_sms_link_to_client.isAvailable = billingState.available_services.includes(PartnerStoreHelper.SERVICES.SEND_SMS_LINK_TO_CLIENT);
          state.services.multi_guest_signing.isAvailable = billingState.available_services.includes(PartnerStoreHelper.SERVICES.MULTI_GUEST_SIGNING);
          state.services.passport_ocr.isAvailable = billingState.available_services.includes(PartnerStoreHelper.SERVICES.PASSPORT_OCR);
        },
        COMMIT_PARTNER_USER: function (state, partnerUser) { return state.partnerUser = partnerUser; },
        COMMIT_PARTNER_DATA: function (state, partnerData) {
          state.partnerData = partnerData;
          state.contractNumber = partnerData.contract_number;
        },
        COMMIT_WIDGET_STATE: function (state, widgetState) { return state.widgetState = widgetState; },
        COMMIT_ES_CONTRACT_STATE: function (state, esContractState) {
          state.esContractState = esContractState;
        },
        UPDATE_CONTRACT: function UPDATE_CONTRACT(state, ref) {
          var target = ref.target;
          var contract = ref.contract;

          Object.assign(target, contract);
        },

        IS_WIDGET_LOADING_DATA: function (state, isLoadingData) { return state.isLoadingData = isLoadingData; },
        IS_WIDGET_CONTRACT_CREATING: function (state, isContractCreating) { return state.isContractCreating = isContractCreating; },
        HANDLE_FATAL_ERROR: function HANDLE_FATAL_ERROR(state, error) {
          state.hasFatalError = true;
          state.fatalError = error;
          state.isLoadingData = false;
        },
        HANDLE_WIDGET_WARNING: function HANDLE_WIDGET_WARNING(state, warning) {
          state.hasWarning = true;
          state.warning = warning;
        },
        CLEAN_WARNING: function CLEAN_WARNING(state) {
          state.hasWarning = false;
          state.warning = null;
        },
        SHOW_CONFIRM_REQUEST_SCREEN: function (state, promise) { return state.confirmationPromise = promise; },
        REMOVE_CONFIRM_REQUEST_SCREEN: function (state) { return state.confirmationPromise = null; },
        SET_UPDATER_INTERVAL: function SET_UPDATER_INTERVAL(state, interval) {
          state.updaterInterval = interval;
        },
        COMMIT_CONTRACT_ID: function (state, contractId) { return state.contractId = contractId; },
        COMMIT_CONTRACT_PHONE: function (state, contractPhone) { return state.contractPhone = contractPhone; },
        SET_SERVICE_LOCKS_STATUS: function (state, status) { return state.services.remote_lock_management.status = status; },
        UPDATE_APARTMENTS: function (state, apartments) { return state.partnerData.apartments = apartments; },
        UPDATE_APARTMENTS_SCHEDULE: function (state, schedule) { return state.partnerData.schedule = schedule; },
        UPDATE_PASSCODE: function (state, opts) {
          var passcode = opts.target;
          var data = opts.data;

          passcode.start_datetime = data.start_datetime || passcode.start_datetime;
          passcode.end_datetime = data.end_datetime || passcode.end_datetime;
          passcode.passcode = data.code || passcode.passcode;
          passcode.status_id = data.status || passcode.status_id;
        }
      },
      actions: {
        handleFatalError: async function handleFatalError(ref, error) {
          var commit = ref.commit;

          commit('HANDLE_FATAL_ERROR', error);
        },
        handleWidgetWarning: async function handleWidgetWarning(ref, warning) {
          var commit = ref.commit;

          commit('HANDLE_WIDGET_WARNING', warning);
        },
        cleanWarning: function cleanWarning(ref) {
          var commit = ref.commit;

          commit('CLEAN_WARNING');
        },

        getWidgetContractState: async function getWidgetContractState(ref) {
          var commit = ref.commit;
          var state = ref.state;

          var contractNumber = state.partnerData.contract_number;

          // Получение информации по удаленному заселению в eSigner
          var esContractState = await this.processApiRequest(
            commit,
            state.eswidgetApi.getContractState(contractNumber).then(function (r) { return r.json(); })
          );

          commit('COMMIT_ES_CONTRACT_STATE', esContractState);
        },

        /**
         * Заново загружает стейт контракта
         * 
         * Метод используется сокетом при получении сообщения 'application_created'
         * 
         * Т.к. контрактов может создаться за раз несколько штук, например при оформлении с сервисом
         * множественного заселения, а в сообщении всего один контракт - обновляется стейт виджета полностью.
         */
        contractCreated: async function contractCreated(ref, data) {
          ref.commit;
          var state = ref.state;

          var contractNumber = state.partnerData.contract_number;

          if(data.contract_number.startsWith(contractNumber)) {
            this.dispatch("contractCreating", true);
            this.dispatch('togglePreloader', true);
            await this.dispatch('getWidgetContractState');
            this.dispatch("contractCreating", false);
            this.dispatch('togglePreloader', false);
          }
        },

        /**
         * Обновляет контракт находящийся в стейте виджета
         * 
         * Метод используется сокетом при получении сообщения 'application_updated'
         */
        updateContract: function updateContract(ref, contract) {
          var commit = ref.commit;
          var state = ref.state;

          var bnovo_customer_id = contract.bnovo_customer_id;
          var contractNumber = state.partnerData.contract_number;

          var contracts = state.esContractState.bookings;
          if (contracts && contract.contract_number.startsWith(contractNumber)) {
            var target = contracts.find(function (el) { return el.bnovo_customer_id == bnovo_customer_id; });

            if (target) {
              commit('UPDATE_CONTRACT', { target: target, contract: contract });
              // Если обновление пришло по основному бронированию, то contract_number будет без суффикса "/1" и т.д.
              // В таком случае надо обновить основное состояние контракта (кода)
              if(contract.contract_number.split('/').length == 1) {
                commit('COMMIT_ES_CONTRACT_STATE', Object.assign(state.esContractState, {code: contract.code}));
              }
              this.dispatch('togglePreloader', false);
            }
          }
        },

        /**
         * Загрузка информации по замкам
         */
        loadApartmentsLocks: async function loadApartmentsLocks(ref) {
          var commit = ref.commit;
          var state = ref.state;

          var serviceLocksStatus = ServiceLocksStatus.OK;

          var apartments = state.partnerData.apartments;
          var locks_data = null;

          if (apartments.length < 1) {
            serviceLocksStatus = ServiceLocksStatus.PERIOD_NOT_SET;
          } else {
            locks_data =
              await this.processApiRequest(
                commit,
                state.eswidgetApi.getApartmentsLocksData(apartments).then(function (r) { return r.json(); })
              );
          }

          if (locks_data.status === 'ok') {
            commit('COMMIT_ESIGNER_LOCKS', locks_data.locks);

            var withoutLocksCount = 0;
            var isRemoteBookingPossibleWithLocks = true;

            var loop = function ( i ) {
              var el = apartments[i];

              var apart = locks_data.apartments.find(function (data_el) {
                return data_el.room_id == el.room_id;
              });

              if (apart !== undefined) {
                var locks = locks_data.locks.filter(function (data_lock) {
                  return apart.locks.includes("" + data_lock.lockId);
                });

                el['isSignerExists'] = true;
                el['locks'] = locks;
              } else {
                // Если апартаменты не были найдены - в сайнере требуется синхронизация
                el['isSignerExists'] = false;
                el['locks'] = [];
                serviceLocksStatus = ServiceLocksStatus.REQUIRED_SYNCH;
              }

              if (el['room_id'] == 0) {
                isRemoteBookingPossibleWithLocks = false;
                serviceLocksStatus = ServiceLocksStatus.PERIOD_NOT_SET;
              }

              if (el['locks'].length == 0) {
                ++withoutLocksCount;
              }

              if (isRemoteBookingPossibleWithLocks && withoutLocksCount == apartments.length) {
                isRemoteBookingPossibleWithLocks = false;
                serviceLocksStatus = ServiceLocksStatus.EMPTY_LOCKS;
              }
            };

            for (var i = 0; i < apartments.length; i++) loop( i );
          } else if (locks_data.status === "error") {
            switch (locks_data.code) {
              case 'system':
                this.dispatch('handleWidgetWarning', 'Ошибка сервиса: ' + locks_data.error);
                break;
              case 'ttlock_timeout':
                serviceLocksStatus = ServiceLocksStatus.TIMEOUT;
                break;
              default:
                throw new Error('Получена неизвестная ошибка при работе сервиса замков TTLock');
            }
          }

          var bookingId = state.partnerData.bnovo_booking_id;
          var schedule =
            await new Promise(function (resolve) {
              // loadBookingData - глобальная функция bnovo
              loadBookingData(bookingId, function () {
                var schedule = [];
                Object.keys(booking_data).forEach(function (date) {
                  var el = booking_data[date];

                  schedule.push({
                    apartments_id: el.room_id,
                    apartments_real_type_id: el.real_room_type_id,
                    date: date
                  });
                });
                resolve(schedule);
              });
            });

          commit('UPDATE_APARTMENTS', apartments);
          commit('UPDATE_APARTMENTS_SCHEDULE', schedule);
          commit('SET_SERVICE_LOCKS_STATUS', serviceLocksStatus);
        },

        /**
         * 
         */
        updateState: async function updateState(ref, interval) {
          var this$1$1 = this;
          var commit = ref.commit;
          var state = ref.state;

          var i = (interval !== undefined ? interval : 5000);
          var contract_number = state.partnerData.contract_number;
          var identifier = PartnerStoreHelper.getContractIdentifier(state.esContractState);

          // Обработка случая если проверка состояния была запущена еще раз - вырубаем предыдущий интервал
          if (state.updaterInterval) {
            clearInterval(state.updaterInterval);
          }

          var updaterInterval = setInterval(async function () {
            this$1$1.processApiRequest(
              commit,
              state.eswidgetApi.getContractState(contract_number).then(function (r) { return r.json(); })
            ).then(function (result) {
              if (result) {
                var newIdentifier = PartnerStoreHelper.getContractIdentifier(result);

                if (newIdentifier != identifier) {
                  identifier = newIdentifier;
                  commit('COMMIT_ES_CONTRACT_STATE', result);
                  this$1$1.dispatch('togglePreloader', false);
                  this$1$1.dispatch("contractCreating", false);
                }
              } else {
                console.warn('[ESigner] Ошибка при обновлении статуса контракта', result);
                clearInterval(this$1$1.updaterInterval);
              }
            });
          }, i);

          commit('SET_UPDATER_INTERVAL', updaterInterval);
        },

        sendLinkToGuest: async function sendLinkToGuest(ref) {
          ref.commit;
          var state = ref.state;

          var contractId = state.contractNumber;
          return state.eswidgetApi.sendLinkToGuest(contractId).then(function (r) { return r.json(); });
        },

        changeGuestPhone: async function changeGuestPhone(ref, ref$1) {
          ref.commit;
          var state = ref.state;
          var newPhone = ref$1[0];
          var contract = ref$1[1];

          var customer_id = contract.bnovo_customer_id;
          var contractId = state.contractNumber;

          return state.eswidgetApi.updateGuestData(contractId, { phone: newPhone }, customer_id).then(function (r) { return r.json(); });
        },

        shiftPasscodeHour: async function shiftPasscodeHour(ref, data) {
          ref.commit;
          var state = ref.state;

          return state.eswidgetApi.shiftPasscodeHour(data.id, data.shiftType).then(function (r) { return r.json(); });
        },

        initSocket: async function initSocket(ref, opts) {
          var commit = ref.commit;
          ref.state;

          var socket = new ESignerSocket(opts.token, opts.wssUrl, this).start(opts.libPath);
          commit("UPDATE_SOCKET", socket);
        },

        updatePasscode: async function updatePasscode(ref, passcode) {
          var commit = ref.commit;
          ref.state;

          var passcodes = this.getters.getPasscodes;

          var target = passcodes.find(function (el) { return passcode.id == el.id; });
          if (target) {
            commit("UPDATE_PASSCODE", { target: target, data: passcode });
          }
        },

        updateWidgetBillingState: async function updateWidgetBillingState(ref, billingState) {
          var commit = ref.commit;

          commit("COMMIT_WIDGET_BILLING_STATE", billingState);
          commit("REMOVE_CONFIRM_REQUEST_SCREEN");
        },

        // Фунционал показа экрана подтверждения действия
        showConfirmationScreen: function showConfirmationScreen(ref, data) {
          var commit = ref.commit;

          var message = data.message || 'Требуется подтверждение действия';

          var callback = null;
          var promise = new Promise(function (resolve) {
            var onAccept = data.onAccept || (function (_) { return _; });
            var onDecline = data.onDecline || (function (_) { return _; });

            callback = function (value) {
              commit("REMOVE_CONFIRM_REQUEST_SCREEN");
              value ? onAccept() : onDecline();
              resolve(value);
            };
          });
          promise.action = callback;
          promise.message = message;

          commit("SHOW_CONFIRM_REQUEST_SCREEN", promise);
        },

        togglePreloader: function togglePreloader(ref, value) {
          var commit = ref.commit;
          var state = ref.state;

          if (value != state.isLoadingData) {
            commit('IS_WIDGET_LOADING_DATA', value);
          }
        },

        contractCreating: function contractCreating(ref, value) {
          var commit = ref.commit;
          var state = ref.state;

          if (value != state.isContractCreating) {
            commit('IS_WIDGET_CONTRACT_CREATING', value);
          }
        }
      },
    });

    var locales = {
        ru: {
            'checkin_datetime': 'Дата заезда',
            'checkout_datetime': 'Дата выезда',
            'bookingId': 'ID бронирования',
            'customerId': 'Внешний ИД гостя',
            'first_name': 'Имя',
            'last_name': 'Фамилия',
            'middle_name': 'Отчество',
            'phone': 'Телефон',
            'email': 'E-mail гостя',
            'number_of_adults': 'Количество взрослых гостей',
            'status.new': 'Приглашение к подписанию сформировано, но не отправлено автоматически.',
            'status.link_sent': 'Приглашение к подписанию сформировано и отправлено гостю',
            'status.visited': 'Гость перешел по ссылкe, заполняет персональные данные,<br/>вы получите уведомление после подписания',
            'status.waiting_signing_of_other_contracts': 'Гость подписал свой договор. Ожидание подписания другими гостями',
            'status.signed': 'Договор успешно подписан',
            'locks_status.loading': 'Получение информации по возможности оформления с замками...',
            'locks_status.ok': 'Доступно оформление удалённого заселения с замками',
            'locks_status.locks_not_exists': 'В бронировании нет привязанных к апартаментам замков.<br/>В данный момент доступно только подписание документов.',
            'locks_status.period_not_set': 'В бронировании есть периоды, которые не распределены по номерам.<br/>В данный момент доступно только подписание документов.',
            'locks_status.required_synchronization': 'В бронировании есть апартаменты, не синхронизированные с системой eSigner.<br/>Пожалуйста, подождите некоторое время.<br/>В данный момент доступно только подписание документов.',
            'locks_status.account_not_set': 'Ваш аккаунт не синхронизирован с TTLock.<br/>В данный момент доступно только подписание документов.',
            'locks_status.ttlock_timeout': 'Сервис замков недоступен, попробуйте обновить страницу.<br/>В данный момент доступно только подписание документов.',
            'passcode_status.failed': 'Ошибка установки',
            'passcode_status.default': 'Устанавливается',
            'passcode_status.expired': 'Истек',
            'passcode_error.ttlock_timeout': 'Ошибка запроса к TTLock, попробуйте позже',
            'passcode_error.gateway_busy': 'Шлюз временно занят, попробуйте позже',
            'passcode_error.gateway_is_offline': 'Шлюз отключен, включите и попробуйте снова',
            'passcode_error.lock_wo_gateway': 'Замок не подключен к шлюзу',
            'passcode_error.unexpected': 'Неизвестная ошибка при выполнении операции, попробуйте позже',
        }
    };

    function $tt(msgid) {
        var translate = locales.ru[msgid];
        if (translate !== undefined) {
            return translate;
        } else {
            return msgid;
        }
    }

    // Объявление функции установки, выполняемой Vue.use()
    function install(Vue) {
      if (install.installed) { return; }
      install.installed = true;

      Vue.use(Vuex__default["default"]);

      // https://stackoverflow.com/questions/53089441/how-to-access-vuex-from-vue-plugin
      Vue.eswStore = store;
      Vue.prototype.$eswStore = store;

      // Подключение локалей
      Vue.$tt = $tt;
      Vue.prototype.$tt = $tt;

      Vue.component('ESWIdget', __vue_component__);
    }

    // Создание значения модуля для Vue.use()
    var plugin = {
      install: install
    };

    // Автоматическая установка, когда vue найден (например в браузере с помощью тега <script>)
    var GlobalVue = null;
    if (typeof window !== 'undefined') {
      GlobalVue = window.Vue;
    } else if (typeof global !== 'undefined') {
      GlobalVue = global.Vue;
    }
    if (GlobalVue) {
      GlobalVue.use(plugin);
    }

    exports["default"] = __vue_component__;
    exports.install = install;

    Object.defineProperty(exports, '__esModule', { value: true });

}));
//# sourceMappingURL=widget.js.map
