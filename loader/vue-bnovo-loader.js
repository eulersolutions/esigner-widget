import 'https://cdn.jsdelivr.net/npm/vue@2.6.14/dist/vue.js';
import 'https://cdn.jsdelivr.net/npm/vuex@3.6.2/dist/vuex.min.js';

(function() {

    window._eswc = Object.assign({
        contractId: $('#bookingID').val(),
        contractPhone: $('[name="phone"]').val(),
        placeSelector: '.generalPageGuestContainer.m-hr',
        token: '',
        domain: 'widget-api.esigner.ru',
        partnerApi: '/partner_api/bnovo/v1',
        widgetLoaderPath: '/widget_loader/bnovo/v2',
        socketLibPath: '/js/phoenix-1.6.2.min.js',
        socketPath: '/widget/bnovo/v1'
    }, (window._eswc || {}));
    const opts = window._eswc;

    const _esRepo = `//${opts.domain}${opts.widgetLoaderPath}/component`;
    let _wPath = ``;
    if(opts.dev) _wPath = `//${(opts.dev_domain ? opts.dev_domain : 'localhost:3011')}/umd/widget.js`;
    else _wPath = _esRepo;
    
    import(_wPath).then(() => {
        if(typeof window._eswc.contractId === 'string') widget();
        else console.warn('Запуск виджета eSigner приостановлен, не обнаружен номер бронирования в переменной window._eswc.contractId');
    });

    function widget() {
        $(opts.placeSelector).append($('<div id="esigner-widget"/>'));
    
        const esBnovoWidget = {
            data: function() {
                return {
                    showModal: false,
                    showPreloader: true,
                    hasFatalError: false,
                    modalStyles: modalStyles(),
                    btnClasses: {},
                    billingInfo: null
                };
            },
            ready() {
                modalStyles();
            },
            computed: {
                tarifBalance() {
                    let balance = 0;
                    if(this.billingInfo.tarif_type_id == 'trial') {
                        const remaining_days = Math.round((new Date(this.billingInfo.trial_ends_at) - new Date()) / (1000*60*60*24));
                        balance = `Остаток дней: ${remaining_days > 0 ? remaining_days : 0}`;
                    } else {
                        balance = `Остаток заселений: ${this.billingInfo.balance}`;
                    }
                    return balance;
                },
                tarifName() {
                    return this.billingInfo.tarif_name || 'Не выбран';
                }
            },
            template: `
                <div>
                    <div class="switcher m-margin">
                        <div style="display: none;">
                            <div class="modal" id="_es_modal">
                                <div v-show="showPreloader" class="s-loading-overflow"><div class="preloader"></div></div>
                                <div class="grid m-padding m-cellpadding"> \
                                    <div class="grid__cell"> \
                                        <ESWidget
                                            v-on:showpreloader="togglePreloader"
                                            v-on:updatestate="updateState"
                                            v-on:updatebilling="updateBilling"
                                            v-on:fatalerror="gotFatalError"
                                        ></ESWidget>
                                    </div>
                                </div>
                                <div class="popup-btn-close arcticmodal-close" v-on:click="closeESModal"></div>
                            </div>
                        </div>
                        <button class="switcher__item" :class="btnClasses" v-on:click="showESModal">
                            Удалённое заселение
                        </button>
                    </div>
                    <template v-if="!hasFatalError">
                        <div v-if="billingInfo" class="switcher m-margin _es-billing-data">
                            <div class="_es-billing-data__item">
                                <b>Тариф: {{ tarifName }}</b><br/>
                                {{ tarifBalance }}
                            </div>
                        </div>
                        <div v-if="billingInfo" class="g-fr">
                            <a :href='billingInfo.payment_page_url' target='_blank' class='switcher__item button m-primary'>
                                Купить
                            </a>
                        </div>
                        <div v-if="!billingInfo" class="switcher m-margin _es-billing-data">
                            <div class="_es-billing-data__item">
                                Загрузка...
                            </div>
                        </div>
                    </template>
                </div>
            `,
            methods: {
                showESModal() {
                    window.jQuery('#_es_modal').arcticmodal();
                },
                closeESModal() {
                    window.jQuery('#_es_modal').arcticmodal('close');
                },
                togglePreloader(event) {
                    this.showPreloader = event.value;
                },
                updateState(state) {
                    this.btnClasses = {};
                    switch(state.status) {
                        case 0:
                            break;
                        case 'new':
                        case 'link_sent':
                            this.btnClasses['status m-yellow-contrast'] = true;
                            break;
                        case 'visited':
                            this.btnClasses['status m-blue-contrast'] = true;
                            break;
                        case 'signed':
                        case 'waiting_signing_of_other_contracts':
                            this.btnClasses['status m-green-contrast'] = true;
                            break;
                        case 'canceled':
                        case 'expired':
                            this.btnClasses['status m-red-contrast'] = true;
                            break;
                    }
                },
                updateBilling(billingInfo) {
                    this.billingInfo = billingInfo;
                },
                gotFatalError() {
                    this.hasFatalError = true;
                }
            },
            components: { 'ESWidget': ESWidget.default }
        };
    
        const esComponent = Vue.extend(esBnovoWidget);
        new esComponent({}).$mount('#esigner-widget');
    
        function modalStyles() {
            window.jQuery('head').append(`
                <style id="_es_widget_styles">
                    .s-loading-overflow {position: absolute; width: 100%; height: 100%; top: 0; left: 0; z-index: 10; background-color: rgba(128, 128, 128, 0.2);}
                    .s-loading-overflow .preloader {position: absolute; width: 64px; height: 64px; top: calc(50% - 32px); left: calc(50% - 32px);}
                    ._es-billing-data{font-size: 12px;}
                    @media (max-width: 450px) {._es-billing-data {width: 70%;}}
                </style>
            `);
        }
    }
})()