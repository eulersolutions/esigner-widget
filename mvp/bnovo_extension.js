const LOGGING = true;
const SIGNER_URL = 'http://localhost:4011/partner_api/bnovo/v1';
const SIGNER_WSS_URL = 'ws://localhost:4011/widget/bnovo/v1';
const PHOENIX_SOCKET_LIB_PATH = 'http://localhost:4011/js/phoenix-1.6.2.min.js';
const SIGNER_TOKEN = 'sometoken';

const SIGNER_PHONE_REGEX = /^\+?7\d{10}$|^8\d{10}$/;

$(document).ready(function (e) {
    // Если обнаружена переменная токена, которая может вставиться только со стороны биново то не запускать виджет
    let token = null;
    if(window._eswc && window._eswc.token) {
        return;
    } else {
        token = SIGNER_TOKEN;
    }

    const bookingid = $('#bookingID').val();


    // Если на странице есть ИД бронирования, то будем считать что мы находися на странице бронирования
    // и можно запускать виджет
    if (bookingid) {
        // Инициализация виджета
        const signer = new Signer({
            parentSelector: '.generalPageGuestContainer',
            token: token,
            bookingId: bookingid
        });

        signer.attachWidget();
    }

    // Инициализация блока уведомлений, полученных с eSigner
    const signerNotifications = new SignerNotifications(); // = null

    // Инициализация вебсокета для получения сообщений с бекенда eSigner
    const signerSocket = new SignerSocket(SIGNER_TOKEN, signerNotifications);
    signerSocket.joinLegalEntityChannel();
});

/**
 *
 * @param {Object} opts - Настройки виджета
 * @param {String} opts.parentSelector - селектор родительского блок, куда нужно вставить кнопку вызова виджета
 * @returns
 */
function Signer(opts) {

    const STARTUP_ERROR_CREDENTIALS_INVALID = 200;
    const STARTUP_ERROR_PHONE_NOT_FOUND = 201;
    const STARTUP_ERROR_PHONE_RESTRICTED = 202;
    const STARTUP_ERROR_ACCOUNT_WO_PERMISSIONS = 203;

    const apiUrl = SIGNER_URL;
    const apiUrlRoutes = {
        checkBooking: '/bookings',
        newBooking: '/bookings',
        newBookingWithLocks: '/bookings-locks',
        lock_management: {
            // GET эндпоинт на получение информации по замкам
            apartments_locks: '/lock_managment/apartments_locks',
            passcodes: '/passcodes'
        },
        rejectBooking: '/bookings',
        updateGuest: '/bookings',
        sendLink: '/send-link',
        getWidgetOptions: '/widget',
    };

    let parentSelector;
    if (opts && opts.parentSelector) {
        parentSelector = opts.parentSelector;
    } else {
        throw "Signer: Не задан обязательный параметр parentSelector";
    }

    if (!(opts && opts.token && (typeof opts.token) === 'string')) {
        throw "Signer: Не задан обязательный параметр token";
    }
    let authToken = opts.token;

    const widget = new SignerWidget(parentSelector, {})

    /**
     * Получает список доступных кастомных шаблонов из выпадающего списка на странице бронирования
     *
     * @returns {Array} Список доступных кастомных шаблонов
     */
    function getCustomTemplatesList() {
        let list = {};

        $('.drop__sublinks .drop__link').each( function() {
            const href = $(this).attr('href');
            if( href.startsWith('/booking/doc/') ) {
                const url = new URL(href, 'https://' + window.location.hostname);
                // Исключение дублей шаблонов, т.к. поиск по шаблонам может найти блоки, например, для мобилки
                list[url.searchParams.get( 'template' )] = null;
            }
        });

        return Object.keys(list);
    }

    function ajaxSigner(opts) {
        let postprocessed = opts;

        postprocessed.beforeSend = function (xhr) {
            xhr.setRequestHeader('Authorization', 'Bearer ' + authToken);
        };

        postprocessed.error = function (data) {
            console.log('Ошибка при выполнении запроса к АПИ Signer');

            switch (data.status) {
                case 401:
                    widget.showCriticalError('Ошибка при выполнении запроса, необходимa авторизация.');
                    break;

                case 500:
                    widget.showCriticalError('Ошибка на стороне сервера');
                    break;

                default:
                    widget.showCriticalError('Неизвестная ошибка при выполнении запроса');
                    break;
            }
        };

        $.ajax(postprocessed);
    }

    /**
     * Делает запрос к Signer API для получения настроек для запуска виджета
     *
     * @returns {Promise}
     */
    function getWidgetState() {
        const requestUrl = apiUrl + apiUrlRoutes.getWidgetOptions;

        return new Promise(function(resolve, reject) {
            ajaxSigner({
                url: requestUrl,
                method: 'GET',
                cache: false,
                data: {hotel_id: window.current_hotel_id},
                success: function (data) {
                    resolve(data.data);
                },
                error: function(error) {
                    reject(error);
                }
            });
        });
    }

    /**
     * Получает информацию по бронированию из Bnovo
     *
     * Подразумевается что метод должен вызываться на авторизованной странице PMS Bnovo т.к. для запроса необходима авторизация.
     *
     * @param {Int} bookingId - ИД бронирования в Bnovo
     * @returns {Promise}
     */
    function getBookingData(bookingId) {
        return fetch('/booking/general/' + bookingId, {headers: {Accept: 'application/json'}});
    }

    function getBnovoAccountData() {
        return new Promise(function (resolve, reject) {
            $.ajax({
                url: '/account/current',
                withCredentials: true,
                dataType: 'json',
                success: (data) => {
                    resolve({
                        bnovo_user_email: data.current_user.email,
                        notify_list: [
                            data.current_user.email,
                            data.hotel.owner_email
                        ],
                        timezone: data.hotel.timezone,
                        currency: data.hotel.currency,
                        language: data.hotel.language
                    });
                },
                error: (error) => {
                    dd("Ошибка при получении информации об аккаунте для инициализации виджета ESigner", error);
                    reject(error);
                }
            });
        });
    }

    /**
     * Метод запрашиваемый с eSigner данные по апартаментам и их замкам
     *
     * @param {int[]} apartments_room_ids - список `room_id` апартаментов бронирования
     */
    function getSignerApartmentsLocksData(apartments_room_ids) {
        const requestUrl = apiUrl + apiUrlRoutes.lock_management.apartments_locks;

        return new Promise(function(resolve, reject) {
            ajaxSigner({
                url: requestUrl,
                method: 'GET',
                cache: false,
                data: {apartments: apartments_room_ids},
                success: function (data) {
                    resolve(data);
                },
                error: function(error) {
                    reject(error);
                }
            });
        });
    }

    return {
        /**
         * @type SignerWidget
         */
        widget: widget,
        updaterInterval: null,

        previousBookingStatuses: null,
        previousResponseCode: null,

        _opts: opts,

        /**
         * Информация по запуску виджета, полученная с бекенда eSigner.
         *
         * Переменная инициализируется в случае успешного вызова `attachWidget`
         */
        state: null,

        /**
         * Данные по бронированию, на странице которого проинициализирован виджет.
         *
         * Переменная инициализируется в случае успешного вызова `attachWidget`
         */
        booking: null, // Данные по бронированию полученные с АПИ Bnovo
        bookingApartments: [],
        bookingLocks: [],

        /**
         * Данные аккаунта оператора, на странице которого проинициализирован виджет.
         *
         * Переменная инициализируется в случае успешного вызова `attachWidget`
         */
        bnovoAccount: null,

        /**
         * Флаг означающий что виджет находится в процессе регистрации удаленного заселения
         * без замков
         */
        isRemoteBooking: false,

        /**
         * Флаг означающий что виджет находится в процессе регистрации удаленного заселения
         * с замками
         */
        isLockRegistration: false,

        /**
         *
         * @returns {Object} Результат ответа
         */
        checkBookingInfo: function () {
            const collectedData = this.collectBookingData();

            let result = null;
            let requestUrl = apiUrl + apiUrlRoutes.checkBooking + '/' + collectedData.contract_id;

            ajaxSigner({
                url: requestUrl,
                method: 'GET',
                async: false,
                withCredentials: true,
                contentType: 'application/json',
                success: function (data) {
                    result = data;
                }
            })

            console.log('Запрос данных по удаленному заселению', requestUrl, result);

            return result;
        },

        /**
         * Отправка данных по созданию бронирования
         *
         * @param {*} form Объект формы
         */
        sendRemoteBookingRequestForm: function (form) {
            const requestUrl = apiUrl + apiUrlRoutes.newBooking;

            dd('Отправка запроса на удаленное заселение', requestUrl);

            const $this = this;
            const collectedData = Object.assign($this.collectBookingData(), $this._opts.bnovoAccount, formDataToJson(form));

            ajaxSigner({
                method: 'POST',
                url: requestUrl,
                data: collectedData,
                success: function (data) {
                    dd('Результат запроса на удаленное заселение', data);

                    $this.processCheckBookingInfo(data);

                    if(collectedData.bnovo_custom_template) {
                        window.localStorage.setItem('signer_widget_selected_template', collectedData.bnovo_custom_template);
                    }
                }
            });
        },

        /**
         * Отправка данных по созданию бронирования с данными для запуска процесса создания пасскодов к замкам апартаментов
         *
         * @param {*} form Объект формы
         */
        sendRemoteBookingWithLocksRequestForm: function (form) {
            const requestUrl = apiUrl + apiUrlRoutes.newBookingWithLocks;

            dd('Отправка запроса на удаленное заселение с генерацией пасскодов', requestUrl);

            let $this = this;

            $this.getBookingSchedule($this._opts.bookingId)
                .then((schedule) => {
                    const collectedData = Object.assign(
                        $this.collectBookingData(),
                        $this._opts.bnovoAccount,
                        formDataToJson(form),
                        {apartments: schedule, schedule: this.booking.merged_rooms}
                    );

                    ajaxSigner({
                        method: 'POST',
                        url: requestUrl,
                        data: collectedData,
                        success: function (data) {
                            dd('Результат запроса на удаленное заселение с генерацией пасскодов', data);

                            $this.processCheckBookingInfo(data);

                            if(collectedData.bnovo_custom_template) {
                                window.localStorage.setItem('signer_widget_selected_template', collectedData.bnovo_custom_template);
                            }

                            if(collectedData.bnovo_checkin_instruction_template) {
                                $this.widget.setCacheSelectedCheckinInstruction($this.booking.merged_rooms[0].room_id, collectedData.bnovo_checkin_instruction_template);
                            }
                        }
                    });
                });
        },

        /**
         * Запускает переодическую проверку состояния бронирования.
         *
         * @param {int} interval - промежуток в `ms` между запусками интервала. Если не задано, то по умолчанию значение равно `1000ms`
         */
        updateStateOfRemoteBooking(interval) {
            let collectedData = this.collectBookingData();
            let i = (interval !== undefined ? interval : 1000);
            const $this = this;

            // Обработка случая если проверка состояния была запущена еще раз - вырубаем предыдущий интервал
            if (this.updaterInterval) {
                clearInterval($this.updaterInterval);
                this.updaterInterval = null;
            }

            this.updaterInterval = setInterval(() => {
                const result = $this.checkBookingInfo(collectedData.contract_id);

                if (result) {
                    $this.processCheckBookingInfo(result);
                } else {
                    console.log('Ошибка при обновлении статуса', result);
                    clearInterval(this.updaterInterval);
                }
            }, i);
        },

        sendLinkToGuest() {
            let collectedData = this.collectBookingData();
            const bookingId = collectedData.contract_id;

            const requestUrl = apiUrl + apiUrlRoutes.sendLink + '/' + bookingId;

            ajaxSigner({
                method: 'GET',
                url: requestUrl,
                withCredentials: true,
                success: function (data) {
                    console.log('Запрос на отправку ссылки выполнен', data);
                }
            });

            // Далее ничего делать не нужно, подразумевается что виджет обновится самостоятельно когда бек обновит бронирование
        },

        /**
         * Обновление стейта виджета по сервисам, используемым в полученном бронировании
         */
        setBookingResponseService(signer_booking_response) {
            if( signer_booking_response.is_lock_management ) {
                this.isRemoteBooking = true;
                this.isLockRegistration = true;
            } else {
                this.isRemoteBooking = true;
            }
        },

        /**
         * Обновляет состояние виджета на основании полученного ответа
         * @param {Object} result - JSON объект, полученный с Signer API
         */
        processCheckBookingInfo(result) {
            const newBookingStatuses = result.bookings && JSON.stringify(result.bookings.map(elem => elem.code).slice().sort()) || "";

            // Условие на проверку того, статус был изменен в избежании постоянного обновления окна виджета
            if (this.previousBookingStatuses != newBookingStatuses || this.previousResponseCode != result.code) {

                this.setBookingResponseService(result);

                // TODO: Нужно менять после загрузки экрана виджета
                this.setWidgetScreenHeader();

                const booking_timezone = this.booking.booking.hotel.timezone;

                switch (result.code) {
                    // Код означающий что бронирование никак ранее не регистрировалось в eSigner
                    // и можно начать процесс удаленного заселения
                    case 0:
                        if (this.isBillingServiceAvailable(this.widget.ESIGNER_SERVICES.REMOTE_CONTRACT_SIGNING)) {
                            this.createNewBooking();
                            this.widget.changeButtonStatus(this.widget.REMOTE_BOOKING_NOT_EXISTS);

                            this.registerWelcomeBookingActions();
                        } else {
                            this.widget.showBillingError();
                        }
                        clearInterval(this.updaterInterval);
                        break;
                    case 4020:
                        this.widget.showBillingError();
                        break;
                    case 4021:
                        this.widget.showBillingBalanceError();
                        break;
                    case 501:
                        this.widget.showBookingCreationErrors(result);
                        break;
                    case "new":
                        this.widget.showBookingInfo(result.code, result.bookings, booking_timezone, result.available_services);
                        this.widget.changeButtonStatus(this.widget.REMOTE_BOOKING_CREATED);

                        let $this = this;
                        // Отправка приглашения по СМС
                        $('#btn_send_link_to_guest').on('click', function (e) {
                            e.preventDefault();
                            $this.sendLinkToGuest();
                            return false;
                        });

                        this.registerUpdateGuestAction();
                        this.updateStateOfRemoteBooking();
                        break;
                    case "link_sent":
                        this.widget.showBookingInfo(result.code, result.bookings, booking_timezone, result.available_services);
                        this.widget.changeButtonStatus(this.widget.REMOTE_BOOKING_LINK_SENT);
                        this.updateStateOfRemoteBooking(3000);

                        // this.registerRejectBookingAction();
                        this.registerUpdateGuestAction();
                        break;
                    case "visited":
                    case "waiting_signing_of_other_contracts":
                        this.widget.showBookingInfo(result.code, result.bookings, booking_timezone, result.available_services);
                        this.widget.changeButtonStatus(this.widget.REMOTE_BOOKING_VISITED);

                        //this.registerRejectBookingAction();
                        this.registerUpdateGuestAction();
                        this.registerBookingInfoActions();
                        this.updateStateOfRemoteBooking();
                        break;
                    case "signed":
                        this.widget.showBookingInfo(result.code, result.bookings, booking_timezone, result.available_services);
                        this.widget.changeButtonStatus(this.widget.REMOTE_BOOKING_COMPLETED);

                        this.registerBookingInfoActions();
                        clearInterval(this.updaterInterval);
                        break;
                    case "canceled":
                        this.widget.showBookingIsCanceledBlock();
                        this.widget.changeButtonStatus(this.widget.REMOTE_BOOKING_CANCELED);
                        clearInterval(this.updaterInterval);
                        break;
                    case "expired":
                        this.widget.showBookingIsExpiredBlock();
                        this.widget.changeButtonStatus(this.widget.REMOTE_BOOKING_EXPIRED);
                        this.updateStateOfRemoteBooking(5000);
                        break;
                    default:
                        this.widget.showCriticalError('Получен неизвестный статус удаленного заселения, вероятно у вас старая версия виджета.');
                        clearInterval(this.updaterInterval);
                        break;
                }
            }

            this.previousBookingStatuses = newBookingStatuses;
            this.previousResponseCode = result.code;
        },

        /**
         * Собирает и возвращает список полей необходимых для начала оформления удаленного заселения
         *
         * @returns {Object} bookingData
         */
        collectBookingData() {
            const adults_number = parseInt($('[name="adults"]').val());
            const childrens_number = parseInt($('[name="children"]').val());

            return {
                contract_id: this.bookingNumber,
                bnovo_booking_id: $('[name="booking_id"]').val(),
                bnovo_customer_id: $('[name="customer_id"]').val(),
                bnovo_hotel_id: current_hotel_id,
                bnovo_user_id: current_user_id,
                total_price: $('#booking_initial_total_amount').val(),
                daily_price: $('').val(),
                status: $('.switcher .switcher__item.m-current').attr('data-status-id') || 1,
                checkin_datetime: $('[name="date_from"]').val() + ' ' + $('[name="hour_from"]').val(),
                checkout_datetime: $('[name="date_to"]').val() + ' ' + $('[name="hour_to"]').val(),
                first_name: $('[name="name"]').val(),
                last_name: $('[name="surname"]').val(),
                middle_name: $('[name="middlename"]').val(),
                phone: $('[name="phone"]').val(),
                email: $('[name="email"]').val(),
                number_of_adults: adults_number,
                number_of_guests: adults_number + childrens_number,
                services: window.services_existing_prices || []
            };
        },

        /**
         * Регистрирует события для экрана "Удаленное заселение" метода виджета `showSignerSubmitBlock` без инициализации замков
         */
        registerFirstStepRemoteBookingActions() {
            let $this = this;
            // Отправка формы
            $('#signer_form').on('submit', function (e) {
                e.preventDefault();
                $this.sendRemoteBookingRequestForm($(this));
                return false;
            });
        },

        /**
         * Регистрирует события для экрана "Удаленное заселение" метода виджета `showSignerSubmitBlock`
         * с инициализацией процесса создания пинкодов для удаленного заселения
         */
        registerFirstStepRemoteBookingWithLocksActions() {
            let $this = this;
            // Отправка формы
            $('#signer_form').on('submit', function (e) {
                e.preventDefault();
                $this.sendRemoteBookingWithLocksRequestForm($(this));
                return false;
            });
        },

        registerRejectBookingAction() {
            let $this = this;

            $('#btn_reject_booking')
                .off('click') // Отвязка предыдущих событий клика на кнопке
                .on( 'click', function (e){
                    e.preventDefault();
                    $this.rejectBooking();
                    return false;
                });
        },

        registerWelcomeBookingActions() {
            let $this = this;

            // Кнопка "Удаленное подписание договора"
            $('#action_start_remote_booking')
                .off('click')
                .on( 'click', function (e){
                    e.preventDefault();
                    $this.startRemoteBooking();
                    return false;
                });

            // Кнопка "Удаленное заселение с замками"
            $('#action_start_remote_booking_with_locks')
                .off('click')
                .on( 'click', function (e) {
                    e.preventDefault();
                    $this.startRemoteBookingWithLocks();
                    return false;
                });
        },

        /**
         * Регистрирует события для экрана bookingInfo
         */
        registerBookingInfoActions() {
            const $this = this;

            // Обновление пасскода
            // Событие может быть вызвано из объекта вебсокета SignerSocket при получениии сообщения
            // ttlock_update с типом passcode_update
            // Событие вызывается для конкретного блока пасскода, по его data-pc-id
            $('.s_passcode')
                .off('ws-passcode-update')
                .on('ws-passcode-update', function(e, data) {
                    $this.widget.pcObjects[data.id].updateBlock(data.status, data.code, data.is_algorithmic, data.start_datetime, data.end_datetime);
                });

            $('.s_passcode')
                .on('click', '.s_passcode-shift_hour', function(e) {
                    e.preventDefault();
                    const el = $(this);
                    let pcId = el.attr('data-pc-id');
                    let shiftType = el.attr('data-shift-type');

                    $this.shiftPasscodePeriod(pcId, shiftType);

                    // Класс disabled добавляется на все кнопки изменения периода, т.к. нельзя одновременно менять и начало и конец.
                    $('.s_passcode .s_passcode-shift_hour').addClass('disabled');

                    return false;
                });
        },

        isBillingServiceAvailable(serviceName) {
            return this.widgetState.billing_info.available_services.includes(serviceName);
        },

        /**
         * Метод, инициализирующий приветственный экран с выбором типа УЗ
         */
        createNewBooking() {
            let apartments_ids = [];
            for (let i = 0; i < this.bookingApartments.length; i++) {
                const el = this.bookingApartments[i];
                apartments_ids.push(el.room_id);
            }

            let isRemoteBookingPossible = true;
            let isRemoteBookingPossibleWithLocks = true;
            let serviceLocksStatus = this.widget.SERVICE_LOCKS_STATUS_OK;

            const isLockServiceAvailable = this.isBillingServiceAvailable(this.widget.ESIGNER_SERVICES.REMOTE_LOCK_MANAGEMENT);

            // Предзагрузка данных по замкам доступна только для тех юр. лиц, у которых есть настроенный аккаунт TTLock
            if( this.widgetState.ttlock_account_is_set && isLockServiceAvailable) {
                this.screenPreloading([
                        getSignerApartmentsLocksData(apartments_ids),
                    ], (result) => {
                        let data = result[0];
                        dd("Получены данные по замкам для апартаментов:", data);

                        // Обработка случая, когда сервис TTLock ответил таймаутом или иной ошибкой
                        if(data.status === 'ok') {
                            this.bookingLocks = data.locks;
                            let withoutLocksCount = 0;

                            // Сохраняются замки в апартаменты
                            for (let i = 0; i < this.bookingApartments.length; i++) {
                                const el = this.bookingApartments[i];

                                let apart = data.apartments.find((data_el) => {
                                    return data_el.room_id == el.room_id;
                                });

                                if(apart !== undefined) {
                                    let locks = data.locks.filter((data_lock) => {
                                        return apart.locks.includes("" + data_lock.lockId);
                                    });

                                    el['isSignerExists'] = true;
                                    el['locks'] = locks;
                                } else {
                                    // Если апартаменты не были найдены - в сайнере требуется синхронизация
                                    el['isSignerExists'] = false;
                                    el['locks'] = [];
                                    serviceLocksStatus = this.widget.SERVICE_LOCKS_STATUS_REQUIRED_SYNCH;
                                }

                                // Период(апартаменты) не распределен по номерам
                                // В таком случае кнопка УЗЗ блокируется
                                if(el['room_id'] == 0) {
                                    isRemoteBookingPossibleWithLocks = false;
                                    serviceLocksStatus = this.widget.SERVICE_LOCKS_STATUS_PERIOD_NOT_SET;
                                }

                                if(el['locks'].length == 0) {
                                    ++withoutLocksCount;
                                }
                            }

                            // Если ни у одного периода нет привязанных замков
                            // В таком случае кнопка УЗЗ блокируется
                            if( isRemoteBookingPossibleWithLocks && withoutLocksCount == this.bookingApartments.length ) {
                                isRemoteBookingPossibleWithLocks = false;
                                serviceLocksStatus = this.widget.SERVICE_LOCKS_STATUS_EMPTY_LOCKS;
                            }
                            dd("Замки распределены по апартаментам:", this.bookingApartments);
                        } else if(data.status === "error" && data.code === 'ttlock_timeout') {
                            serviceLocksStatus = this.widget.SERVICE_LOCKS_STATUS_TIMEOUT;
                        } else if(data.status === 'error') {
                            this.widget.showCriticalError('Получена неизвестная ошибка сервиса замков TTLock');

                            return;
                        }

                        this.widget.showSignerWelcomeScreen( this.collectBookingData(), isRemoteBookingPossible, serviceLocksStatus, isLockServiceAvailable );

                        this.registerWelcomeBookingActions();
                    }
                );
            } else {
                this.widget.showSignerWelcomeScreen( this.collectBookingData(), isRemoteBookingPossible, this.widget.SERVICE_LOCKS_STATUS_ACCOUNT_NOT_SET, isLockServiceAvailable );

                this.registerWelcomeBookingActions();
            }
        },

        shiftPasscodePeriod(pcId, shiftType) {
            const $this = this;
            const requestUrl = apiUrl + apiUrlRoutes.lock_management.passcodes + '/' + pcId + '/shift-period';

            ajaxSigner({
                method: 'POST',
                url: requestUrl,
                withCredentials: true,
                data: {
                    edit_value: shiftType
                },
                success: function (data) {
                    dd('Запрос на изменение периода действия пасскода, ответ бекенда:', data);
                    // Пасскод обновится через сокет, но если ошибка - отобразить ее

                    if(data.status == 'error') {
                        switch (data.code) {
                            case 'lock_wo_gateway':
                            case 'gateway_busy':
                            case 'gateway_is_offline':
                            case 'ttlock_timeout':
                                $this.widget.pcObjects[pcId].showError(data.code);
                                break;

                            default:
                                $this.widget.pcObjects[pcId].showError('unexpected');
                                break;
                        }
                    }
                }
            });
        },

        rejectBooking() {
            let collectedData = this.collectBookingData();
            const bookingId = collectedData.contract_id;

            const requestUrl = apiUrl + apiUrlRoutes.rejectBooking + '/' + bookingId + '/reject';

            ajaxSigner({
                method: 'GET',
                url: requestUrl,
                withCredentials: true,
                success: function (data) {
                    dd('Запрос на отмену удаленного заселения, ответ бекенда:', data);
                }
            });

            // Далее ничего делать не нужно, подразумевается что виджет обновится самостоятельно когда бек обновит бронирование
        },

        registerUpdateGuestAction() {
            let $this = this;

            $('.signer_update_guest_show')
                .off('click')
                .on('click', function(e) {
                    const bnovo_customer_id = $(this).data('bnovo-customer-id');
                    $("#signer_guest_phone_" + bnovo_customer_id).hide();
                    $("#signer_update_guest_block_" + bnovo_customer_id).show();
                    e.preventDefault();
                    return false;
                })

            $('.signer_update_guest_cancel')
                .off('click')
                .on('click', function(e) {
                    const bnovo_customer_id = $(this).data('bnovo-customer-id');
                    $("#signer_guest_phone_" + bnovo_customer_id).show();
                    $("#signer_update_guest_block_" + bnovo_customer_id).hide();
                    $("#signer_update_guest_error_" + bnovo_customer_id).html('').hide();
                    const prev_val = $("#signer_update_guest_block_" + bnovo_customer_id + " [name='phone']").data('prev-value')
                    $("#signer_update_guest_block_" + bnovo_customer_id + " [name='phone']").val(prev_val);
                    e.preventDefault();
                    return false;
                })

            $('.signer_update_guest_submit')
                .off('click') // Отвязка предыдущих событий клика на кнопке
                .on('click', function (e){
                    const bnovo_customer_id = $(this).data('bnovo-customer-id');
                    e.preventDefault();
                    $this.updateGuest(bnovo_customer_id);
                    return false;
                });
        },

        updateGuest(bnovo_customer_id) {
            $("#signer_update_guest_error_" + bnovo_customer_id).html('').hide();

            const collectedData = this.collectBookingData();
            const bookingId = collectedData.contract_id;
            const requestUrl = apiUrl + apiUrlRoutes.updateGuest + '/' + bookingId + '/guests/' + bnovo_customer_id;

            const new_phone = $("#signer_update_guest_block_" + bnovo_customer_id + " [name='phone']").val().replaceAll(/[^\d^\+]+/g, '');

            const phone_valid = new_phone.match(SIGNER_PHONE_REGEX);
            const same_phone_numbers = $(".signer_booking [name='phone']").filter((i, elem) => {
                return $(elem).val().replaceAll(/[^\d^\+]+/g, '') == new_phone;
            })

            if (!phone_valid) {
                $("#signer_update_guest_error_" + bnovo_customer_id).html('Пожалуйста, укажите российский номер телефона (с префиксом +7 или 8)').show();
            } else if (same_phone_numbers.length > 1) {
                $("#signer_update_guest_error_" + bnovo_customer_id).html('Совпадает с номером другого гостя').show();
            } else {
                $("#signer_update_guest_error_" + bnovo_customer_id).html('');

                const guestData = {
                    phone: new_phone
                };

                dd('Запрос на обновление сведений о госте', {bookingId: bookingId, bnovo_customer_id: bnovo_customer_id, guestData: guestData});

                $("#signer_booking_details_" + bnovo_customer_id).html('<h4>Передача информации о госте в систему eSigner...</h4>')

                this.previousBookingStatuses = null;
                this.previousResponseCode = null;

                ajaxSigner({
                    method: 'PUT',
                    url: requestUrl,
                    withCredentials: true,
                    data: guestData,
                    success: function (data) {
                        dd('Запрос на обновление сведений о госте, ответ бекенда:', data);
                    }
                });
            }
        },

        /**
         * Метод, запускающий ветвь оформления удаленного заселения без оформления замков
         */
        startRemoteBooking() {
            this.isRemoteBooking = true;
            this.isLockRegistration = false;
            const isSmsLinkAvailable = this.isBillingServiceAvailable(this.widget.ESIGNER_SERVICES.SEND_SMS_LINK_TO_CLIENT);

            this.setWidgetScreenHeader();
            this.widget.showSignerSubmitBlock( this.collectBookingData(), getCustomTemplatesList(), this.bookingApartments, this.isLockRegistration, isSmsLinkAvailable );
            this.widget.populateMultipleContractsContainer( this.booking, this.isMultiGuestSigningAvailable(this.booking) );

            this.registerFirstStepRemoteBookingActions();
        },


        /**
         * Метод, запускающий ветвь оформления удаленного заселения с запуском процесса
         * формирования пинкодов для замков, привязанных к апартаментам бронирования
         */
        startRemoteBookingWithLocks() {
            this.isRemoteBooking = true;
            this.isLockRegistration = true;
            const isSmsLinkAvailable = this.isBillingServiceAvailable(this.widget.ESIGNER_SERVICES.SEND_SMS_LINK_TO_CLIENT);

            this.setWidgetScreenHeader();
            this.widget.showSignerSubmitBlock( this.collectBookingData(), getCustomTemplatesList(), this.bookingApartments, this.isLockRegistration, isSmsLinkAvailable );
            this.widget.populateMultipleContractsContainer( this.booking, this.isMultiGuestSigningAvailable(this.booking) );

            this.registerFirstStepRemoteBookingWithLocksActions();
        },

        isMultiGuestSigningAvailable(booking) {
            const isServiceAvailable = this.isBillingServiceAvailable(this.widget.ESIGNER_SERVICES.MULTI_GUEST_SIGNING);
            const currentBalance = this.widgetState.billing_info.balance;
            const possibleSignings = booking.booking.extra.adults;
            const enoughBalance = (this.widgetState.billing_info.tarif_type_id == 'trial') || (currentBalance >= possibleSignings);
            return isServiceAvailable && enoughBalance;
        },

        /**
         * Загрузка данных по бронированию. Функция глобальная от бново.
         * @returns {Promise<Object>}
         */
        getBookingSchedule(bookingId) {
            const $this = this;
            return new Promise(function(resolve) {
                // loadBookingData - функция bnovo
                loadBookingData(bookingId, function() {
                    // Формирование структуры апартаментов для передачи на бек
                    let schedule = [];
                    Object.keys(booking_data).forEach((date) => {
                        const el = booking_data[date];

                        schedule.push({
                            apartments_id: el.room_id,
                            apartments_real_type_id: el.real_room_type_id,
                            date: date
                        });
                    });
                    resolve(schedule);
                });
            });
        },

        /**
         * Проводит проверку на возможность запуска виджета.
         * Если все проверки прошли - возвращает true, иначе - код ошибки.
         */
        startupChecks() {
            const data = this.collectBookingData();

            // Если с бекенда пришло значение credentials_valid = false
            // То виджет должен приостановить работу
            if (this.state && !this.state.credentials_valid) {
                return STARTUP_ERROR_CREDENTIALS_INVALID;
            }
            // Если с бекенда пришла ошибка о том, что бекенд не может работать с текущим отелем, т.к. логин пароль в системе Signer
            // для работы с АПИ bnovo корректны, но не имеют доступ к текущему отелю.
            if (this.state && !this.state.enough_permissions) {
                return STARTUP_ERROR_ACCOUNT_WO_PERMISSIONS;
            }
            // Телефон должен быть заполнен в форме гостя
            if (data && !data.phone) {
                return STARTUP_ERROR_PHONE_NOT_FOUND;
            } else {
                const phone = data.phone.replaceAll(/[^\d^\+]+/g, '');
                const phoneCheck = phone.match(SIGNER_PHONE_REGEX);

                // Ограничение на формат номера телефона
                if (!phoneCheck) {
                    return STARTUP_ERROR_PHONE_RESTRICTED;
                }
            }

            return true;
        },

        async attachWidget() {
            // При вызове данного метода по умолчанию показывается экран "Получение данных..."
            // который сменится при вызове `updateStateOfRemoteBooking` или при получении ошибки
            this.widget.attachWidget();

            const widgetLoadingChain = Promise.all([
                getBnovoAccountData(),
                getWidgetState(),
                getBookingData(this._opts.bookingId)
            ]);

            widgetLoadingChain.then((result) => {
                dd("Данные инициализации:", result);

                return this.init(result);
            }).catch((error) => {
                dd("Ошибка инициализации виджета:", error);

                this.widget.showCriticalError("Ошибка при получении данных для работы виджета");
            });
        },

        async init(initData) {
            const bnovoAccount = initData[0];
            dd("Получена информация по аккаунту оператора:", bnovoAccount);
            this.bnovoAccount = bnovoAccount;

            const state = initData[1];
            dd("Загружен стейт виджета:", state);
            this.widgetState = state;

            if (state.billing_info) {
                this.widget.showBillingInfo(state.billing_info);
            } else {
                this.widget.showCriticalError("Ошибка при получении данных для работы виджета");
            }

            const booking = await initData[2].json();
            dd("Загружены данные по бронированию:", booking);
            this.booking = booking;
            this.bookingNumber = booking.booking.number;
            this.bookingApartments = booking.merged_rooms;

            // Обязательные проверки перед запуском виджета
            switch(this.startupChecks()) {
                case STARTUP_ERROR_ACCOUNT_WO_PERMISSIONS:
                    this.widget.showCriticalError(" \
                        Указанные в системе Signer логин-пароль от аккаунта в Bnovo не имеют доступа к данному отелю \
                        <br>Связаться с поддержкой Signer <a href='email:tech@euler.solutions'>tech@euler.solutions</a> \
                    ");
                    break;
                case STARTUP_ERROR_CREDENTIALS_INVALID:
                    this.widget.showCriticalError(" \
                        Signer не может подключиться к API Bnovo с указанными логином-паролем в системе Signer. \
                        <br>Связаться с поддержкой Signer <a href='email:tech@euler.solutions'>tech@euler.solutions</a> \
                    ");
                    break;
                case STARTUP_ERROR_PHONE_NOT_FOUND:
                    this.widget.showCriticalError("Не указан номер телефона у гостя");
                    break;
                case STARTUP_ERROR_PHONE_RESTRICTED:
                    this.widget.showCriticalError("Номер телефона гостя не принадлежит Российским операторам сотовой связи");
                    break;
                default:
                    this.updateStateOfRemoteBooking();
            }
        },

        /**
         * Метод включающий оверфлоу загрузки пока ожидается выполнения списка промисов
         *
         * Вторым аргументом принимает функцию-колбек, куда передается результат выполнения промисов.
         *
         * @param {[Promise]} tasks
         * @param {Function} onLoad
         */
        screenPreloading(tasks, onLoad) {
            const $this = this;
            $this.widget.toggleLoadingOverflow(true);

            Promise.all(tasks)
                .then(function(result) {
                    $this.widget.toggleLoadingOverflow(false);

                    onLoad(result);
                })
                .catch(function(error) {
                    $this.widget.toggleLoadingOverflow(false);

                    dd("Error on screen preloading:", error);
                });
        },

        setWidgetScreenHeader() {
            if( this.isLockRegistration ) {
                this.widget.setModalHeader( this.bookingNumber, this.widget.WIDGET_SERVICE_LOCKS );
            } else {
                this.widget.setModalHeader( this.bookingNumber );
            }
        },
    };
}

function SignerWidget(parentSelector, opts) {
    const parentBlock = $(parentSelector);

    // Кнопка с блоком, отображающая модалку удаленного подписания
    const signerWrapper = $('<div/>').addClass('switcher m-margin');

    // Сборка модалки
    const modalBaseWrapper = buildModal();

    // Сама кнопка
    const button = $('<button/>').addClass('switcher__item');
    button.html('Удаленное заселение');

    // Событие клика по кнопке
    button.on('click', function (e) {
        modalBaseWrapper.signerPopup.arcticmodal();
    });

    // Контейнер для биллинговой информации, которая показывается справа от кнопки виджета
    const billingInfoWrapper = $('<div/>').addClass('switcher m-margin').css({
        fontSize: 'var(--px-rem-14)'
    });

    // Сборка всего в один блок
    signerWrapper.append(button);

    signerWrapper.append(modalBaseWrapper);

    function buildModal() {
        // Основная обертка, скрывающая модалку
        const modalBaseWrapper = $('<div/>').css('display', 'none');

        // Сама модалка
        const modalBase = $('<div/>').addClass('modal').attr('id', 'signer_modal');
        modalBaseWrapper.append(modalBase);
        modalBaseWrapper.signerPopup = modalBase;

        // Оверфлоу загрузки
        const overflow = $('<div class="s-loading-overflow"><div class="preloader"></div></div>');
        modalBase.append(overflow);
        modalBaseWrapper.loadingOverflow = overflow;

        // Контент
        modalBase.append($(' \
            <div class="grid m-padding m-cellpadding"> \
                <div class="grid__cell"> \
                    <form id="signer_form" class="form"> \
                        <div class="form__header"> \
                            <h3>Удаленное заселение</h3> \
                        </div> \
                        <div class="modal-content"> \
                            <p>Получение информации по удаленному заселению...</p> \
                        </div> \
                    </form> \
                </div> \
            </div> \
            <style> \
                .modal-content p {font-size: 1rem;} \
                .s-loading-overflow {display: none; position: absolute; width: 100%; height: 100%; top: 0; left: 0; z-index: 10; background-color: rgba(128, 128, 128, 0.2);} \
                .s-loading-overflow .preloader {position: absolute; width: 64px; height: 64px; top: calc(50% - 32px); left: calc(50% - 32px);} \
                .apartments-list {font-size: 12px;} \
                .modal-content .status {margin-left: 0;} \
                .modal-content p:not(:last-child) {margin-bottom: 0.6rem;} \
                .s-passcode_availability {display: block;} \
                .s_passcode-error:not(:empty) {color: red; display: block;} \
                .s_passcode-shift_hour.disabled {position: relative;} \
                .s_passcode-shift_hour.disabled:after {content: \'\'; cursor: default; pointer-events: none; z-index: 1; display: block; position: absolute; width: 100%; height: 100%; left: 0; top: 0; background: url(/public/img/design/preloader.png) center center no-repeat; animation: spin 1s infinite linear;} \
            </style> \
        '));

        // Кнопка закрытия модалки
        const modalCloseButton = $('<div/>').addClass('popup-btn-close arcticmodal-close').on('click', function (e) {
            modalBase.arcticmodal('close');
        })
        modalBase.append(modalCloseButton);

        return modalBaseWrapper;
    }

    // Формирование ссылок на блок заголовка и контента
    const contentBlock = modalBaseWrapper.find('#signer_modal .modal-content');
    const headerBlock = modalBaseWrapper.find('#signer_modal .form__header');


    const selectedCheckinInstructionCacheKey = 'signer_widget_selected_checkin_template';


    return {
        // Статусы бронирования, на осно
        REMOTE_BOOKING_NOT_EXISTS: 101,
        REMOTE_BOOKING_CREATED: 102,
        REMOTE_BOOKING_COMPLETED: 103,
        REMOTE_BOOKING_EXPIRED: 104,
        REMOTE_BOOKING_VISITED: 105,
        REMOTE_BOOKING_LINK_SENT: 106,
        REMOTE_BOOKING_CANCELED: 107,

        // Сервис(ы) с которым работает виджет
        WIDGET_SERVICE_LOCKS: 'lock_management',

        // Статус сервиса замков для виджета
        SERVICE_LOCKS_STATUS_OK: 'ok',
        SERVICE_LOCKS_STATUS_REQUIRED_SYNCH: 'required_synchronization',
        SERVICE_LOCKS_STATUS_EMPTY_LOCKS: 'locks_not_exists',
        SERVICE_LOCKS_STATUS_PERIOD_NOT_SET: 'period_not_set',
        SERVICE_LOCKS_STATUS_ACCOUNT_NOT_SET: 'account_not_set',
        SERVICE_LOCKS_STATUS_TIMEOUT: 'ttlock_timeout',

        ESIGNER_SERVICES: {
            REMOTE_CONTRACT_SIGNING: 'remote_contract_signing',
            REMOTE_LOCK_MANAGEMENT: 'remote_lock_management',
            SEND_SMS_LINK_TO_CLIENT: 'send_sms_link_to_client',
            MULTI_GUEST_SIGNING: 'multi_guest_signing',
            MOBILE_ID: 'mobile_id',
            PASSPORT_OCR: 'passport_ocr'
        },

        initialButton: button,
        modalBlock: modalBaseWrapper,
        headerBlock: headerBlock,
        contentBlock: contentBlock,
        loadingOverflow: modalBaseWrapper.loadingOverflow,

        pcObjects: {},

        attachWidget: function () {
            parentBlock.append(signerWrapper);
            parentBlock.append(billingInfoWrapper);
        },
        detachWidget: function () {
            this.initialButton.remove();
            this.modalBase.remove();
        },

        toggleLoadingOverflow(flag) {
            if (flag) {
                this.loadingOverflow.show();
            } else {
                this.loadingOverflow.hide();
            }
        },

        /**
         * Кеширует выбранный шаблон инструкции по заселению для переданных апартаментов
         * @param {integer} roomId
         * @param {String} value
         */
        setCacheSelectedCheckinInstruction(roomId, value) {
            let key = `${selectedCheckinInstructionCacheKey}_${roomId}`;

            window.localStorage.setItem(key, value);
        },

        /**
         * Возвращает закешированное значение названия инструкции по заселению для переданных апартаментов
         * @param {integer} roomId
         * @returns {?String} - Название шаблона для инструкции
         */
        getCacheSelectedCheckinInstruction(roomId) {
            let key = `${selectedCheckinInstructionCacheKey}_${roomId}`;

            return window.localStorage.getItem(key);
        },

        /**
         * Устанавливает заголовок модального окна по умолчанию
         *
         * Выставляет заголовок модального окна в зависимости от того с каким сервисом виджет работает
         *
         * @param {*} service
         */
        setModalHeader( contract_number, service ) {
            switch (service) {
                case this.WIDGET_SERVICE_LOCKS:
                    this.headerBlock.html('Удалённое заселение для бронирования ' + contract_number);
                    break;

                default:
                    this.headerBlock.html('Удалённое подписание документов бронирования ' + contract_number);
                    break;
            }
        },

        setCustomModalHeader( block ) {
            this.headerBlock.html( block );
        },

        /**
         * Метод отображения приветственного экрана
         *
         * @param {*} registerData
         * @param {boolean} isRemoteBookingPossible
         * @param {String} serviceLocksStatus - статус сервиса замков виджета для регуляции показа блоков.
         * @param {boolean} isLockServiceAvailable - флаг доступности в биллинге услуги заселения с замками
         *  Статусы хранятся в полях объекта `SignerWidget.SERVICE_LOCKS_STATUS_*`
         */
        showSignerWelcomeScreen( registerData, isRemoteBookingPossible, serviceLocksStatus, isLockServiceAvailable ) {
            let serviceLocksErrorBlock = '';
            switch (serviceLocksStatus) {
                case this.SERVICE_LOCKS_STATUS_ACCOUNT_NOT_SET:
                case this.SERVICE_LOCKS_STATUS_TIMEOUT:
                case this.SERVICE_LOCKS_STATUS_REQUIRED_SYNCH:
                case this.SERVICE_LOCKS_STATUS_EMPTY_LOCKS:
                case this.SERVICE_LOCKS_STATUS_PERIOD_NOT_SET:
                    serviceLocksErrorBlock = `
                        <div class="form__item status status m-text-orange">
                            <p>${$tt('locks_status.' + serviceLocksStatus)}</p>
                        </div>
                    `;
                    break;
                default:
                    break;
            }
            serviceLocksErrorBlock = isLockServiceAvailable ? serviceLocksErrorBlock : '';
            const locksButton =  isLockServiceAvailable ? '<button class="form__button button m-s"' + (serviceLocksStatus === 'ok' ? ' id="action_start_remote_booking_with_locks"' : ' disabled') + '>Удалённое заселение с замками</button>' : '';

            this.contentBlock.html($(' \
                <div class="form__item"> \
                    <p>ФИО: ' + registerData.last_name + ' ' + registerData.first_name + ' ' + registerData.middle_name + '</p> \
                    <p>Номер телефона: ' + registerData.phone + '</p> \
                    <p>Количество гостей: ' + registerData.number_of_guests + '</p> \
                </div> \
                ' + serviceLocksErrorBlock + ' \
                <div class="form__item"> \
                    <div class="form__cell"> \
                        <button class="form__button button m-s"' + (isRemoteBookingPossible ? ' id="action_start_remote_booking"' : ' disabled') + '">Удаленное подписание документов</button> \
                        ' + locksButton + ' \
                    </div> \
                </div> \
            '));
        },

        /**
         * Метод отображения экрана первого этапа удаленного заселения
         *
         * @param {*} registerData
         * @param {*} templatesList
         * @param {boolean} isLocksRegistration - Флаг сообщающий о том, что необходимо показать информацию по оформлению замков
         * @param {boolean} isSmsLinkAvailable - Флаг доступности отправки СМС клиенту (в соотв с биллингом)
         */
        showSignerSubmitBlock: function ( registerData, templatesList, apartments, isLocksRegistration, isSmsLinkAvailable ) {
            // Выбор шаблона договора
            let selectTemplateBlock = null;
            let selectCheckInInstructionBlock = null;
            let roomId = apartments[0].room_id;

            if( Array.isArray( templatesList ) && templatesList.length > 0 ) {
                selectTemplateBlock = $('<select class="form__select select m-transparent" name="bnovo_custom_template"/>');
                templatesList.forEach( ( el ) => {
                    let selected = '';
                    if( window.localStorage.getItem('signer_widget_selected_template') === el ) {
                        selected = ' selected="selected"';
                    }
                    selectTemplateBlock.append( $('<option value="' + el + '"' + selected + '/>').text( el ) );
                });

                selectCheckInInstructionBlock = $('<select class="form__select select m-transparent" name="bnovo_checkin_instruction_template"/>');
                let cachedCheckinInstruction = this.getCacheSelectedCheckinInstruction(roomId);
                templatesList.forEach( ( el ) => {
                    let selected = '';
                    // Ранее выбранный шаблон для инструкции по заселению кешируется с ключем номера комнаты
                    if( cachedCheckinInstruction === el ) {
                        selected = ' selected="selected"';
                    }
                    selectCheckInInstructionBlock.append( $('<option value="' + el + '"' + selected + '/>').text( el ) );
                });
            }

            let canSentBookingRequest = true;

            let apartmentsBlock = $('<div class="apartments-list"></div>');
            if(isLocksRegistration) {
                apartmentsBlock.prepend($('<p><b>Номера, для которых будут сгенерированы коды доступов:</b></p>'));

                for (let i = 0; i < apartments.length; i++) {
                    const el = apartments[i];

                    canSentBookingRequest = canSentBookingRequest && el.room_id != "0" && el.isSignerExists;

                    const locksListBlock = $('<div class="apartments-locks"/>');
                    for (let j = 0; j < el.locks.length; j++) {
                        const lockEl = el.locks[j];
                        locksListBlock.append($('<div class="apartments-locks_element">' + lockEl["lockAlias"] + '</div>'));
                    }

                    const apartBlockEl = $(' \
                        <div class="apartments-list_element"> \
                            <span class="apartments__name"><b>- ' + el.room_name + ' / ' + el.room_type_name + '</b></span>' + ' \
                            <br> C: ' + el.dfrom + ' По: ' + el.dto + ' \
                            ' + (el.room_id == "0" ? '<br><span class="status m-text-red">Данный период не распределен по номерам!</span>' : '') + ' \
                            ' + (el.room_id != "0" && !el.isSignerExists ? '<br><span class="status m-text-red">Апартаменты не синхронизированы с системой Signer!</span>' : '') + ' \
                            ' + (el.isSignerExists && el.locks.length == 0 ? '<br><span class="status m-text-yellow">Нет привязанных замков</span>' : '') + ' \
                            ' + (el.isSignerExists ? locksListBlock[0].outerHTML : '') + ' \
                        </div><br> \
                    ');

                    apartmentsBlock.append(apartBlockEl);
                }
            }

            let templatesSelectBlock = '';
            if( selectTemplateBlock ) {
                templatesSelectBlock = selectTemplateBlock[0].outerHTML;
            } else {
                templatesSelectBlock = '<p class="g-alert">Отсутствует шаблон для выбора</p><p>Пожалуйста, загрузите пользовательский шаблон в настройках аккаунта.</p>';
            }

            let instructionSelectBlock = '';
            if( isLocksRegistration ) {
                if( selectCheckInInstructionBlock ) {
                    instructionSelectBlock = ' \
                    <label class="form__radio-label fwb" style="padding-left: 0px;"> \
                        Выберите шаблон документа инструкции по заселению \
                    </label> \
                    ' + selectCheckInInstructionBlock[0].outerHTML;
                } else {
                    instructionSelectBlock = '<p class="g-alert">Отсутствует шаблон для выбора инструкции</p><p>Пожалуйста, загрузите пользовательский шаблон в настройках аккаунта.</p>';
                }
            }

            let multipleContractsContainer = '';
            if (selectTemplateBlock) {
                multipleContractsContainer = '\
                <input type="hidden" id="signer_contract_strategy" name="signer_contract_strategy" value="booking_customer_only"> \
                <div id="signer_multiple_contracts_container"><small>получение информации о других гостях...</small></div> \
                ';
            };

            const sendLinkCheckbox = isSmsLinkAvailable ? '\
            <div class="form__item"> \
                <label class="form__radio-label fwb"> \
                    <input name="send_link_to_guest" class="form__checkbox" type="checkbox" checked="checked"> \
                    Отправить ссылку автоматически \
                </label> \
            </div> \
            ' : '';

            this.contentBlock.html($(' \
                <div class="form__item"> \
                    <p>ФИО: ' + registerData.last_name + ' ' + registerData.first_name + ' ' + registerData.middle_name + '</p> \
                    <p>Номер телефона: ' + registerData.phone + '</p> \
                    <p>Количество взрослых гостей: ' + registerData.number_of_adults + '</p> \
                </div> \
                <div class="form__item"> \
                    <label class="form__radio-label fwb" style="padding-left: 0px;"> \
                        Выберите шаблон документа для подписания \
                    </label> \
                    ' + templatesSelectBlock + ' \
                    ' + instructionSelectBlock + ' \
                     \
                </div> \
                ' + (isLocksRegistration ? apartmentsBlock[0].outerHTML : '') + ' \
                ' + sendLinkCheckbox + ' \
                <h4>Заселить удаленно с подписанием договора</h4> \
                <div class="form__item"> \
                    <div class="form__cell"> \
                        <button class="form__button button m-s" type="submit"'  + (selectTemplateBlock || canSentBookingRequest ? '' : ' disabled') + ' onClick="$(\'#signer_contract_strategy\').val(\'booking_customer_only\')">Только заказчиком</button> \
                    </div> \
                </div>' + multipleContractsContainer));
        },

        /**
         * Рендер блока для множественных заселений
         *
         * @param {*} booking
         * @param {boolean} isServiceAvailable - флаг доступности услуги множественных заселений в соотв с биллингом
         */
        populateMultipleContractsContainer: function(booking, isServiceAvailable) {
            if (booking.booking) {
                const total_customers = booking.booking.extra.adults;
                if (total_customers > 1 && isServiceAvailable) {
                    const primary_customer_id = booking.booking.customer_id;
                    const all_adult_customers = booking.booking.customers.filter((customer) => {
                        return !(customer.extra && customer.extra.guest_type == 2);
                    });

                    const other_adult_customers_with_phones = all_adult_customers.filter((customer) => {
                        return customer.id != primary_customer_id
                            && customer.phone.length > 0
                            && customer.phone.replaceAll(/[^\d]+/g, '').match(SIGNER_PHONE_REGEX)
                    });
                    const all_adult_customers_have_phones = other_adult_customers_with_phones.length == total_customers - 1;

                    const phone_numbers = all_adult_customers.filter((customer) => {
                        return customer.phone.length > 0;
                    }).map((customer) => customer.phone.replaceAll(/[^\d]+/g, '').substr(-10));
                    const unique_phone_numbers = new Set(phone_numbers);
                    const all_adult_customer_phones_are_unique = unique_phone_numbers.size == total_customers;

                    if (all_adult_customers_have_phones && all_adult_customer_phones_are_unique) {
                        const inputs = other_adult_customers_with_phones.reduce(function(acc, customer) {
                            return acc + ` \
                                <input type="hidden" name="signer_extra_customers[${customer.phone}][bnovo_customer_id]" value="${customer.id}"> \
                                <input type="hidden" name="signer_extra_customers[${customer.phone}][phone]" value="${customer.phone}"> \
                                <input type="hidden" name="signer_extra_customers[${customer.phone}][email]" value="${customer.email}"> \
                                <input type="hidden" name="signer_extra_customers[${customer.phone}][name]" value="${customer.name}"> \
                                <input type="hidden" name="signer_extra_customers[${customer.phone}][surname]" value="${customer.surname}"> \
                            `
                        }, '');
                        $("#signer_multiple_contracts_container").html($(inputs + `\
                            <div class="form__item"> \
                                <div class="form__cell"> \
                                    <button class="form__button button m-s" type="submit" onClick="$(\'#signer_contract_strategy\').val(\'separate_contract_for_each_guest\')">Каждым гостем (${total_customers})</button> \
                                </div> \
                            </div> \
                        `));
                    } else {
                        $("#signer_multiple_contracts_container").html($('\
                            <div class="bnovo-card"> \
                                <p> \
                                    <i class="i-warning"></i> \
                                    Если требуется подписание договора каждым<br/> \
                                    гостем, то необходимо добавить в бронь <br/> \
                                    всех гостей с указанием их номеров телефонов.<br/> \
                                    Эти номера телефонов должны быть уникальны. \
                                </p> \
                            </div> \
                        '));
                    }
                } else {
                    $("#signer_multiple_contracts_container").html($(''));
                }
            }
        },

        /**
         * Отображает блок со списком договоров гостей в данном бронировании.
         *
         * Для каждого гостя показывается статус его договора, заполненные данные и загруженные файлы.
         *
         * Если бронирование только что создано и ссылки не были отправлены автоматически показывается соотв. кнопка.
         *
         * @param {String} code Статус основного договора в бронировании (т.е. заявки заказчика)
         *
         * @param {Array} bookings Массив объектов с данными по бронированиям гостей с полями:
         *      - `phone` - номер телефона,
         *      - `form_url` - ссылка на бронирование,
         *      - `first_name` - имя гостя,
         *      - `last_name` - фамилия гостя
         *
         * @param {Array} availableServicesInBooking - массив услуг биллинга, доступных на момент создания брони
         */
        showBookingInfo: function (code, bookings, booking_timezone, availableServicesInBooking) {
            const $this = this;
            const canSendSmsLinkToClient = availableServicesInBooking.includes($this.ESIGNER_SERVICES.SEND_SMS_LINK_TO_CLIENT);

            let sendBlock = '';
            if (code == "new" && canSendSmsLinkToClient) {
                sendBlock = ' \
                <div class="form__item"> \
                    <div class="form__cell"> \
                        <a href="#" class="form__button button m-s" id="btn_send_link_to_guest">Отправить приглашение</a> \
                    </div> \
                </div>';
            }

            this.contentBlock.html($(' \
                <div class="form__item signer_bookings"> \
                    ' + bookings.reduce(function(acc, bookingInfo) {
                        if (bookingInfo.code && bookingInfo.form_url && bookingInfo.phone) {
                            const isSigned = bookingInfo.code === 'signed' || bookingInfo.code == 'waiting_signing_of_other_contracts';
                            const cid = bookingInfo.bnovo_customer_id;

                            let signedBlock = '';
                            let updateGuestButton = '';
                            let updateGuestForm = '';

                            let passcodes_list = $('<section/>');
                            if (isSigned) {
                                const sign_date = new Date(bookingInfo.sign_datetime).toLocaleDateString([], { hour: '2-digit', minute: '2-digit', second: '2-digit' });

                                const signed_files_list = bookingInfo.signed_files.reduce(function(acc, signedFile) {
                                    return acc + `<p><a target="_blank" href="${signedFile.path}">${signedFile.title}</a></p>`;
                                }, '');

                                if (bookingInfo.is_lock_management) {
                                    // Группировка пасскодов по апартаментам для поблочного отображения
                                    // bookingInfo.passcodes -  массив пасскодов, пришедший с бекенда
                                    let grouped_passcodes = {};
                                    if(bookingInfo.passcodes) for (let i = 0; i < bookingInfo.passcodes.length; i++) {
                                        const el = bookingInfo.passcodes[i];
                                        var room_id = `${el.room_id}_${el.period_index}`;

                                        // Если массив с пасскодами лдя апартаментов уже есть в объекте группировки, то добавляем пасскод к апартаментам
                                        // Как один из кодов апартаментов
                                        if(grouped_passcodes[room_id]) grouped_passcodes[room_id].push(el);
                                        else grouped_passcodes[room_id] = [el];
                                    }

                                    // Обход расписания апартаментов, сохраненного в бронировании Signer и использованного
                                    // для создания пасскодов, только на основе данного списка выводятся пасскоды
                                    // т.к. после оформления УЗЗ апартаменты могут измениться
                                    // bookingInfo.initial_apartments является объектом псевдомассивом - индексы сохранены как ключи.
                                    var keys = Object.keys(bookingInfo.initial_apartments);
                                    if(bookingInfo.initial_apartments) {
                                        if(keys) for (let i = 0; i < keys.length; i++) {
                                            const el = bookingInfo.initial_apartments[keys[i]];
                                            const passcodes = grouped_passcodes[`${el.room_id}_${i}`];

                                            if(passcodes) for (let j = 0; j < passcodes.length; j++) {
                                                const pc = passcodes[j];

                                                const passcode = new PassCodeBlock(
                                                    pc.id,
                                                    el.room_name + ' / ' + el.room_type_name + ' (' + pc.lock_name + ')',
                                                    pc.status_id,
                                                    pc.passcode,
                                                    pc.is_algorithmic,
                                                    pc.start_datetime,
                                                    pc.end_datetime,
                                                    booking_timezone
                                                );
                                                $this.pcObjects[pc.id] = passcode;

                                                passcodes_list.append( passcode.getElement() );
                                            }
                                        }
                                    }
                                }

                                const failedAutochecks = bookingInfo.failed_autochecks || [];
                                const failedAutochecksBlock = failedAutochecks.length > 0 ? failedAutochecks.reduce((acc, message) => acc + `<p>${message}</p>`, '<br/><h4>Проблемы при проверке паспортных данных:</h4>') : '';

                                signedBlock = ` \
                                    <p>Серия и номер паспорта: ${bookingInfo.passport_series} ${bookingInfo.passport_number}</p> \
                                    <p>Дата выдачи: ${bookingInfo.passport_date}</p> \
                                    <p>Кем выдан: ${bookingInfo.passport_issued_by}</p> \
                                    <br/> \
                                    <h4>Информация по подписанию:</h4> \
                                    <p>Дата подписания: ${sign_date}</p>
                                    <br/> \
                                    <h4>Подписанные документы:</h4> \
                                    <div>${signed_files_list}</div> \
                                    ${failedAutochecksBlock} \
                                    <br/> \
                                    ` + (bookingInfo.is_lock_management ? `<h4>Статус генерации кодов доступов для апартаментов</h4>` + passcodes_list[0].outerHTML : ``) + ` \
                                    <br/>`;
                            }

                            if (!isSigned) {
                                updateGuestButton = `&nbsp;<a href="#" class="i-edit signer_update_guest_show" data-bnovo-customer-id="${cid}>изменить</a>`;
                                updateGuestForm = `<div style="display:none;" id="signer_update_guest_block_${cid}"> \
                                    <input type="tel" class="form__input input ml10" name="phone" value="${bookingInfo.phone}" data-prev-value="${bookingInfo.phone}">\
                                    <button class="signer_update_guest_submit form__button button m-s m-primary" data-bnovo-customer-id="${cid}">Сохранить</button> \
                                    <button class="signer_update_guest_cancel form__button button m-s m-secondary" data-bnovo-customer-id="${cid}">Отменить</button> \
                                </div><small class="alert m-red" style="display: none;" id="signer_update_guest_error_${cid}"></small>`;
                            }

                            const bookingLink = isSigned ? '' : `<p>Ссылка: ${bookingInfo.form_url}</p><br/>`;
                            const bookingStatus = `<p><b>${t_ru['status.' + bookingInfo.code]}</b></p><br/>`;
                            const bookingHeader = `${bookingInfo.first_name} ${bookingInfo.last_name} ${(bookingInfo.middle_name ? bookingInfo.middle_name : '')}<br/>`;

                            let bookingPhone = `<span>тел.: <b>${bookingInfo.phone}${updateGuestButton}</b></span>`;
                            if (!isSigned) {
                                bookingPhone = `<span id="signer_guest_phone_${cid}">тел.: <a class="signer_update_guest_show" data-bnovo-customer-id="${cid}" ><b class="g-dotted">${ bookingInfo.phone}</b>&nbsp;<i class="i-edit"></i></a></span>`;
                            }

                            return acc
                                + '<div id="signer_booking_' + cid + '" class="signer_booking bnovo-card">'
                                    + '<div class="m-s"><h4>'
                                        + bookingHeader
                                        + bookingPhone
                                        + updateGuestForm
                                    + '</h4></div>'

                                    + '<div id="signer_booking_details_' + cid + '">'
                                        + bookingStatus
                                        + bookingLink
                                        + signedBlock
                                    + '</div>'
                                + '</div>';
                        } else {
                            return acc + '<p>Ошибка при формировании ссылки</p><p>&nbsp;</p>'
                        }
                    }, '') + '\
            </div>' + sendBlock));
        },

        showBookingIsCanceledBlock: function () {
            this.contentBlock.html(' \
                <h4>Удаленное заселение для данного бронирования было отменено</h4> \
            ');
        },

        showBookingIsExpiredBlock: function () {
            this.contentBlock.html(' \
                <h4>Истек срок удаленного заселения</h4> \
            ');
        },

        showCriticalError: function (error) {
            this.contentBlock.html($(' \
                <h4>Ошибка в работе виджета:</h4> \
                <p class="g-alert">' + error + '</p> \
            '));

            button.addClass("i-warning2 red");
        },

        showBookingCreationErrors: function (response) {
            let text = Object.keys(response.errors).map((key) => {
                return $tt(key) + ': ' + response.errors[key];
            }, []).join('<br>');

            this.contentBlock.append($(' \
                <h4>Ошибка при регистрации удаленного заселения:</h4> \
                <p class="g-alert">' + text + '</p> \
            '));
        },

        showBillingError: function() {
            this.contentBlock.html($(' \
                <h4>Услуги eSigner недоступны:</h4> \
                <p class="g-alert">Пакет услуг закончился или истек пробный период.<br/>Если вы недавно приобрели новый пакет услуг, попробуйте обновить страницу.</p> \
            '));

            button.addClass("i-warning2 red");
        },

        showBillingBalanceError: function() {
            this.contentBlock.html($(' \
                <h4>Ошибка при регистрации удаленного заселения:</h4> \
                <p class="g-alert">Текущего остатка заселений недостаточно для регистрации данного бронирования.<br/>Если вы недавно приобрели новый пакет услуг, попробуйте обновить страницу.</p> \
            '));

            button.addClass("i-warning2 red");
        },

        /**
         *
         * @param {int} status Один из статусов представленный в объекте
         */
        changeButtonStatus(status) {
            this.initialButton.removeClass('status m-yellow-contrast m-blue-contrast m-green-contrast m-red-contrast');

            switch (status) {
                case this.REMOTE_BOOKING_NOT_EXISTS:
                    break;
                case this.REMOTE_BOOKING_CREATED:
                case this.REMOTE_BOOKING_LINK_SENT:
                    this.initialButton.addClass('status m-yellow-contrast');
                    break;
                case this.REMOTE_BOOKING_VISITED:
                    this.initialButton.addClass('status m-blue-contrast');
                    break;
                case this.REMOTE_BOOKING_COMPLETED:
                    this.initialButton.addClass('status m-green-contrast');
                    break;
                case this.REMOTE_BOOKING_CANCELED:
                case this.REMOTE_BOOKING_EXPIRED:
                    this.initialButton.addClass('status m-red-contrast');
                    break;
            }
        },

        showBillingInfo(billingInfo) {
            dd("billingInfo", billingInfo);

            let tarifBalance = '';
            if (billingInfo.tarif_type_id == 'trial') {
                const remaining_days = Math.round((new Date(billingInfo.trial_ends_at) - new Date()) / (1000*60*60*24));
                tarifBalance = `остаток дней: ${remaining_days > 0 ? remaining_days : 0}`;
            } else {
                tarifBalance = `остаток заселений: ${billingInfo.balance}`;
            }

            const currentTarif = $('<div/>').addClass('switcher__item').css({
                border: '0px',
                cursor: 'default'
            }).attr('onMouseOver', 'this.style.backgroundColor="#fff"').html(`Тариф: ${billingInfo.tarif_name || 'Не выбран'}, ` + tarifBalance);

            const purchaseButton = $(`<a href='${billingInfo.payment_page_url}' target='_blank' class='switcher__item button m-primary'>Купить</a>`);
            billingInfoWrapper.append(currentTarif).append(purchaseButton);
        }
    };
}

/**
 * TODO: Promises для цепочки инициализации
 *
 * Script source https://www.jsdelivr.com/package/npm/phoenix-js?path=dist%2Fglob
 *
 * @param {String} authToken - Токен авторизации пользователя
 * @param {SignerNotifications}
 * @param {SignerWidget}
 * @returns {SignerSocket}
 */
function SignerSocket(authToken, signerNotifications) {

    const WIDGET_CHANNEL = 'bnovo:widget';
    const LEGAL_ENTITY_CHANNEL = 'bnovo:legal_entity';

    let socket = null;
    let connectionData = {};
    let legalEntityChannel = null;
    let channels = {
        widget: null,
        legalEntity: null,
    };

    $.getScript(PHOENIX_SOCKET_LIB_PATH, onLoad)

    function onLoad() {
        socket = new Phoenix.Socket(SIGNER_WSS_URL, { params: { 'token': authToken } });
        socket.onOpen(function () {
            const _channels = channels;
            if (!channels.widget) {
                let channel = socket.channel(WIDGET_CHANNEL, {});
                channel.join()
                    .receive("ok", _ => {
                        if (!_channels.widget) {
                            dd("Joined widget channel");
                            getWidgetSocketData(channel);
                            _channels.widget = channel;
                        } else {
                            dd("Re-joined legal entity channel");
                        }
                    });
            }
        });
        socket.connect();
    }

    function getWidgetSocketData(channel) {
        channel.push("info", {})
            .receive("ok", resp => {
                connectionData.info = resp;
            });
    }

    function onLegalEntityChannelMessage(channel) {
        channel.on("notification", payload => {
            dd('Got legal entity notification message', payload);

            if (signerNotifications) {
                signerNotifications.pushNotification(null, payload);
            }
        });

        channel.on("ttlock_update", payload => {
            dd('Got ttlock update message', payload);

            switch (payload.type) {
                case "passcode_update":
                    $(`.s_passcode[data-pc-id="${payload.id}"]`).trigger('ws-passcode-update', payload);
                    break;

                default:
                    break;
            }
        });
    }

    return {
        socket: socket,
        channels: channels,
        socketData: connectionData,

        /**
         *
         * @returns boolean
         */
        isConnected: function () {
            return socket && socket.isConnected() && this.socketData && this.socketData.info;
        },

        joinLegalEntityChannel: function () {
            let socketAttempts = 0;
            let $this = this;
            _joinLegalEntityChannel();

            function _joinLegalEntityChannel() {
                setTimeout(() => {
                    if ($this.isConnected() && !$this.channels.legalEntity) {
                        let channel = socket.channel(LEGAL_ENTITY_CHANNEL + ":" + $this.socketData.info.legal_entity_id, {});

                        channel.join()
                            .receive("ok", resp => {

                                // Привязывать хули только если канал не создан ранее, т.к. данный колбек вызывается при реконнекте
                                if (!$this.channels.legalEntityChannel) {
                                    dd("Joined legal entity channel");
                                    $this.channels.legalEntityChannel = channel;
                                    onLegalEntityChannelMessage(channel);
                                } else {
                                    dd("Re-joined legal entity channel");
                                }
                            });
                    } else if ($this.channels.legalEntity) {
                        dd("Legal entity channel already created");
                    } else {
                        socketAttempts++;
                        dd("Unable to join legal entity channel, socket not ready");

                        if (socketAttempts < 3) _joinLegalEntityChannel();
                    }
                }, 2000);
            }
        }
    };
}

function SignerNotifications() {

    let notificationWrapper = $(' \
        <div class="sn-wrapper"> \
        </div> \
        <style> \
            .sn-wrapper { \
                position: fixed; \
                right: 32px; \
                bottom: 32px; \
                width: 300px; \
                z-index: 999; \
            } \
            .sn-notification { \
                background-color: white; \
                position: relative; \
                margin: 0 auto; \
                width: 90%; \
                padding: 8px; \
                box-sizing: border-box; \
                box-shadow: 0px 0px 6px 0px #0000006b; \
                border-radius: 4px; \
                margin-top: 4px; \
            } \
            .sn-wrapper hr { \
                margin: 4px 0; \
            } \
        </style> \
    ');

    function install() {
        $('body').append(notificationWrapper);
    }

    install();

    return {
        pushNotification: function (header, msg) {
            let message = $(' \
                <div class="sn-notification"><b>' + (header || "Виджет удаленного заселения") + '</b><hr>' + msg.message + '</div> \
            ');
            message.on('click', function (e) { $(this).remove(); });
            notificationWrapper.append(message);
        }
    };
}

function PassCodeBlock(signer_id, room_name, status, code, is_algorithmic, pc_start_datetime, pc_end_datetime, timezone) {

    const $titleBlock = $(`<h5 class="s_passcode-title">${room_name}</h5>`);
    const $codeBlock = $('<span class="s_passcode-code"></span>');
    const $codeStartBlock = $('<span class="s_passcode-start"></span>');
    const $codeEndBlock = $('<span class="s_passcode-end"></span>');
    const $codeErrorBlock = $('<span class="s_passcode-error"></span>');

    const $codeAvailabilityBlock =
        $('<span class="s-passcode_availability"/>')
            .append( 'Действует с ' )
            .append( $codeStartBlock )
            .append( ' по ' )
            .append( $codeEndBlock );

    const $contentBlock =
        $('<p class="s_passcode-content"/>')
            .append('Код от замка: ')
            .append( $codeBlock )
            .append( $codeAvailabilityBlock )
            .append( $codeErrorBlock );

    const $block =
        $(`<div class="s_passcode" data-pc-id="${signer_id}"/>`)
            .append( $titleBlock )
            .append( $contentBlock );

    function formatPasscodeDatetime(datetime_raw, timezone) {
        return new Date(datetime_raw).toLocaleDateString([], { hour: '2-digit', minute: '2-digit', timezone: timezone });
    }

    function setCodeBlockContent(status, code, start_datetime, end_datetime, timezone) {
        $codeBlock.text( formatCodeByStatus(code, status) );
        $codeStartBlock.text( formatPasscodeDatetime( start_datetime, timezone ) );

        $codeEndBlock.text( formatPasscodeDatetime( end_datetime, timezone ) );
    }

    function addShiftPeriod(block, status, type, text) {
        if(block.find('.s_passcode-shift_hour').length == 0) {
            if(status == "set") block.append($(`<a href="javascript:void(0);" class="s_passcode-shift_hour" data-pc-id="${signer_id}" data-shift-type="${type}">${text}</a>`));
        }
    }

    function formatCodeByStatus(code, status) {
        let content = "Ошибка";

        switch (status) {
            case "set":
                content = code;
                break;
            case "canceling":
                content = $tt("passcode_status." + status); // В процессе отмены
                $codeAvailabilityBlock.remove();
                break;
            case "canceled":
                content = $tt("passcode_status." + status); // Отменен
                $codeAvailabilityBlock.remove();
                break;
            case "failed":
                content = $tt("passcode_status." + status); // "Ошибка установки";
                break;
            case "expired":
                content = code + ` (${$tt("passcode_status." + status)})`; // "Истек";
                break;
            default:
                content = $tt("passcode_status.default");
                break;
        }

        return content;
    }

    setCodeBlockContent(status, code, pc_start_datetime, pc_end_datetime, timezone);
    if(!is_algorithmic) {
        addShiftPeriod($codeStartBlock, status, 'start_minus_hour', '(-1 час)');
        addShiftPeriod($codeEndBlock, status, 'end_plus_hour', '(+1 час)');
    }

    return {
        id: signer_id,
        status: status,
        room_name: room_name,
        code: code,
        start_datetime: pc_start_datetime,
        end_datetime: pc_end_datetime,
        timezone: timezone,
        is_algorithmic: is_algorithmic,

        element: $block,

        getElement() {
            return this.element;
        },

        updateBlock(status, code, is_algorithmic, start_datetime, end_datetime) {
            this.status = status;
            this.code = code;
            this.start_datetime = start_datetime;
            this.end_datetime = end_datetime;
            this.is_algorithmic = is_algorithmic;

            this.cleanError();
            $(`.s_passcode[data-pc-id="${this.id}"] .s_passcode-code`).text( formatCodeByStatus(this.code, this.status) );

            if(!this.is_algorithmic) {
                const $sEl = $(`.s_passcode[data-pc-id="${this.id}"] .s_passcode-start`).text( formatPasscodeDatetime( this.start_datetime ) );
                addShiftPeriod($sEl, this.status, 'start_minus_hour', '(-1 час)');

                const $eEl = $(`.s_passcode[data-pc-id="${this.id}"] .s_passcode-end`).text( formatPasscodeDatetime( this.end_datetime ) );
                addShiftPeriod($eEl, this.status, 'end_plus_hour', '(+1 час)');

                $('.s_passcode .s_passcode-shift_hour').removeClass('disabled');
            }
        },

        showError(code) {
            $(`.s_passcode[data-pc-id="${this.id}"] .s_passcode-error`).text( $tt('passcode_error.' + code) );

            $('.s_passcode .s_passcode-shift_hour').removeClass('disabled');
        },

        cleanError() {
            $(`.s_passcode[data-pc-id="${this.id}"] .s_passcode-error`).text('');
        }
    };
}

//////////////////////////////////////////

/**
 * Сериализует данные формы в JSON объект
 *
 * @param {JQuery} form jQuery объект формы
 */
function formDataToJson(form) {
    // var arr = form.serializeArray(), obj = {};
    // $.each(arr, function(indx, el) {
    //     obj[el.name] ? obj[el.name].push(el.value) : (obj[el.name] = el.value);
    // });
    // У bnovo есть своя глобальная функция сериализации формы. Код выше - на память.
    return get_data_from_form(form);
}

//////////////////////////////////////////

const t_ru = {
    'checkin_datetime': 'Дата заезда',
    'checkout_datetime': 'Дата выезда',
    'bookingId': 'ID бронирования',
    'customerId': 'Внешний ИД гостя',
    'first_name': 'Имя',
    'last_name': 'Фамилия',
    'middle_name': 'Отчество',
    'phone': 'Телефон',
    'email': 'E-mail гостя',
    'number_of_adults': 'Количество взрослых гостей',
    'status.new': 'Приглашение к подписанию сформировано, но не отправлено автоматически.',
    'status.link_sent': 'Приглашение к подписанию сформировано и отправлено гостю',
    'status.visited': 'Гость перешел по ссылкe, заполняет персональные данные,<br/>вы получите уведомление после подписания',
    'status.waiting_signing_of_other_contracts': 'Гость подписал свой договор. Ожидание подписания другими гостями',
    'status.signed': 'Договор успешно подписан',
    'locks_status.ok': 'Доступно оформление удалённого заселения с замками',
    'locks_status.locks_not_exists': 'В бронировании нет привязанных к апартаментам замков.<br/>В данный момент доступно только подписание документов.',
    'locks_status.period_not_set': 'В бронировании есть периоды, которые не распределены по номерам.<br/>В данный момент доступно только подписание документов.',
    'locks_status.required_synchronization': 'В бронировании есть апартаменты, не синхронизированные с системой eSigner.<br/>Пожалуйста, подождите некоторое время.<br/>В данный момент доступно только подписание документов.',
    'locks_status.account_not_set': 'Ваш аккаунт не синхронизирован с TTLock.<br/>В данный момент доступно только подписание документов.',
    'locks_status.ttlock_timeout': 'Сервис замков недоступен, попробуйте обновить страницу.<br/>В данный момент доступно только подписание документов.',
    'passcode_status.failed': 'Ошибка установки',
    'passcode_status.default': 'Устанавливается',
    'passcode_status.expired': 'Истек',
    'passcode_error.ttlock_timeout': 'Ошибка запроса к TTLock, попробуйте позже',
    'passcode_error.gateway_busy': 'Шлюз временно занят, попробуйте позже',
    'passcode_error.gateway_is_offline': 'Шлюз отключен, включите и попробуйте снова',
    'passcode_error.lock_wo_gateway': 'Замок не подключен к шлюзу',
    'passcode_error.unexpected': 'Неизвестная ошибка при выполнении операции, попробуйте позже',
};

function $tt(msgid) {
    let tt = t_ru;
    let translate = tt[msgid];
    if (translate !== undefined) {
        return translate;
    } else {
        return msgid;
    }
}

function dd(something, data) {
    if (LOGGING) {
        if (data !== undefined) {
            console.log(something, data);
        } else {
            console.log(something);
        }
    }
}