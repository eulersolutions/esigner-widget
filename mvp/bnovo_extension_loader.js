// ==UserScript==
// @name         Bnovo Esigner Extension Loader
// @namespace    https://online.bnovo.ru
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://online.bnovo.ru/*
// @icon         https://www.google.com/s2/favicons?domain=bnovo.ru
// @grant        none
// ==/UserScript==

const ESIGNER_WIDGET_LOADER_LOGGING = true;
const ESIGNER_WIDGET_LOADER_URL = 'https://widget-api.euler.solutions/widget_loader/bnovo/v1/';
const ESIGNER_WIDGET_LOADER_SIGNER_TOKEN = 'sometoken';

$(document).ready(function (e) {
    $.ajax({
        url: ESIGNER_WIDGET_LOADER_URL + "?token=" + ESIGNER_WIDGET_LOADER_SIGNER_TOKEN,
        type: "GET",
        dataType: 'script',
        success: (data) => {
            dd("Скрипт виджета eSigner загружен");
        },
        error: (error) => {
            dd("Ошибка загрузки скрипта виджета eSigner", error);
            reject(error);
        }
    });

});

function dd(something, data) {
    if (ESIGNER_WIDGET_LOADER_LOGGING) {
        if (data !== undefined) {
            console.log(something, data);
        } else {
            console.log(something);
        }
    }
}