let esDomain = "https://widget-api.esigner.ru";
let esWssDomain = "wss://widget-api.esigner.ru"; 

if(window._eswc && window._eswc.esDomain) {
    esDomain = window._eswc.esDomain;
    esWssDomain = window._eswc.esWssDomain;
}

/**
 *
 */
 const eSignerWidget = function(token) {
    let api = new eSignerWidgetApi(token);

    return {
        eApi: api,
        state: null,
        widgetCollection: {},
        contractsCollection: {},

        init: function() {
            const $this = this;
            insertWidgetStyles();

            $this.loadWidgetState().then((state) => {
                let contractIds = [];
                $('.esw-parent').each(function(i) {
                    let el = $(this);
                    let contractId = el.attr('attr-order-id');
                    contractIds.push(contractId);

                    let options = {
                        contract_number: contractId,
                        checkin_datetime: el.attr('attr-dt-begin'),
                        checkout_datetime: el.attr('attr-dt-end'),
                        phone: el.attr('attr-phone'),
                        full_name: el.attr('attr-name'),
                        timezone: el.attr('attr-timezone')
                    };

                    let wBlock = new eSignerWidgetBlockView(el, options);
                    let contract = new eSignerContractState(wBlock, options);
                    wBlock.init();
                    $this.saveWidgetBlock(contractId, wBlock);
                    $this.contractsCollection[contractId] = contract;
                });

                $this.loadContracts(contractIds);
                let socket = new eSignerSocket(token, this);
                socket.start();
            });
        },

        /**
         * Получение информации по работе виджета
         */
        loadWidgetState: async function() {
            return this.eApi.getWidgetState()
                .then(function(state) {
                    this.state = state;
                }).catch((error) => {
                    console.log('[ESigner] Error on loading widget state', error);
                });
        },

        /**
         * Получение информации по контрактам
         */
        loadContracts(contractsIds) {
           return this.eApi.getContractStates(contractsIds)
                .then((contracts) => {
                    this.setContractsScreens(contracts.data);
                }).catch((error) => {
                    console.log('[ESigner] Error on contracts data', error);
                });
        },

        saveWidgetBlock: function(bookingId, widgetBlock) {
            this.widgetCollection[bookingId] = widgetBlock;
        },

        /**
         * Принимает на вход полученные с бекенда объекты контрактов и показывает для каждого контракта
         * нужный экран.
         *
         * Каждый ранее созданный контракт сохраняет в переменную contractsCollection
         */
        setContractsScreens: function(contractsStates) {
            const cNumbers = Object.keys(this.contractsCollection);

            cNumbers.forEach((contractNumber) => {
                let contract = this.contractsCollection[contractNumber];

                let created = null;
                for (let i = 0; i < contractsStates.length; i++) {
                    const el = contractsStates[i];
                    if(el.contract_number == contractNumber) {
                        created = el;
                        break;
                    }
                }

                if(created) {
                    contract.setContractState(created);
                    this.toogleContractScreen(contract);
                } else if(contract.state == null) {
                    contract.block.showWelcomeScreen(contract, this.onFormSubmit.bind(this));
                }
            });
        },

        /**
         * @param {*} contract
         */
        toogleContractScreen(contract) {
            const block = contract.block;
            switch(contract.state.status) {
                case 'signed':
                    block.showContractSignedScreen(contract.state);
                    break;
                case 'new':
                case 'visited':
                case 'link_sent':
                    let sentSmsCallback = function() {
                        this.onSentLink(contract.contract_number);
                    }.bind(this);
                    block.showContractInfoScreen(contract.state.status, contract.state.link, sentSmsCallback);
                    break;
                case 'expired':
                    block.showExpiredScreen();
                    break;
                default: '';
            }
        },

        onFormSubmit: function(form) {
            // Контракты приходят массивом. Унаследованная логика от множественных заселений
            this.eApi.registerContract(form).then((response) => {
                // контракты обновятся по сокету
            }).catch((error) => {
                console.log('[ESigner] Error on creating new contract', error);
            });
        },

        onSentLink: function(contractNumber) {
            this.eApi.sentSmsLink(contractNumber).then(() => {
                // контракты обновятся по сокету
            }).catch((error) => {
                console.log('[ESigner] Error on sending sms link', error);
            });
        }
    };
};

const insertWidgetStyles = function() {
    //
    const styles = $(`
        <style>
            ._esw-content {
                font-size: 14px;
                padding-top: 8px;
                display: none;
            }
            ._esw-content_p {
                margin-bottom: 8px;
            }
            ._esw-content_p:last-child {
                margin-bottom: 0px;
            }
            ._esw-copy-link {
                color: inherit !important;
            }
            ._esw-header_arrow {
                width: 12px;
                position: absolute;
                right: 8px;
                top: 8px;
                transition: transform .3s;
            }
            ._esw-content_header {
                font-size: 14px;
                margin-top: 16px;
                font-weight: 700;
                position: relative;
                cursor: pointer;
            }
            ._esw-content_header.opened ._esw-header_arrow {
                transform: rotate(-180deg);
            }
            ._esw-content_header ._esw-status {
                display: inline-block;
                width: 0.8em;
                height: 0.8em;
                border-radius: 4px;
                margin-left: 4px;
                position: relative;
                top: 0.05em;
                border: 1px solid rgba(196, 196, 196, 1);
                visibility: hidden;
            }
            ._esw-status.visited {
                visibility: inherit;
                background-color: rgba(188, 206, 251, 1);
            }
            ._esw-status.created {
                visibility: inherit;
                background-color: rgba(240, 191, 76, 1);
            }
            ._esw-status.signed {
                visibility: inherit;
                background-color: rgba(158, 235, 71, 1);
            }
        </style>
    `);

    $('head').append(styles);
};

/**
 *
 */
const eSignerWidgetBlockView = function(parent, options) {

    // Сборка блока, раскрывающего аккордион при клике
    const startBlock = $(`
        <div class="_esw-content_header">
            удалённое заселение 
        </div>
    `);
    const statusIndicator = $('<span class="_esw-status"></span>');
    const arrow = $(`<img class="_esw-header_arrow" src="/doc/i/select_arrow.png"/>`);
    startBlock.append(statusIndicator);
    startBlock.append(arrow);

    // Сборка контент блока
    const content = $(`
        <div class="_esw-content">Идет загрузка данных...</div>
    `);

    // Блок для показа ошибки
    const errorBlock = $(`
        <div class="_esw-error"></div>
    `);

    return {
        screen: null,
        startBlock: startBlock,
        contentBlock: content,
        errorBlock: errorBlock,
        statusIndicator: statusIndicator,


        init: function() {
            parent.append(this.startBlock);
            parent.append(this.contentBlock);
            parent.append(this.errorBlock);

            this.startBlock.on('click', function() {
                const el = $(this);
                el.toggleClass('opened');
                el.next().slideToggle();
            });
        },

        showFatalErrorScreen: function(message) {
            this.contentBlock.hide();
            this.errorBlock.html(message).show();
        },

        showWelcomeScreen: function(contract, onSubmit) {
            if(this.screen == 'welcome') return;
            this.screen = 'welcome';

            let firstName = "";
            let lastName = "";

            const split = contract.full_name.split(" ");
            switch(split.length) {
                case 2:
                    firstName = split[0];
                    lastName = split[1];
                    break;
                default:
                    firstName = contract.full_name;
                    lastName = "sutochno";
            }

            let welcomeBlock = $(`
                <form>
                    <input type="hidden" name="contract_number" value="${contract.contract_number}"/>
                    <input type="hidden" name="phone" value="${contract.phone}"/>
                    <input type="hidden" name="checkin_datetime" value="${contract.checkin_datetime}"/>
                    <input type="hidden" name="checkout_datetime" value="${contract.checkout_datetime}"/>
                    <input type="hidden" name="first_name" value="${firstName}"/>
                    <input type="hidden" name="last_name" value="${lastName}"/>
                    <input type="hidden" name="timezone" value="Europe/Moscow"/>
                    <div class="_esw-content_p">
                        <input type="checkbox" name="send_link_to_guest"/> Отправить СМС автоматически
                    </div>
                    <div class="_esw-content_p">
                        <button class="btn btn-default btn-block btn-mb" style="width: initial; padding: 4px 8px; display: inline-block;" type="submit">Подписать договор</button>
                    </div>
                </form>
            `);

            welcomeBlock.on('submit', function(e) {
                e.preventDefault();

                let form = $(this).serialize();
                onSubmit(form);

                return false;
            });

            this.contentBlock.html(welcomeBlock);
        },

        showContractSignedScreen: function(contractState) {
            if(this.screen == 'contractSigned') return;
            this.screen = 'contractSigned';
            this.statusIndicator.removeClass('visited').removeClass('created');
            this.statusIndicator.addClass('signed');

            let statusText = 'договор подписан';
            const sign_date = new Date(contractState.sign_datetime).toLocaleDateString([], { hour: '2-digit', minute: '2-digit', second: '2-digit' });
            let filesList = [];
            for(let i = 0; i < contractState.documents.length; i++) {
                const el = contractState.documents[i];
                filesList.push(`<a target="_blank" href="${el.path}">${el.title}</a>`);
            }
            const signedScreen = $(`
                <div class="_esw-content_p">
                    Статус: ${statusText}
                    <br/>Дата подписания: ${sign_date}
                </div>
                <div class="_esw-content_p">
                    ФИО: ${contractState.full_name}
                    <br/>Серия и номер паспорта: ${contractState.passport_series} ${contractState.passport_number}
                    <br/>Дата выдачи: ${contractState.passport_date}
                    <br/>Кем выдан: ${contractState.passport_issued_by}
                </div>
                <div class="_esw-content_p">
                    ${filesList.join('<br/>')}
                </div>
            `);
            this.contentBlock.html(signedScreen);
        },

        /**
         * 
         * @param {String} status 
         * @param {String} link 
         * @param {function} onSentSms 
         * @returns 
         */
        showContractInfoScreen: function(status, link, onSentSms = () => {}) {
            if(this.screen == 'contractInfo' + status) return;
            this.screen = 'contractInfo' + status;
            this.statusIndicator
                .removeClass('created')
                .removeClass('visited')
                .removeClass('signed');

            let sentSmsBlock = '';
            let statusText = null;
            switch(status) {
                case 'new':
                    statusText = 'создано';
                    this.statusIndicator.addClass('created');
                    sentSmsBlock = $(`
                        <div class="_esw-content_p">
                            <a href="javascript:void(0)" class="btn btn-default btn-block btn-mb">Отправить приглашение</a>
                        </div>    
                    `);
                    sentSmsBlock.on('click', 'a', function(e) {
                        e.preventDefault();
                        $(this).remove();
                        onSentSms();
                        return false;
                    });
                    break;
                case 'link_sent':
                    this.statusIndicator.addClass('created');
                    statusText = 'приглашение отправлено';
                    break;
                case 'visited':
                    this.statusIndicator.addClass('visited');
                    statusText = 'гость заполняет анкету';
                    break;
                default: statusText = 'неизвестный статус';
            }
            const infoScreen = $(`
                <div class="_esw-content_p">Статус: ${statusText}</div>
                <div class="_esw-content_p"><a class="_esw-copy-link" onclick="copyLink('${link}')" href="javascript:void(0);">Скопировать ссылку</a></div>`
            );
            this.contentBlock.html(infoScreen).append(sentSmsBlock);
        },

        /**
         * 
         */
        showExpiredScreen: function() {
            if(this.screen == 'contractExpired') return;
            this.screen = 'contractExpired';
            this.statusIndicator
                .removeClass('created')
                .removeClass('link_sent')
                .removeClass('visited');
                
            const expiredScreen = $(`
                <div class="_esw-content_p">Истек срок оформления удалённого заселения</div>
            `);
            this.contentBlock.html(expiredScreen);
        }
    };
};

const eSignerWidgetApi = function(token) {
    const backendUrl = esDomain + '/partner_api/sutochno/v1';
    // const backendUrl = 'http://localhost:4011/partner_api/sutochno/v1';

    const beforeSend = function(xhr) {
        xhr.setRequestHeader('Authorization', 'Bearer ' + token);
    };
    const onSuccess = function(data, res, rej) {
        if(data.status == "ok") {
            res(data);
        } else {
            rej(data.error);
        }
    };

    function get(url, data = {}) {
        return new Promise((res, rej) => {
            $.ajax({
                url: url,
                withCredentials: true,
                dataType: 'json',
                data: data,
                beforeSend: beforeSend,
                success: (data) => onSuccess(data, res, rej),
                error: (error) => rej(error),
            });
        });
    }

    function post(url, data) {
        return new Promise((res, rej) => {
            $.ajax({
                method: 'POST',
                url: url,
                withCredentials: true,
                dataType: 'json',
                data: data,
                beforeSend: beforeSend,
                success: (data) => onSuccess(data, res, rej),
                error: (error) => rej(error),
            });
        });
    }

    return {
        getWidgetState: function() {
            return get(backendUrl + '/widget');
        },

        getContractStates: function(contractsIds) {
            return get(backendUrl + '/contracts', {contracts: contractsIds});
        },

        registerContract: function(data) {
            return post(backendUrl + '/register_contract', data);
        },

        sentSmsLink: function(contract_number) {
            return get(backendUrl + '/send-link/' + contract_number);
        }
    };
};

/**
 *
 * @param {eSignerWidgetBlockView} block
 * @param {*} opts
 * @returns
 */
const eSignerContractState = function(block, opts) {
    return {
        state: null,
        block: block,

        contract_number: opts.contract_number,
        phone: opts.phone,
        full_name: opts.full_name,
        last_name: "test",
        checkin_datetime: opts.checkin_datetime,
        checkout_datetime: opts.checkout_datetime,
        timezone: opts.timezone,

        setContractState: function(data) {
            this.state = data;
        },
        isCreated: function() {
            return this.state !== null;
        }
    };
};

const eSignerSocket = function(token, widget) {
    const wssUrl = esWssDomain + '/widget/sutochno/v1';
    let socket = null;
    const channels = {
        account: null,
        widget: null,
    };
    const connection = {
        info: null
    };

    function joinChannels() {
        socket = new Phoenix.Socket(wssUrl, { params: { 'token': token } });

        socket.onOpen(function () {
            const _channels = channels;
            if (!channels.widget) {
                let channel = socket.channel('sutochno:widget', {});

                channel.join()
                    .receive("ok", _ => {
                    if (!_channels.widget) {
                        console.info("[ESigner] Joined widget channel");
                        getWidgetSocketData(channel);
                        _channels.widget = channel;
                    } else {
                        console.info("[ESigner] Re-joined widget channel");
                    }
                });
            }
        });

        socket.connect();
    }

    function loadSocketScript() {
        return new Promise(function(res, rej) {
            $.ajax({
                url: esDomain + '/js/phoenix-1.6.2.min.js',
                type: "GET",
                dataType: 'script',
                success: (data) => res(data),
                error: (error) => rej(error)
            });
        });
    }

    function getWidgetSocketData(channel) {
        channel.push("info", {})
            .receive("ok", resp => {
                connection.info = resp;
                joinOtherChannels();
            });
    }

    function joinOtherChannels() {
        let accountId = connection.info.legal_entity_id;

        if(!channels.account) {
            let channel = socket.channel('sutochno:legal_entity:' + accountId, {});
            const _channels = channels;

            channel.join()
                .receive("ok", _ => {
                    if (!_channels.account) {
                        console.info("[ESigner] Joined account channel");
                        _channels.account = channel;
                        onAccountChannelMessage(channel);
                    } else {
                        console.info("[ESigner] Re-joined account channel");
                    }
                });
        }
    }

    function onAccountChannelMessage(channel) {
        channel.on("application_created", payload => {
            widget.setContractsScreens([payload]);
        });

        channel.on("application_updated", payload => {
            widget.setContractsScreens([payload]);
        });
    }

    return {
        isConnected: false,
        channels: channels,

        start: function() {
            loadSocketScript().then(() => joinChannels());
        },
    };
}

window.copyLink = function(link) {
    navigator.clipboard.writeText(link)
        .then(() => {
        // Получилось!
        })
        .catch(err => {});
}

const eToken = window._eswc.token;
const esw = new eSignerWidget(eToken);
esw.init();