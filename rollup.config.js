import commonjs from "@rollup/plugin-commonjs";
import vue from 'rollup-plugin-vue';
import buble from '@rollup/plugin-buble';

const packageJson = require("./package.json");

export default {
    input: "src/main.js",
    output: [
      {
        file: packageJson.umd,
        format: "umd",
        sourcemap: true,
        name: "ESWidget",
        exports: 'named'
      },
    ],
    plugins: [
        vue({
            css: true,
            compileTemplate: true,
        }),
        commonjs(),
        buble({
          transforms: {
            asyncAwait: false
          }
        }),
    ],
};