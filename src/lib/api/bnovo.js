export default function() {

    function getInputValue(inputSelector) {
        let el = document.querySelector(inputSelector);
        return el.value;
    }

    return {
        /**
         * Возвращает данные по партнеру(аккаунту бново) для запуска виджета
         * 
         * @return {Promise<Response>}
         */
        partnerUser() {
            const req =
                fetch('/account/current')
                .then(function(response) {
                    return response.json();
                })
                .then(function(json) {
                    const partnerData = {
                        // TODO: Переименовать в user_email
                        bnovo_user_email: json.current_user.email,
                        notify_list: [
                            json.current_user.email,
                            json.hotel.owner_email
                        ],
                        hotel_id: json.hotel.id,
                        timezone: json.hotel.timezone,
                        currency: json.hotel.currency,
                        language: json.hotel.language
                    };

                    return partnerData;
                });

            return req;
        },

        /**
         * TODO: агрегация данных для работы через API биново
         */
        async partnerData() {
            const adults_number = parseInt(getInputValue('[name="adults"]'));
            const childrens_number = parseInt(getInputValue('[name="children"]'));

            let data = {
                contract_number: '',
                bnovo_booking_id: getInputValue('[name="booking_id"]'),
                bnovo_customer_id: getInputValue('[name="customer_id"]'),
                bnovo_hotel_id: current_hotel_id, // global
                bnovo_user_id: current_user_id, // global
                total_price: getInputValue('#booking_initial_total_amount'),
                // daily_price: getInputValue(''),
                // status: getInputValue('.switcher .switcher__item.m-current').attr('data-status-id') || 1,
                checkin_datetime: getInputValue('[name="date_from"]') + ' ' + getInputValue('[name="hour_from"]'),
                checkout_datetime: getInputValue('[name="date_to"]') + ' ' + getInputValue('[name="hour_to"]'),
                first_name: getInputValue('[name="name"]'),
                last_name: getInputValue('[name="surname"]'),
                middle_name: getInputValue('[name="middlename"]'),
                phone: getInputValue('[name="phone"]'),
                email: getInputValue('[name="email"]'),
                number_of_adults: adults_number,
                number_of_guests: adults_number + childrens_number,
                services: services_existing_prices || [] // global
            };

            let res = await this.bookingData(data.bnovo_booking_id).then(r => r.json());
            data.contract_number = res.booking.number;
            data.contract_id = res.booking.number;
            data.arrival_datetime = res.booking.arrival;

            let templates = [];
            Object.entries(res.templates).forEach(([i, item]) => {
                templates.push({name: item.name, id: item.name});
            });
            data.templates = templates;
            data.checkinTemplates = templates;
            
            data.customer_id = res.booking.customer_id;
            data.customers = res.booking.customers;
            
            data.apartments = res.merged_rooms;

            return data;
        },

        bookingData(bookingId) {
            return fetch('/booking/general/' + bookingId, {headers: {Accept: 'application/json'}});
        },
    }
};