export default function(token, url, partnerApi) {
    
    const _authToken = token;
    const _domain = url;
    const _partnerApi = partnerApi;

    function get(query, headers) {
        let requestOptions = {
            method: 'GET',
            headers: (headers || {})
        };

        return req(query, requestOptions);
    }

    function post(query, headers, body) {
        let requestOptions = {
            method: 'POST',
            headers: (headers || {})
        };

        return req(query, requestOptions, body);
    }
    
    function put(query, headers, body) {
        let requestOptions = {
            method: 'PUT',
            headers: (headers || {})
        };

        return req(query, requestOptions, body);
    }

    function req(query, requestOptions, body) {
        requestOptions.headers = Object.assign({
                'Authorization': 'Bearer ' + _authToken,
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }, requestOptions.headers);

        if(typeof body !== undefined) {
            requestOptions.body = JSON.stringify(body);
        }

        return fetch('//' + _domain + _partnerApi + query, requestOptions).then((res) => {
            switch(res.status) {
                case 401:
                    throw new Error("Пользователь виджета не может быть авторизован");
                case 404:
                    throw new Error("Запрашиваемый эндпоинт несуществует");
                case 500:
                    console.error("[ESigner] Ошибка сервиса", res.body);
                    throw new Error("Ошибка сервиса ESigner");
            }

            return new Promise((resolve) => resolve(res));
        });
    }

    return {
        getWidgetState(hotelId) {
            const urlPath = `/widget`;
            const headers = {
                cache: "no-store"
            };

            return get(`${urlPath}?hotel_id=${hotelId}`, headers);
        },
        getContractState(contractNumber) {
            const urlPath = `/bookings`;
            const headers = {
                cache: "no-store"
            };

            return get(`${urlPath}/${contractNumber}`, headers);
        },
        getApartmentsLocksData(apartments) {
            let apartments_ids = [];
            for (let i = 0; i < apartments.length; i++) {
                const el = apartments[i];
                apartments_ids.push('apartments[]=' + el.room_id);
            }

            const urlPath = '/lock_managment/apartments_locks?' + apartments_ids.join('&');
            const headers = {
                cache: "no-store"
            };

            return get(`${urlPath}`, headers);
        },

        registerContract(payload) {
            const urlPath = '/bookings';
            const headers = {};
            const body = payload;
            
            return post(`${urlPath}`, headers, body);
        },

        registerContractWithLocks(payload) {
            const urlPath = '/bookings-locks';
            const headers = {};
            const body = payload;

            return post(`${urlPath}`, headers, body);
        },
        
        sendLinkToGuest(contractNumber) {
            const urlPath = '/send-link';
            const headers = {
                cache: "no-store",
                pragma: "no-cache"
            };
            const body = {};

            return get(`${urlPath}/${contractNumber}`, headers, body);
        },

        updateGuestData(contractNumber, data, customer_id) {
            const urlPath = '/bookings';
            const headers = {};
            const body = data;

            return put(`${urlPath}/${contractNumber}/guests/${customer_id}`, headers, body);
        },

        shiftPasscodeHour(passcodeId, shiftType) {
            const urlPath = '/passcodes';
            const headers = {};
            const body = {
                edit_value: shiftType
            };

            return post(`${urlPath}/${passcodeId}/shift-period`, headers, body);
        }
    };
};