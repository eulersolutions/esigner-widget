export default {
    OK: 'ok',
    LOADING: 'loading',
    // Период для замков неопределен
    PERIOD_NOT_SET: 'period_not_set',
    // Требуется синхронизация апартаментов
    REQUIRED_SYNCH: 'required_synchronization',
    // У апартаментов нет замков
    EMPTY_LOCKS: 'locks_not_exists',
    // Таймаут подключения к TTLock
    TIMEOUT: 'ttlock_timeout',
};
