/**
 * 
 * @param {*} isMultiple 
 * @param {*} isUsingLockService 
 * @param {*} contractTemplate 
 * @param {*} checkinInstructionTemplate 
 * @param {*} widgetApi 
 * @param {*} store 
 * @returns {Promise}
 */
export default async function(
    isMultiple,
    adultCustomers,
    isUsingLockService,
    contractTemplate,
    checkinInstructionTemplate,
    sendMessageToClients,
    partnerData,
    partnerUser,
    widgetApi,
) {

    // Обязательные поля для регистрации удаленного заселения
    /**
     bnovo_user_email
     bnovo_booking_id
     bnovo_customer_id
     bnovo_hotel_id
     bnovo_custom_tempplate
     bnovo_checkin_instruction_template
     schedule
     notify_list
     */
    
    let partnerDataCopy = Object.assign({}, partnerData);

    // В обоих случаях бек ждет ассоциативный массив, где ключ элемента - порядковый индекс элемента
    let apartments = {};
    partnerData.apartments.forEach((el, index) => {
        apartments[index] = el;
    });
    let schedule = {};
    if(partnerData.schedule) partnerData.schedule.forEach((el, index) => {
        schedule[index] = el;
    });

    // Это очень странно, да, но бек смотрит на данные ключи именно так
    partnerDataCopy.schedule = apartments;
    partnerDataCopy.apartments = schedule;

    let data = Object.assign(
        {
            // выбранный шаблон договора
            bnovo_custom_template: contractTemplate,
            // выбранный шаблон инструкции
            bnovo_checkin_instruction_template: checkinInstructionTemplate,
        },
        partnerDataCopy,
        partnerUser,
    );

    // чекбокс отправки смс. Бек принимает любое значение ключа send_link_to_guest как true, потому ключ добавляется только
    // если надо отправить смс
    if(sendMessageToClients) {
        data.send_link_to_guest = true;
    }

    if(isMultiple) {
        data.signer_contract_strategy = 'separate_contract_for_each_guest';
        data.signer_extra_customers = {};
        for (let i = 0; i < adultCustomers.length; i++) {
            const el = adultCustomers[i];
            data.signer_extra_customers[el.phone] = {
                bnovo_customer_id: el.id,
                phone: el.phone,
                email: el.email,
                name: el.name,
                surname: el.surname
            };
        }
    } else {
        data.signer_contract_strategy = 'booking_customer_only';
    }

    // Отличаются эндпоинты регистрации контракта с замками или без
    let promise = null;
    if(isUsingLockService) {
        promise = widgetApi.registerContractWithLocks(data);
    } else {
        promise = widgetApi.registerContract(data);
    }

    return promise.then(r => r.json());
}