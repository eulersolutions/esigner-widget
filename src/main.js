import EsignerWidget from "./ESWidget.vue";
import Vuex from 'vuex';
import store from "./store/store";
import $tt from './translates.js';

// Объявление функции установки, выполняемой Vue.use()
export function install(Vue) {
  if (install.installed) return;
  install.installed = true;

  Vue.use(Vuex);

  // https://stackoverflow.com/questions/53089441/how-to-access-vuex-from-vue-plugin
  Vue.eswStore = store;
  Vue.prototype.$eswStore = store;

  // Подключение локалей
  Vue.$tt = $tt;
  Vue.prototype.$tt = $tt;

  Vue.component('ESWIdget', EsignerWidget);
}

// Создание значения модуля для Vue.use()
const plugin = {
  install
};

// Автоматическая установка, когда vue найден (например в браузере с помощью тега <script>)
let GlobalVue = null;
if (typeof window !== 'undefined') {
  GlobalVue = window.Vue;
} else if (typeof global !== 'undefined') {
  GlobalVue = global.Vue;
}
if (GlobalVue) {
  GlobalVue.use(plugin);
}

// Экспорт компонента, для использования в качестве модуля (npm/webpack/etc.)
export default EsignerWidget;