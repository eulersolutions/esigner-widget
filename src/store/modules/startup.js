import BnovoApi from '../../lib/api/bnovo';
import ESWidgetApi from '../../lib/api/esigner';
import StartupChecks from '../../lib/enums/startup_checks';
import ESignerHelper from '../../store/partners/bnovo_helper';

const CONTRACT_PHONE_REGEX = /^\+?7\d{10}$|^8\d{10}$/;

function translateStartupError(startupCheck) {
    let message = '';
    switch (startupCheck) {
        case StartupChecks.ERROR_PHONE_NOT_FOUND:
            message = 'Не указан номер телефона у гостя';
            break;
        case StartupChecks.ERROR_PHONE_RESTRICTED:
            message = 'Номер телефона гостя не принадлежит Российским операторам сотовой связи';
            break;
        case StartupChecks.ERROR_ACCOUNT_WO_PERMISSIONS:
            message = `
          Указанные в системе Signer логин-пароль от аккаунта в Bnovo не имеют доступа к данному отелю
        `;
            break;
        case StartupChecks.ERROR_CREDENTIALS_INVALID:
            message = `
          Signer не может подключиться к API Bnovo с указанными логином-паролем в системе Signer.
        `;
            break;
        case StartupChecks.ERROR_CHECKINDATE_IN_PAST:
            message = `
          Дата заезда должна быть в будущем времени
        `;
            break;
        case StartupChecks.ERROR_FULLNAME_INVALID:
            message = `
          В ФИО гостя присутствуют недопустимые символы
        `;
            break;
    }
    return message;
}

const state = {
    isReady: false,
    startupCheck: null,
};

const getters = {
    isReady: (state) => state.isReady,
};

const actions = {
    setWidgetReady({ commit }) {
        commit('WIDGET_READY');
    },

    /**
     * Экшен, запускающий подругзку всех необходимых для работы данных
     */
    async startup({ commit }, opts) {
        const state = this.state;

        const contractId = opts.contractId;
        commit('COMMIT_CONTRACT_ID', contractId);

        const contractPhone = opts.contractPhone;
        commit('COMMIT_CONTRACT_PHONE', contractPhone);

        // Создания инстансов АПИ
        const esApiOpts = opts.esApi;
        commit('REGISTER_PARTNER_API', new BnovoApi());
        commit('REGISTER_ESWIDGET_API', new ESWidgetApi(esApiOpts.token, esApiOpts.domain, esApiOpts.partnerApi));

        // Получение необходимой информации пользователя виджета
        const partnerUser = await this.processApiRequest(commit, state.partnerApi.partnerUser());
        commit('COMMIT_PARTNER_USER', partnerUser);
        // Получение данных по бронированию, для которого оформляется Удалённое заселение
        const partnerData = await state.partnerApi.partnerData();
        partnerData.phone = contractPhone;
        commit('COMMIT_PARTNER_DATA', partnerData);

        // Получение стейта виджета с бекенда eSigner
        // hotel_id - ИД отеля, который привязан к аккаунту eSigner
        const hotel_id = state.partnerUser.hotel_id;
        const widgetState =
            await this.processApiRequest(commit, state.eswidgetApi.getWidgetState(hotel_id).then(r => r.json()));
        commit('COMMIT_WIDGET_STATE', widgetState.data);
        commit('COMMIT_WIDGET_BILLING_STATE', widgetState.data.billing_info);

        await this.dispatch('getWidgetContractState');

        await this.dispatch('startupChecks');
    },

    /**
     * Экшен, запускающий проверки данных для запуска виджета
     */
    async startupChecks({ commit }) {
        const state = this.state;

        let checksStatus = StartupChecks.OK;
        // Если с бекенда пришло значение credentials_valid = false
        // То виджет должен приостановить работу
        if (!state.widgetState.credentials_valid) {
            checksStatus = StartupChecks.ERROR_CREDENTIALS_INVALID;
        }
        // Если с бекенда пришла ошибка о том, что бекенд не может работать с текущим отелем, т.к. логин пароль в системе Signer
        // для работы с АПИ bnovo корректны, но не имеют доступ к текущему отелю.
        if (!state.widgetState.enough_permissions) {
            checksStatus = StartupChecks.ERROR_ACCOUNT_WO_PERMISSIONS;
        }
        // Телефон должен быть заполнен в форме гостя
        if (!state.contractPhone) {
            checksStatus = StartupChecks.ERROR_PHONE_NOT_FOUND;
        } else {
            const phone = state.contractPhone.replaceAll(/[^\d^\+]+/g, '');
            const phoneCheck = phone.match(ESignerHelper.CONTRACT_PHONE_REGEX);

            // Ограничение на формат номера телефона
            if (!phoneCheck) {
                checksStatus = StartupChecks.ERROR_PHONE_RESTRICTED;
            }
        }

        // Дата заезда должна быть на текущий момент или позже
        if (state.esContractState.code === 0 && new Date(state.partnerData.arrival_datetime) < new Date()) {
            checksStatus = StartupChecks.ERROR_CHECKINDATE_IN_PAST;
        }

        // В ФИО гостя не должно быть недопустимых символов
        if ( state.partnerData ) {
            const fio = state.partnerData.first_name + state.partnerData.middle_name + state.partnerData.last_name;

            const fioCheck = fio.match(ESignerHelper.GUEST_FULLNAME_REGEX);
            if (!fioCheck) {
                checksStatus = StartupChecks.ERROR_FULLNAME_INVALID;
            }
        }

        // Пуш проваленной проверки как фатальной ошибки
        if (checksStatus != StartupChecks.OK) {
            commit('HANDLE_FATAL_ERROR', new Error(translateStartupError(checksStatus)));
        }

        commit('COMMIT_STARTUP_CHECKS', checksStatus);
    },
};

const mutations = {
    WIDGET_READY: (state) => state.isReady = true,
    COMMIT_STARTUP_CHECKS: function (state, startupCheck) {
        state.startupCheck = startupCheck;
    },
};

export default {
    state,
    getters,
    actions,
    mutations
};