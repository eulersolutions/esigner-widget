

export default {
    signedStatuses: ['signed', 'waiting_signing_of_other_contracts'],
    SERVICES: {
        REMOTE_CONTRACT_SIGNING: 'remote_contract_signing',
        REMOTE_LOCK_MANAGEMENT: 'remote_lock_management',
        SEND_SMS_LINK_TO_CLIENT: 'send_sms_link_to_client',
        MULTI_GUEST_SIGNING: 'multi_guest_signing',
        MOBILE_ID: 'mobile_id',
        PASSPORT_OCR: 'passport_ocr'
    },

    /**
     * Префиксы сокет каналов на бекенде
     */
    CHANNELS: {
        /**
         * Канал виджета
         */
        WIDGET_CHANNEL: 'bnovo:widget',
        /**
         * Канал юридического лица (пользователя виджета)
         */
        ACCOUNT: 'bnovo:legal_entity',
    },

    /**
     * Regex для проверки номера телефона
     */
    CONTRACT_PHONE_REGEX: /^\+?7\d{10}$|^8\d{10}$/,

    /**
     * Regex для проверки ФИО гостя
     */
    GUEST_FULLNAME_REGEX: /^[а-яА-Яр-юA-Za-z]+([\-\s–]?[а-яА-Яр-юA-Za-z])+$/,

    /**
     * Определяет и возвращает уникальный идентификатор контракта из системы ESigner
     * основываясь на ключевых полях сущностей.
     * Если одно из ключевых полей будет изменено - изменится ключ.
     * 
     * Может использоваться для определения необходимости ре-рендера виджета
     */
    getContractIdentifier(esContractState) {
        let result = '';

        if(esContractState && esContractState.code) {
            result += esContractState.code;

            if(esContractState.bookings) {
                for (let i = 0; i < esContractState.bookings.length; i++) {
                    const el = esContractState.bookings[i];

                    // Добавляется информация по статусам бронированиям внутри контракта
                    result += el.code;

                    // Добавляются ссылки на подписание
                    result += el.form_url;

                    // Добавляет номер телефона
                    result += el.phone;

                    // Количество документов (может быть изменено)
                    result += el.signed_files.length;
                }
            }
        }

        return result;
    },
};