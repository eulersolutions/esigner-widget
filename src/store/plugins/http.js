export default (store) => {
    store.handleApiRequestError = (commit, request) => {
        if (request && request.status) switch (request.status) {
            case 401: throw new Error('Ошибка запроса: пользователь не авторизован в системе eSigner');
            case 404:
                commit('HANDLE_WIDGET_WARNING', 'Запрашиваемый эндпоинт не существует');
                break;
            case 500: throw new Error('Ошибка на стороне сервера eSigner');
        }

        return request;
    }

    store.processApiRequest = (commit, apiPromise) => {
        return apiPromise
            .then((request) => store.handleApiRequestError(commit, request));
    }
};