import ESignerHelper from './partners/bnovo_helper';

export default function(authToken, wssUrl, store) {
    let socket = null;
    let connection = {
        info: null,
    };
    let channels = {
        account: null,
        widget: null,
    };

    function onAccountChannelMessage(channel) {
        // channel.on("notification", payload => {
        //     if (signerNotifications) {
        //         signerNotifications.pushNotification(null, payload);
        //     }
        // });

        channel.on("ttlock_update", payload => {
            switch (payload.type) {
                case "passcode_update":
                    store.dispatch('updatePasscode', payload);
                    break;

                default:
                    break;
            }
        });

        channel.on("account_billing_update", payload => {
            store.dispatch("updateWidgetBillingState", payload);
        });

        channel.on("application_created", payload => {
            store.dispatch("contractCreated", payload);
        });
        
        channel.on("application_updated", payload => {
            store.dispatch("updateContract", payload);
        });
    }

    function onLoad() {
        socket = new Phoenix.Socket(wssUrl, { params: { 'token': authToken } });

        socket.onOpen(function () {
            const _channels = channels;
            if (!channels.widget) {
                let channel = socket.channel(ESignerHelper.CHANNELS.WIDGET_CHANNEL, {});

                channel.join()
                    .receive("ok", _ => {
                        if (!_channels.widget) {
                            console.info("[ESigner] Joined widget channel");
                            getWidgetSocketData(channel);
                            _channels.widget = channel;
                        } else {
                            console.info("[ESigner] Re-joined widget channel");
                        }
                    });
            }
        });

        socket.connect();
    }

    function getWidgetSocketData(channel) {
        channel.push("info", {})
            .receive("ok", resp => {
                connection.info = resp;
                joinOtherChannels();
            });
    }

    function joinOtherChannels() {
        let accountId = connection.info.legal_entity_id;

        if(!channels.account) {
            let channel = socket.channel(ESignerHelper.CHANNELS.ACCOUNT + ':' + accountId, {});
            const _channels = channels;

            // Подключение к каналу текущего аккаунта
            channel.join()
                .receive("ok", _ => {
                    if (!_channels.account) {
                        console.info(`[ESigner] Joined account#${accountId} channel`);
                        _channels.account = channel;
                        onAccountChannelMessage(channel);
                    } else {
                        console.info(`[ESigner] Re-joined account#${accountId} channel`);
                    }
                });
        }
    }

    return {
        isConnected: false,

        socket: socket,
        channels: channels,
        connection: connection,

        start(libPath) {
            var s = document.createElement('script');
            s.src = libPath;
            s.onload = function() {
                onLoad();
                this.remove();
            };
            (document.head || document.documentElement).appendChild(s);
        }
    };
};