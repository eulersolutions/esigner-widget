import Vuex from 'vuex';
import PartnerStoreHelper from './partners/bnovo_helper';
import ServiceLocksStatus from '../lib/enums/lock_service_status_list';
import ESignerSocket from './socket';
import Startup from './modules/startup';
import http from './plugins/http';

function getAdultsFromCustomers(customers) {
  return customers.filter((customer) => !(customer.extra && customer.extra.guest_type == 2));
}

let store = new Vuex.Store({
  plugins: [
    http
  ],
  modules: [
    Startup
  ],
  state: function () {
    return {
      /**
       * Флаг, при котором(по идее) отображается оверфлоу предзагрузчика
       */
      isLoadingData: true,

      /**
       * Флаг отвечающий за состояние виджета "в процессе создания контракта"
       */
      isContractCreating: false,

      isUsingLockService: false,

      hasFatalError: false,
      fatalError: null,

      hasWarning: false,
      warning: null,

      confirmationPromise: null,

      /**
       * 
       */
      partnerApi: null,
      eswidgetApi: null,

      /**
       * Информация по сущности для которой оформляется удаленное заселение
       */
      partnerData: {
        customers: []
      },
      /**
       * Информация по текущему пользователю.
       */
      partnerUser: {},

      /**
       * ID бронирования
       */
      contractId: null,
      /**
       * Телефон гостя, для которого создается удаленное заселение
       */
      contractPhone: null,
      /**
       * Номер удаленного заселения, который используется как уникальный идентификатор в системе eSigner 
       */
      contractNumber: null,

      /**
       * Данные по удаленному заселению в системе eSigner
       */
      esContractState: {
        code: 'loading'
      },

      /**
       * Данные по биллингу аккаунта пользователя
       */
      billingState: null,

      /**
       * Данные, полученные с бекенда eSigner
       * В данном массиве содержится информация о том, может ли клиент(!), у которого работает
       * виджет, с ним работать.
       */
      widgetState: {},

      /**
       * Объект интервала обновления состояния виджета
       */
      updaterInterval: null,

      /**
       * Список сервисов виджета
       */
      services: {
        remote_lock_management: {
          isAvailable: false,
          status: 'loading',
          locks: []
        },
        send_sms_link_to_client: {
          isAvailable: false,
        },
        multi_guest_signing: {
          isAvailable: false,
        },
        passport_ocr: {
          isAvailable: false,
        }
      }
    };
  },
  getters: {
    getWidgetApi: (state) => state.eswidgetApi,
    getWidgetBillingState: (state) => state.billingState,
    getPartnerData: (state) => state.partnerData,
    getPartnerUser: (state) => state.partnerUser,
    getWidgetTimezone: (state) => state.partnerUser.timezone,

    isLoadingData: (state) => state.isLoadingData,
    isContractCreating: (state) => state.isContractCreating,
    isConfirmationRequired: (state) => {
      return state.confirmationPromise;
    },
    getConfirmationRequest: (state) => {
      return state.confirmationPromise;
    },
    getClientData(state) {
      return {
        phone: state.contractPhone,
        firstName: state.partnerData.first_name,
        lastName: state.partnerData.last_name,
        middleName: state.partnerData.middle_name,
        numberOfGuests: state.partnerData.number_of_guests
      };
    },
    isServiceAvailable(state) {
      return (service) => state.services[service].isAvailable;
    },
    getServiceStatus(state) {
      return (service) => state.services[service].status;
    },
    isLocksServiceConfigured: (state) => state.widgetState.ttlock_account_is_set,

    getAdultsCount: (state) => state.partnerData.number_of_adults,
    getPrimaryCustomerId: (state) => state.partnerData.customer_id,
    getAdultCustomers: (state) => {
      return getAdultsFromCustomers(state.partnerData.customers);
    },
    getAdultCustomersWithPhones: function (state) {
      const regex = PartnerStoreHelper.CONTRACT_PHONE_REGEX;
      let primaryCustomerId = state.partnerData.customer_id;

      return getAdultsFromCustomers(state.partnerData.customers).filter((customer) => {
        return true
          && customer.id != primaryCustomerId
          && customer.phone.length > 0
          && customer.phone.replaceAll(/[^\d]+/g, '').match(regex);
      });
    },

    getContractId: (state) => state.contractId,
    getContractNumber: (state) => state.contractNumber,
    getContractState: (state) => state.esContractState,
    isContractSigned: (state) => PartnerStoreHelper.signedStatuses.includes(state.esContractState.code),

    hasFatalError: (state) => state.hasFatalError,
    getFatalError: (state) => state.fatalError,
    hasWidgetWarning: (state) => state.hasWarning,
    getWidgetWarning: (state) => state.warning,

    getPartnerTemplates: (state) => state.partnerData.templates,
    getPartnerCheckinTemplates: (state) => state.partnerData.checkinTemplates,
    getFirstRoomId: (state) => state.partnerData.apartments[0].room_id,
    getApartments: (state) => state.partnerData.apartments,
    getPasscodes: (state) => {
      let contracts = state.esContractState.bookings;
      let passcodes = [];
      if (contracts) for (let i = 0; i < contracts.length; i++) {
        const contract = contracts[i];
        if (contract.is_lock_management) for (let j = 0; j < contract.passcodes.length; j++) {
          const passcode = contract.passcodes[j];
          passcodes.push(passcode);
        }
      }
      return passcodes;
    }
  },
  mutations: {
    REGISTER_PARTNER_API: (state, api) => state.partnerApi = api,
    REGISTER_ESWIDGET_API: (state, api) => state.eswidgetApi = api,
    COMMIT_WIDGET_BILLING_STATE: (state, billingState) => {
      if (!state.billingState) {
        state.billingState = billingState;
      } else {
        Object.assign(state.billingState, billingState);
      }

      // Изменение флага доступности сервиса замков
      state.services.remote_lock_management.isAvailable = billingState.available_services.includes(PartnerStoreHelper.SERVICES.REMOTE_LOCK_MANAGEMENT);
      state.services.send_sms_link_to_client.isAvailable = billingState.available_services.includes(PartnerStoreHelper.SERVICES.SEND_SMS_LINK_TO_CLIENT);
      state.services.multi_guest_signing.isAvailable = billingState.available_services.includes(PartnerStoreHelper.SERVICES.MULTI_GUEST_SIGNING);
      state.services.passport_ocr.isAvailable = billingState.available_services.includes(PartnerStoreHelper.SERVICES.PASSPORT_OCR);
    },
    COMMIT_PARTNER_USER: (state, partnerUser) => state.partnerUser = partnerUser,
    COMMIT_PARTNER_DATA: function (state, partnerData) {
      state.partnerData = partnerData;
      state.contractNumber = partnerData.contract_number;
    },
    COMMIT_WIDGET_STATE: (state, widgetState) => state.widgetState = widgetState,
    COMMIT_ES_CONTRACT_STATE: (state, esContractState) => {
      state.esContractState = esContractState;
    },
    UPDATE_CONTRACT(state, { target, contract }) {
      Object.assign(target, contract);
    },

    IS_WIDGET_LOADING_DATA: (state, isLoadingData) => state.isLoadingData = isLoadingData,
    IS_WIDGET_CONTRACT_CREATING: (state, isContractCreating) => state.isContractCreating = isContractCreating,
    HANDLE_FATAL_ERROR(state, error) {
      state.hasFatalError = true;
      state.fatalError = error;
      state.isLoadingData = false;
    },
    HANDLE_WIDGET_WARNING(state, warning) {
      state.hasWarning = true;
      state.warning = warning;
    },
    CLEAN_WARNING(state) {
      state.hasWarning = false;
      state.warning = null;
    },
    SHOW_CONFIRM_REQUEST_SCREEN: (state, promise) => state.confirmationPromise = promise,
    REMOVE_CONFIRM_REQUEST_SCREEN: (state) => state.confirmationPromise = null,
    SET_UPDATER_INTERVAL(state, interval) {
      state.updaterInterval = interval;
    },
    COMMIT_CONTRACT_ID: (state, contractId) => state.contractId = contractId,
    COMMIT_CONTRACT_PHONE: (state, contractPhone) => state.contractPhone = contractPhone,
    SET_SERVICE_LOCKS_STATUS: (state, status) => state.services.remote_lock_management.status = status,
    UPDATE_APARTMENTS: (state, apartments) => state.partnerData.apartments = apartments,
    UPDATE_APARTMENTS_SCHEDULE: (state, schedule) => state.partnerData.schedule = schedule,
    UPDATE_PASSCODE: (state, opts) => {
      let passcode = opts.target;
      let data = opts.data;

      passcode.start_datetime = data.start_datetime || passcode.start_datetime;
      passcode.end_datetime = data.end_datetime || passcode.end_datetime;
      passcode.passcode = data.code || passcode.passcode;
      passcode.status_id = data.status || passcode.status_id;
    }
  },
  actions: {
    async handleFatalError({ commit }, error) {
      commit('HANDLE_FATAL_ERROR', error);
    },
    async handleWidgetWarning({ commit }, warning) {
      commit('HANDLE_WIDGET_WARNING', warning);
    },
    cleanWarning({ commit }) {
      commit('CLEAN_WARNING');
    },

    async getWidgetContractState({ commit, state }) {
      const contractNumber = state.partnerData.contract_number;

      // Получение информации по удаленному заселению в eSigner
      const esContractState = await this.processApiRequest(
        commit,
        state.eswidgetApi.getContractState(contractNumber).then(r => r.json())
      );

      commit('COMMIT_ES_CONTRACT_STATE', esContractState);
    },

    /**
     * Заново загружает стейт контракта
     * 
     * Метод используется сокетом при получении сообщения 'application_created'
     * 
     * Т.к. контрактов может создаться за раз несколько штук, например при оформлении с сервисом
     * множественного заселения, а в сообщении всего один контракт - обновляется стейт виджета полностью.
     */
    async contractCreated({ commit, state }, data) {
      const contractNumber = state.partnerData.contract_number;

      if(data.contract_number.startsWith(contractNumber)) {
        this.dispatch("contractCreating", true);
        this.dispatch('togglePreloader', true);
        await this.dispatch('getWidgetContractState');
        this.dispatch("contractCreating", false);
        this.dispatch('togglePreloader', false);
      }
    },

    /**
     * Обновляет контракт находящийся в стейте виджета
     * 
     * Метод используется сокетом при получении сообщения 'application_updated'
     */
    updateContract({ commit, state }, contract) {
      let bnovo_customer_id = contract.bnovo_customer_id;
      const contractNumber = state.partnerData.contract_number;

      let contracts = state.esContractState.bookings;
      if (contracts && contract.contract_number.startsWith(contractNumber)) {
        const target = contracts.find(el => el.bnovo_customer_id == bnovo_customer_id);

        if (target) {
          commit('UPDATE_CONTRACT', { target, contract });
          // Если обновление пришло по основному бронированию, то contract_number будет без суффикса "/1" и т.д.
          // В таком случае надо обновить основное состояние контракта (кода)
          if(contract.contract_number.split('/').length == 1) {
            commit('COMMIT_ES_CONTRACT_STATE', Object.assign(state.esContractState, {code: contract.code}));
          }
          this.dispatch('togglePreloader', false);
        }
      }
    },

    /**
     * Загрузка информации по замкам
     */
    async loadApartmentsLocks({ commit, state }) {
      let serviceLocksStatus = ServiceLocksStatus.OK;

      const apartments = state.partnerData.apartments;
      var locks_data = null;

      if (apartments.length < 1) {
        serviceLocksStatus = ServiceLocksStatus.PERIOD_NOT_SET;
      } else {
        locks_data =
          await this.processApiRequest(
            commit,
            state.eswidgetApi.getApartmentsLocksData(apartments).then(r => r.json())
          );
      }

      if (locks_data.status === 'ok') {
        commit('COMMIT_ESIGNER_LOCKS', locks_data.locks);

        let withoutLocksCount = 0;
        let isRemoteBookingPossibleWithLocks = true;

        for (let i = 0; i < apartments.length; i++) {
          const el = apartments[i];

          let apart = locks_data.apartments.find((data_el) => {
            return data_el.room_id == el.room_id;
          });

          if (apart !== undefined) {
            let locks = locks_data.locks.filter((data_lock) => {
              return apart.locks.includes("" + data_lock.lockId);
            });

            el['isSignerExists'] = true;
            el['locks'] = locks;
          } else {
            // Если апартаменты не были найдены - в сайнере требуется синхронизация
            el['isSignerExists'] = false;
            el['locks'] = [];
            serviceLocksStatus = ServiceLocksStatus.REQUIRED_SYNCH;
          }

          if (el['room_id'] == 0) {
            isRemoteBookingPossibleWithLocks = false;
            serviceLocksStatus = ServiceLocksStatus.PERIOD_NOT_SET;
          }

          if (el['locks'].length == 0) {
            ++withoutLocksCount;
          }

          if (isRemoteBookingPossibleWithLocks && withoutLocksCount == apartments.length) {
            isRemoteBookingPossibleWithLocks = false;
            serviceLocksStatus = ServiceLocksStatus.EMPTY_LOCKS;
          }
        }
      } else if (locks_data.status === "error") {
        switch (locks_data.code) {
          case 'system':
            this.dispatch('handleWidgetWarning', 'Ошибка сервиса: ' + locks_data.error);
            break;
          case 'ttlock_timeout':
            serviceLocksStatus = ServiceLocksStatus.TIMEOUT;
            break;
          default:
            throw new Error('Получена неизвестная ошибка при работе сервиса замков TTLock');
        }
      }

      let bookingId = state.partnerData.bnovo_booking_id;
      let schedule =
        await new Promise(function (resolve) {
          // loadBookingData - глобальная функция bnovo
          loadBookingData(bookingId, function () {
            let schedule = [];
            Object.keys(booking_data).forEach((date) => {
              const el = booking_data[date];

              schedule.push({
                apartments_id: el.room_id,
                apartments_real_type_id: el.real_room_type_id,
                date: date
              });
            });
            resolve(schedule);
          });
        });

      commit('UPDATE_APARTMENTS', apartments);
      commit('UPDATE_APARTMENTS_SCHEDULE', schedule);
      commit('SET_SERVICE_LOCKS_STATUS', serviceLocksStatus);
    },

    /**
     * 
     */
    async updateState({ commit, state }, interval) {
      let i = (interval !== undefined ? interval : 5000);
      let contract_number = state.partnerData.contract_number;
      let identifier = PartnerStoreHelper.getContractIdentifier(state.esContractState);

      // Обработка случая если проверка состояния была запущена еще раз - вырубаем предыдущий интервал
      if (state.updaterInterval) {
        clearInterval(state.updaterInterval);
      }

      let updaterInterval = setInterval(async () => {
        this.processApiRequest(
          commit,
          state.eswidgetApi.getContractState(contract_number).then(r => r.json())
        ).then((result) => {
          if (result) {
            let newIdentifier = PartnerStoreHelper.getContractIdentifier(result);

            if (newIdentifier != identifier) {
              identifier = newIdentifier;
              commit('COMMIT_ES_CONTRACT_STATE', result);
              this.dispatch('togglePreloader', false);
              this.dispatch("contractCreating", false);
            }
          } else {
            console.warn('[ESigner] Ошибка при обновлении статуса контракта', result);
            clearInterval(this.updaterInterval);
          }
        });
      }, i);

      commit('SET_UPDATER_INTERVAL', updaterInterval);
    },

    async sendLinkToGuest({ commit, state }) {
      let contractId = state.contractNumber;
      return state.eswidgetApi.sendLinkToGuest(contractId).then(r => r.json());
    },

    async changeGuestPhone({ commit, state }, [newPhone, contract]) {
      let customer_id = contract.bnovo_customer_id;
      let contractId = state.contractNumber;

      return state.eswidgetApi.updateGuestData(contractId, { phone: newPhone }, customer_id).then(r => r.json());
    },

    async shiftPasscodeHour({ commit, state }, data) {
      return state.eswidgetApi.shiftPasscodeHour(data.id, data.shiftType).then(r => r.json());
    },

    async initSocket({ commit, state }, opts) {
      const socket = new ESignerSocket(opts.token, opts.wssUrl, this).start(opts.libPath);
      commit("UPDATE_SOCKET", socket);
    },

    async updatePasscode({ commit, state }, passcode) {
      const passcodes = this.getters.getPasscodes;

      let target = passcodes.find(el => passcode.id == el.id);
      if (target) {
        commit("UPDATE_PASSCODE", { target: target, data: passcode });
      }
    },

    async updateWidgetBillingState({ commit }, billingState) {
      commit("COMMIT_WIDGET_BILLING_STATE", billingState);
      commit("REMOVE_CONFIRM_REQUEST_SCREEN");
    },

    // Фунционал показа экрана подтверждения действия
    showConfirmationScreen({ commit }, data) {
      let message = data.message || 'Требуется подтверждение действия';

      let callback = null;
      let promise = new Promise((resolve) => {
        const onAccept = data.onAccept || (_ => _);
        const onDecline = data.onDecline || (_ => _);

        callback = (value) => {
          commit("REMOVE_CONFIRM_REQUEST_SCREEN");
          value ? onAccept() : onDecline();
          resolve(value);
        };
      });
      promise.action = callback;
      promise.message = message;

      commit("SHOW_CONFIRM_REQUEST_SCREEN", promise);
    },

    togglePreloader({ commit, state }, value) {
      if (value != state.isLoadingData) {
        commit('IS_WIDGET_LOADING_DATA', value);
      }
    },

    contractCreating({ commit, state }, value) {
      if (value != state.isContractCreating) {
        commit('IS_WIDGET_CONTRACT_CREATING', value);
      }
    }
  },
});

export default store;