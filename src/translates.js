const locales = {
    ru: {
        'checkin_datetime': 'Дата заезда',
        'checkout_datetime': 'Дата выезда',
        'bookingId': 'ID бронирования',
        'customerId': 'Внешний ИД гостя',
        'first_name': 'Имя',
        'last_name': 'Фамилия',
        'middle_name': 'Отчество',
        'phone': 'Телефон',
        'email': 'E-mail гостя',
        'number_of_adults': 'Количество взрослых гостей',
        'status.new': 'Приглашение к подписанию сформировано, но не отправлено автоматически.',
        'status.link_sent': 'Приглашение к подписанию сформировано и отправлено гостю',
        'status.visited': 'Гость перешел по ссылкe, заполняет персональные данные,<br/>вы получите уведомление после подписания',
        'status.waiting_signing_of_other_contracts': 'Гость подписал свой договор. Ожидание подписания другими гостями',
        'status.signed': 'Договор успешно подписан',
        'locks_status.loading': 'Получение информации по возможности оформления с замками...',
        'locks_status.ok': 'Доступно оформление удалённого заселения с замками',
        'locks_status.locks_not_exists': 'В бронировании нет привязанных к апартаментам замков.<br/>В данный момент доступно только подписание документов.',
        'locks_status.period_not_set': 'В бронировании есть периоды, которые не распределены по номерам.<br/>В данный момент доступно только подписание документов.',
        'locks_status.required_synchronization': 'В бронировании есть апартаменты, не синхронизированные с системой eSigner.<br/>Пожалуйста, подождите некоторое время.<br/>В данный момент доступно только подписание документов.',
        'locks_status.account_not_set': 'Ваш аккаунт не синхронизирован с TTLock.<br/>В данный момент доступно только подписание документов.',
        'locks_status.ttlock_timeout': 'Сервис замков недоступен, попробуйте обновить страницу.<br/>В данный момент доступно только подписание документов.',
        'passcode_status.failed': 'Ошибка установки',
        'passcode_status.default': 'Устанавливается',
        'passcode_status.expired': 'Истек',
        'passcode_error.ttlock_timeout': 'Ошибка запроса к TTLock, попробуйте позже',
        'passcode_error.gateway_busy': 'Шлюз временно занят, попробуйте позже',
        'passcode_error.gateway_is_offline': 'Шлюз отключен, включите и попробуйте снова',
        'passcode_error.lock_wo_gateway': 'Замок не подключен к шлюзу',
        'passcode_error.unexpected': 'Неизвестная ошибка при выполнении операции, попробуйте позже',
    }
}

export default function(msgid) {
    let translate = locales.ru[msgid];
    if (translate !== undefined) {
        return translate;
    } else {
        return msgid;
    }
};